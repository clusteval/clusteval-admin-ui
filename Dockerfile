FROM gradle:alpine
USER root
RUN apk update && apk add libstdc++ && rm -rf /var/cache/apk/*
ENV GRADLE_HOME /usr/bin/gradle
ENV GRADLE_USER_HOME /cache
ADD . /clusteval-admin-ui/
WORKDIR /clusteval-admin-ui/
RUN gradle build
ENV JAVA_OPTS=""
ENTRYPOINT [ "java", "-Xdebug","-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=9001", "-Djava.security.egd=file:/dev/./urandom", "-jar", "build/libs/clusteval-web-admin-1.0.2.jar" ]

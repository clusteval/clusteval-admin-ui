package clusteval.models;

import java.util.ArrayList;

public class ParameterOptimizationResultProgram {

	private String alias;

	private String name;

	private int id;

	private ArrayList<ParameterOptimizationResultData> data = new ArrayList<ParameterOptimizationResultData>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getAlias() {
		return alias;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList<ParameterOptimizationResultData> getData() {
		return data;
	}

	public void setData(ArrayList<ParameterOptimizationResultData> data) {
		this.data = data;
	}

	public void addToData(ParameterOptimizationResultData data) {
		this.data.add(data);
	}
}

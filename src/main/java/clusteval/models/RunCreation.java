package clusteval.models;

import java.rmi.ConnectException;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;

import clusteval.validators.NotBlankOrNull;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.framework.repository.ISerializableWrapper;
import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;
import de.clusteval.framework.repository.ObjectNotFoundException;
import de.clusteval.framework.repository.ObjectVersionNotFoundException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.run.IDataAnalysisRun;
import de.clusteval.run.IExecutionRun;
import de.clusteval.run.IParameterOptimizationRun;
import de.clusteval.run.IRobustnessAnalysisRun;
import de.clusteval.run.IRun;
import de.clusteval.run.ISerializableClusteringRun;
import de.clusteval.run.ISerializableDataAnalysisRun;
import de.clusteval.run.ISerializableParameterOptimizationRun;
import de.clusteval.run.ISerializableRun;
import de.clusteval.run.ISerializableRunAnalysisRun;
import de.clusteval.run.ISerializableRunDataAnalysisRun;
import de.clusteval.run.RUN_TYPE;
import de.clusteval.run.SerializableAnalysisRun;
import de.clusteval.run.SerializableClusteringRun;
import de.clusteval.run.SerializableDataAnalysisRun;
import de.clusteval.run.SerializableExecutionRun;
import de.clusteval.run.SerializableParameterOptimizationRun;
import de.clusteval.run.SerializableRobustnessAnalysisRun;
import de.clusteval.run.SerializableRunAnalysisRun;
import de.clusteval.run.SerializableRunDataAnalysisRun;
import de.clusteval.utils.IStatistic;
import de.clusteval.utils.IncompatibleClustEvalVersionException;

public class RunCreation {
	
	private String originalName;

	@Size(min = 1, message = "Please specify a name for the run")
	private String name;

	@javax.validation.constraints.Pattern(regexp = "\\d+(-SNAPSHOT)?", message = "Please only use digits, optionally postfixed with the '-SNAPSHOT' tag.")
	private String version = "0";

	@NotNull(message = "Please specify a mode for the run")
	private String mode;

	@NotNull(message = "Please select a optimization criterion")
	private String optimizationCriterion;

	@NotNull(message = "Please select a optimization method")
	private String optimizationMethod;

	@NotNull(message = "Please specify number of optimization iterations")
	@Min(value = 1, message = "Please specify a positive integer")
	private Integer optimizationIterations = 100;

	@NotBlankOrNull(message = "Please select one or more programs for the run")
	private ArrayList<String> programs = new ArrayList<String>();

	@NotBlankOrNull(message = "Please select one or more data sets for the run")
	private ArrayList<String> dataSets = new ArrayList<String>();

	@NotBlankOrNull(message = "Please select one or more quality measures for the run")
	private ArrayList<String> qualityMeasures = new ArrayList<String>();

	@NotBlankOrNull(message = "Please select one or more data statistics for the run")
	private ArrayList<String> dataStatistics = new ArrayList<String>();

	@NotBlankOrNull(message = "Please select one or more run statistics for the run")
	private ArrayList<String> runStatistics = new ArrayList<String>();

	@NotBlankOrNull(message = "Please select one or more run data statistics for the run")
	private ArrayList<String> runDataStatistics = new ArrayList<String>();

	// @NotBlankOrNull(message = "Please select one or more unique run
	// identifiers for the run")
	private ArrayList<String> uniqueRunIdentifiers = new ArrayList<String>();

	@NotBlankOrNull(message = "Please select one or more unique data identifiers for the run")
	private ArrayList<String> uniqueDataIdentifiers = new ArrayList<String>();

	private Integer numberOfRandomizedDataSets;

	private String randomizer;

	private ArrayList<List<Parameter>> randomizerParameters = new ArrayList<>();

	@Valid
	private ArrayList<Program> programSettings = new ArrayList<Program>();

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public String getName() {
		return name;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getVersion() {
		return version;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getOptimizationCriterion() {
		return optimizationCriterion;
	}

	public void setOptimizationCriterion(String optimizationCriterion) {
		this.optimizationCriterion = optimizationCriterion;
	}

	public String getOptimizationMethod() {
		return optimizationMethod;
	}

	public void setOptimizationMethod(String optimizationMethod) {
		this.optimizationMethod = optimizationMethod;
	}

	public Integer getOptimizationIterations() {
		return optimizationIterations;
	}

	public void setOptimizationIterations(Integer optimizationIterations) {
		this.optimizationIterations = optimizationIterations;
	}

	public ArrayList<String> getPrograms() {
		return programs;
	}

	public void setPrograms(Collection<String> programs) {
		this.programs = new ArrayList<String>(programs);
	}

	public ArrayList<String> getDataSets() {
		return dataSets;
	}

	public void setDataSets(Collection dataSets) {
		this.dataSets = new ArrayList<String>(dataSets);
	}

	public ArrayList<String> getQualityMeasures() {
		return qualityMeasures;
	}

	public void setQualityMeasures(Collection qualityMeasures) {
		this.qualityMeasures = new ArrayList<String>(qualityMeasures);
	}

	public ArrayList<String> getDataStatistics() {
		return dataStatistics;
	}

	public void setDataStatistics(Collection dataStatistics) {
		this.dataStatistics = new ArrayList<String>(dataStatistics);
	}

	public ArrayList<String> getRunStatistics() {
		return runStatistics;
	}

	public void setRunStatistics(Collection runStatistics) {
		this.runStatistics = new ArrayList<String>(runStatistics);
	}

	public ArrayList<String> getRunDataStatistics() {
		return runDataStatistics;
	}

	public void setRunDataStatistics(Collection runDataStatistics) {
		this.runDataStatistics = new ArrayList<String>(runDataStatistics);
	}

	public ArrayList<String> getUniqueRunIdentifiers() {
		return uniqueRunIdentifiers;
	}

	public void setUniqueRunIdentifiers(Collection uniqueRunIdentifiers) {
		this.uniqueRunIdentifiers = new ArrayList<String>(uniqueRunIdentifiers);
	}

	public ArrayList<String> getUniqueDataIdentifiers() {
		return uniqueDataIdentifiers;
	}

	public void setUniqueDataIdentifiers(Collection uniqueDataIdentifiers) {
		this.uniqueDataIdentifiers = new ArrayList<String>(uniqueDataIdentifiers);
	}

	public String getRandomizer() {
		return randomizer;
	}

	public void setRandomizer(String randomizer) {
		this.randomizer = randomizer;
	}

	public void setRandomizerParameters(ArrayList<List<Parameter>> randomizerParameters) {
		this.randomizerParameters = randomizerParameters;
	}

	public ArrayList<List<Parameter>> getRandomizerParameters() {
		return randomizerParameters;
	}

	public Integer getNumberOfRandomizedDataSets() {
		return numberOfRandomizedDataSets;
	}

	public void setNumberOfRandomizedDataSets(Integer numberOfRandomizedDataSets) {
		this.numberOfRandomizedDataSets = numberOfRandomizedDataSets;
	}

	public ArrayList<Program> getProgramSettings() {
		return programSettings;
	}

	public void setProgramParameters(ArrayList<Program> programSettings) {
		this.programSettings = programSettings;
	}

	public void parse(final ISerializableRun<? extends IRun> run, final Map<String, Program> convertedPrograms)
			throws ObjectNotFoundException, ObjectVersionNotFoundException, ConnectException, NoSuchObjectException, RemoteException, IncompatibleClustEvalVersionException, ParseException {
		setName(run.getName());
		// TODO: we have to store (edit of) infos in the data structure of runs,
		// or better: generally for all repository objects
		// if (currentLine.startsWith("#editOf")) {
		// setOriginalName(line);
		// }
		setOriginalName(run.getName());
		if (run instanceof IRobustnessAnalysisRun)
			setMode(RUN_TYPE.ROBUSTNESS_ANALYSIS.toString());
		else if (run instanceof ISerializableClusteringRun)
			setMode(RUN_TYPE.CLUSTERING.toString());
		else if (run instanceof ISerializableParameterOptimizationRun)
			setMode(RUN_TYPE.PARAMETER_OPTIMIZATION.toString());
		else if (run instanceof ISerializableDataAnalysisRun)
			setMode(RUN_TYPE.DATA_ANALYSIS.toString());
		else if (run instanceof ISerializableRunAnalysisRun)
			setMode(RUN_TYPE.RUN_ANALYSIS.toString());
		else if (run instanceof ISerializableRunDataAnalysisRun)
			setMode(RUN_TYPE.RUN_DATA_ANALYSIS.toString());

		if (run instanceof SerializableExecutionRun) {
			SerializableExecutionRun<? extends IExecutionRun<?>> execRun = (SerializableExecutionRun) run;
			Map<String, ISerializableProgramConfig<? extends IProgramConfig>> programs = new HashMap<>();
			for (ISerializableProgramConfig pc : execRun.getProgramConfigs()) {
				programs.put(pc.toString(), pc);
			}
			setPrograms(programs.keySet());

			// Program parameters
			Map<String, Program> programSettings = new HashMap<>();
			ArrayList<ProgramParameter> programParameters = new ArrayList<ProgramParameter>();
			
			Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> fixedParameterValues = execRun.getFixedParameterValues();
//			
//			try {
//				// Add programs for older runs without parameter settings
//				for (String programName : getPrograms()) {
//					Program programTest = new Program();
//					programTest.setName(programName.trim());
//					if (!programs.contains(programTest)) {
//						ProgramService programService = new ProgramService();
//						program = programService.getConvertedProgramConfig(programName);
//
//						programs.add(program);
//					}
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//
//			setProgramParameters(programs);

			List<String> dataSets = new ArrayList<>();
			for (ISerializableDataConfig dc : execRun.getDataConfigs())
				dataSets.add(dc.toString());
			setDataSets(dataSets);

			List<String> qualityMeasures = new ArrayList<>();
			for (ISerializableWrapperDynamicComponent<IClusteringQualityMeasure> dc : execRun.getQualityMeasures())
				if (dc != null)
					qualityMeasures.add(dc.toString());
			setQualityMeasures(qualityMeasures);

			if (run instanceof SerializableParameterOptimizationRun) {
				SerializableParameterOptimizationRun<IParameterOptimizationRun> optRun = (SerializableParameterOptimizationRun<IParameterOptimizationRun>) run;

				if (optRun.getOptimizationCriterion() != null)
					setOptimizationCriterion(optRun.getOptimizationCriterion().toString());
				if (!optRun.getOptimizationMethods().isEmpty() && optRun.getOptimizationMethods().get(0) != null)
					setOptimizationMethod(optRun.getOptimizationMethods().get(0).toString());
				setOptimizationIterations(optRun.getTotalIterationCount());
			} else if (run instanceof SerializableClusteringRun) {
			} else if (run instanceof SerializableRobustnessAnalysisRun) {
				// } else if (currentLine.startsWith("randomizer")) {
				// setRandomizer(line);
				// } else if
				// (currentLine.startsWith("numberOfRandomizedDataSets")) {
				// setNumberOfRandomizedDataSets(Integer.parseInt(line));

				// Randomizers
				// ArrayList<Randomizer> randomizers = new
				// ArrayList<Randomizer>();
				// try {
				// BufferedReader br = new BufferedReader(new FileReader(path +
				// "/runs/" + fileName + ".run"));
				// String currentLine;
				//
				// while ((currentLine = br.readLine()) != null) {
				// if (currentLine.startsWith("[" + getRandomizer())) {
				// Randomizer randomizer = new Randomizer();
				// randomizer.setName(getRandomizer());
				// ArrayList<Parameter> parameters = new ArrayList<Parameter>();
				// while (!(currentLine = br.readLine()).equals("")) {
				// Parameter parameter = new Parameter();
				// parameter.setName(currentLine.split("=")[0].trim());
				// parameter.setValue(currentLine.split("=")[1].trim());
				// parameters.add(parameter);
				// }
				// randomizer.setParameters(parameters);
				// randomizers.add(randomizer);
				// }
				// }
				// setRandomizers(randomizers);
				// } catch (Exception e) {
				// }
			}
		} else if (run instanceof SerializableAnalysisRun) {

			if (run instanceof SerializableDataAnalysisRun) {
				SerializableDataAnalysisRun<IDataAnalysisRun> dataAnRun = (SerializableDataAnalysisRun<IDataAnalysisRun>) run;
				List<String> dataSets = new ArrayList<>();
				for (ISerializableDataConfig dc : dataAnRun.getDataConfigs())
					dataSets.add(dc.toString());
				setDataSets(dataSets);

				List<String> statistics = new ArrayList<>();
				for (ISerializableWrapper<? extends IStatistic> stat : dataAnRun.getStatistics())
					statistics.add(stat.toString());
				setDataStatistics(statistics);
			} else if (run instanceof SerializableRunAnalysisRun) {

				// } else if (currentLine.startsWith("runStatistics")) {
				// setRunStatistics(Arrays.asList(line.split("\\s*,\\s*")));
				// } else if (currentLine.startsWith("uniqueRunIdentifiers")) {
				// setUniqueRunIdentifiers(Arrays.asList(line.split("\\s*,\\s*")));
			} else if (run instanceof SerializableRunDataAnalysisRun) {

				// } else if (currentLine.startsWith("runDataStatistics")) {
				// setRunDataStatistics(Arrays.asList(line.split("\\s*,\\s*")));
				// } else if (currentLine.startsWith("uniqueRunIdentifiers")) {
				// setUniqueRunIdentifiers(Arrays.asList(line.split("\\s*,\\s*")));
				// } else if (currentLine.startsWith("uniqueDataIdentifiers")) {
				// setUniqueDataIdentifiers(Arrays.asList(line.split("\\s*,\\s*")));
			}
		}
	}

	public String toString(String path) {
		String run = "mode = " + mode + "\n";

		if (mode.equals("clustering") || mode.equals("parameter_optimization") || mode.equals("dataAnalysis")
				|| mode.equals("robustnessAnalysis")) {
			run += "dataConfig = ";
			run += StringUtils.join(dataSets, ',');
			run += "\n";
		}

		if (mode.equals("clustering") || mode.equals("parameter_optimization") || mode.equals("robustnessAnalysis")) {
			run += "programConfig = ";
			run += StringUtils.join(programs, ',');
			run += "\n";

			run += "qualityMeasures = ";
			run += StringUtils.join(qualityMeasures, ',');
			run += "\n";
		}

		if (mode.equals("parameter_optimization")) {
			run += "optimizationCriterion = " + optimizationCriterion + "\n";

			run += "optimizationIterations = " + optimizationIterations + "\n";

			run += "optimizationMethod = " + optimizationMethod + "\n\n";

			ArrayList<String> parametersToOptimize;
			for (Program program : programSettings) {
				if (program.getParameters() != null) {
					parametersToOptimize = new ArrayList<String>();

					run += "[" + program.getName() + "]\n";

					for (ProgramParameter parameter : program.getParameters()) {
						if (parameter.getOptimize() && parameter.getOptimizable()) {
							parametersToOptimize.add(parameter.getName());
						}
					}
					run += "optimizationParameters = " + StringUtils.join(parametersToOptimize, ',') + "\n\n";
				}
			}
		}

		if (mode.equals("dataAnalysis")) {
			run += "dataStatistics = " + StringUtils.join(dataStatistics, ',') + "\n";
		}

		if (mode.equals("runAnalysis") || mode.equals("robustnessAnalysis")) {
			run += "uniqueRunIdentifiers = " + StringUtils.join(uniqueRunIdentifiers, ',') + "\n";
		}

		if (mode.equals("runAnalysis")) {
			run += "runStatistics = " + StringUtils.join(runStatistics, ',') + "\n";
		}

		if (mode.equals("runDataAnalysis")) {
			run += "runDataStatistics = " + StringUtils.join(runDataStatistics, ',') + "\n";
			run += "uniqueRunIdentifiers = " + StringUtils.join(uniqueRunIdentifiers, ',') + "\n";
			run += "uniqueDataIdentifiers = " + StringUtils.join(uniqueDataIdentifiers, ',') + "\n";
		}

		// TODO: i removed the randomizers attribute
		// i think we will have to parse the randomizer parameters still
		// if (mode.equals("robustnessAnalysis")) {
		// run += "randomizer = " + randomizer + "\n";
		// run += "numberOfRandomizedDataSets = " + numberOfRandomizedDataSets +
		// "\n";
		//
		// int randomizerCounter = 1;
		// for (Randomizer rand : randomizers) {
		// run += "\n[" + randomizer + "_" + randomizerCounter + "]\n";
		// for (Parameter parameter : rand.getParameters()) {
		// if (!parameter.getValue().isEmpty()) {
		// run += parameter.getName() + " = " + parameter.getValue() + "\n";
		// }
		// }
		// randomizerCounter++;
		// }
		// }

		if (mode.equals("clustering") || mode.equals("parameter_optimization") || mode.equals("robustnessAnalysis")) {
			for (Program programSetting : programSettings) {
				if (programSetting.getParameters() != null) {
					for (ProgramParameter programParameter : programSetting.getParameters()) {
						run += "\n[" + programSetting.getName() + ":" + programParameter.getName() + "]\n";
						if (programParameter.getMinValue() != null) {
							run += "minValue=" + programParameter.getMinValue() + "\n";
						}
						if (programParameter.getMaxValue() != null) {
							run += "maxValue=" + programParameter.getMaxValue() + "\n";
						}
						if (programParameter.getValue() != null) {
							run += "def=" + programParameter.getValue() + "\n";
						}
						if (programParameter.getOptions() != null) {
							run += "options=" + programParameter.getOptions() + "\n";
						}
					}
				}
			}
		}

		return run;
	}
}

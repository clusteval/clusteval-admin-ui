package clusteval.models;

import org.springframework.web.multipart.MultipartFile;

import clusteval.validators.ContentTypeJar;

public class Utility {
    private String typeName;

    @ContentTypeJar(message = "Please provide a .jar file")
    private MultipartFile file;

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}

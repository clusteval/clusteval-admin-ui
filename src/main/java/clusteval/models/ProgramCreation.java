package clusteval.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

import clusteval.validators.HasOneFile;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.framework.repository.ISerializableWrapper;
import de.clusteval.program.IIntegerProgramParameter;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.program.IStringProgramParameter;
import de.clusteval.program.r.ISerializableRProgramConfig;

public class ProgramCreation {
	private boolean isRProgram;

	@Size(min = 1, message = "Please specify a name for the program")
	@javax.validation.constraints.Pattern(regexp = "([0-9|a-z|A-Z|\\_])+", message = "Please only include letters a-z, numbers 0-9 and underscores (_) in the name")
	private String name;

	@Size(min = 1, message = "Please specify a version for the program")
	@javax.validation.constraints.Pattern(regexp = "\\d+(-SNAPSHOT)?", message = "Please only use digits, optionally postfixed with the '-SNAPSHOT' tag.")
	private String version = "0";

	private String originalName;

	@Size(min = 1, message = "Please specify an alias for the program")
	private String alias;

	private String invocationFormat;

	@HasOneFile(message = "Please provide a executable file containing the program")
	private MultipartFile executableFile;

	@Valid
	private ArrayList<ProgramCreationParameter> parameters;

	private ArrayList<String> compatibleDataSetFormats = new ArrayList<String>();

	private String outputFormat = "";

	public boolean getIsRProgram() {
		return isRProgram;
	}

	public void setIsRProgram(boolean isRProgram) {
		this.isRProgram = isRProgram;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getInvocationFormat() {
		return invocationFormat;
	}

	public void setInvocationFormat(String invocationFormat) {
		this.invocationFormat = invocationFormat;
	}

	public MultipartFile getExecutableFile() {
		return executableFile;
	}

	public void setExecutableFile(MultipartFile executableFile) {
		this.executableFile = executableFile;
	}

	public ArrayList<ProgramCreationParameter> getParameters() {
		return parameters;
	}

	public void setParameters(ArrayList<ProgramCreationParameter> parameters) {
		this.parameters = parameters;
	}

	public ArrayList<String> getCompatibleDataSetFormats() {
		return compatibleDataSetFormats;
	}

	public void setCompatibleDataSetFormats(Collection compatibleDataSetFormats) {
		this.compatibleDataSetFormats = new ArrayList<String>(compatibleDataSetFormats);
	}

	public String getOutputFormat() {
		return outputFormat;
	}

	public void setOutputFormat(String outputFormat) {
		this.outputFormat = outputFormat;
	}

	public void parse(ISerializableProgramConfig<? extends IProgramConfig> programConfig) {
		setName(programConfig.getName());
		setOriginalName(programConfig.getName());
		setIsRProgram(programConfig instanceof ISerializableRProgramConfig);

		List<String> dataSetFormats = new ArrayList<>();
		for (ISerializableWrapper<IDataSetFormat> f : programConfig.getCompatibleDataSetFormats())
			dataSetFormats.add(f.toString());
		setCompatibleDataSetFormats(dataSetFormats);
		setOutputFormat(programConfig.getOutputFormat().toString());
		setAlias(programConfig.getProgram().getAlias());
		setInvocationFormat(programConfig.getInvocationFormat(false));

		// Program parameters
		List<IProgramParameter<?>> originalParams = programConfig.getParameters();
		List<IProgramParameter<?>> originalOptimizableParams = programConfig.getOptimizableParameters();
		ArrayList<ProgramCreationParameter> parameters = new ArrayList<ProgramCreationParameter>();

		for (IProgramParameter<?> p : originalParams) {
			ProgramCreationParameter parameter = new ProgramCreationParameter();
			parameter.setName(p.getName());
			parameter.setDescription(p.getDescription());
			parameter.setOptimizable(originalOptimizableParams.contains(p));
			parameter.setMinValue(p.getMinValue());
			parameter.setMaxValue(p.getMaxValue());
			parameter.setDefaultValue(p.getDefault());
			parameter.setOptions(Arrays.asList(p.getOptions()));
			parameter.setType(
					p instanceof IIntegerProgramParameter ? 1 : (p instanceof IStringProgramParameter ? 0 : 2));

			if (parameter.getDefaultValue() != null) {
				parameters.add(parameter);
			}

			setParameters(parameters);
		}
	}
}
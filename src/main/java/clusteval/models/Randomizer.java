package clusteval.models;

import java.util.ArrayList;

public class Randomizer {
	private String name;
	private ArrayList<Parameter> availableParameters = new ArrayList<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Parameter> getAvailableParameters() {
		return availableParameters;
	}

	public void setAvailableParameters(ArrayList<Parameter> availableParameters) {
		this.availableParameters = availableParameters;
	}
}

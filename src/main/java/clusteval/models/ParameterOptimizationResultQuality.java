package clusteval.models;

public class ParameterOptimizationResultQuality {
	private String alias;
	
    private String name;
    
    private String version;

    private String value;

    private String parameterSet;
    
    public void setAlias(String alias) {
		this.alias = alias;
	}
    
    public String getAlias() {
		return alias;
	}

    public String getName() {
        return name;
    }

	public void setName(String name) {
        this.name = name;
    }
    
    public void setVersion(String version) {
		this.version = version;
	}
    
    public String getVersion() {
		return version;
	}

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getParameterSet() {
        return parameterSet;
    }

    public void setParameterSet(String parameterSet) {
        this.parameterSet = parameterSet;
    }
}

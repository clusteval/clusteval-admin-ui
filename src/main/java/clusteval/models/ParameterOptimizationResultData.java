package clusteval.models;

import java.util.ArrayList;

public class ParameterOptimizationResultData {
	private String name;

	private String alias;

	private int id;

	private ArrayList<ParameterOptimizationResultQuality> qualities = new ArrayList<ParameterOptimizationResultQuality>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getAlias() {
		return alias;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList<ParameterOptimizationResultQuality> getQualities() {
		return qualities;
	}

	public void setQualities(ArrayList<ParameterOptimizationResultQuality> qualitities) {
		this.qualities = qualities;
	}

	public void addToQualities(ParameterOptimizationResultQuality quality) {
		qualities.add(quality);
	}
}

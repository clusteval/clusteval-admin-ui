package clusteval.models;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

import clusteval.validators.HasOneFile;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.ISerializableDataSet;

public class DataCreation {
	@Size(min = 1, message = "Please specify a name for the data")
	@Pattern(regexp = "([0-9|a-z|A-Z|\\_])+", message = "Please only include letters a-z, numbers 0-9 and underscores (_) in the name")
	private String name;

	@Size(min = 1, message = "Please specify a readable alias")
	private String alias;

	private String fileName;

//	private List<String> additionalFileNames;

	@Pattern(regexp = "\\d+(-SNAPSHOT)?", message = "Please only use digits, optionally postfixed with the '-SNAPSHOT' tag.")
	private String version = "0";

	@NotNull(message = "Please specify a dataset type")
	private String dataSetType;

	@NotNull(message = "Please specify a dataset format")
	private String dataSetFormat;

	@HasOneFile(message = "Please provide a dataset file")
	private MultipartFile dataSetFile;

	private List<MultipartFile> additionalDataSetFiles;

	private MultipartFile goldstandardFile;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String originalFileName) {
		this.fileName = originalFileName;
	}

//	public List<String> getAdditionalFileNames() {
//		return additionalFileNames;
//	}
//
//	public void setAdditionalFileNames(List<String> additionalFileNames) {
//		this.additionalFileNames = additionalFileNames;
//	}

	public String getDataSetType() {
		return dataSetType;
	}

	public void setDataSetType(String dataSetType) {
		this.dataSetType = dataSetType;
	}

	public String getDataSetFormat() {
		return dataSetFormat;
	}

	public void setDataSetFormat(String dataSetFormat) {
		this.dataSetFormat = dataSetFormat;
	}

	public MultipartFile getDataSetFile() {
		return dataSetFile;
	}

	public void setDataSetFile(MultipartFile dataSetFile) {
		this.dataSetFile = dataSetFile;
	}

	public List<MultipartFile> getAdditionalDataSetFiles() {
		return additionalDataSetFiles;
	}

	public void setAdditionalDataSetFiles(List<MultipartFile> additionalDataSetFiles) {
		this.additionalDataSetFiles = additionalDataSetFiles;
	}

	public MultipartFile getGoldstandardFile() {
		return goldstandardFile;
	}

	public void setGoldstandardFile(MultipartFile goldstandardFile) {
		this.goldstandardFile = goldstandardFile;
	}

	public void parse(final ISerializableDataConfig dataConfig) {
		setName(dataConfig.getName());

		ISerializableDataSet<IDataSet> dataSet = dataConfig.getDatasetConfig().getDataSet();

		List<String> fileNames = new ArrayList<>();
		// TODO: data set can now contain multiple files; this is the original way of
		// retrieving the file name, using the data config; does this still make sense?
		fileNames.add(dataConfig.getName());
//		for (File addFile : dataSet.getAdditionalDataSetFiles())
//			fileNames.add(addFile.getName());

//		setAdditionalFileNames(fileNames);
		setVersion(dataConfig.getVersion());
		setAlias(dataConfig.getDatasetConfig().getDataSet().getAlias());
		if (dataConfig.getDatasetConfig() != null && dataConfig.getDatasetConfig().getDataSet() != null
				&& dataConfig.getDatasetConfig().getDataSet().getDatasetFormat() != null)
			setDataSetFormat(dataConfig.getDatasetConfig().getDataSet().getDatasetFormat().toString());
		if (dataConfig.getDatasetConfig() != null && dataConfig.getDatasetConfig().getDataSet() != null
				&& dataConfig.getDatasetConfig().getDataSet().getDatasetType() != null)
			setDataSetType(dataConfig.getDatasetConfig().getDataSet().getDatasetType().toString());
	}
}

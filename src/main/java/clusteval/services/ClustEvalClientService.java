package clusteval.services;

import java.rmi.ConnectException;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.cli.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import de.clusteval.auth.IClientPermissions;
import de.clusteval.auth.UnknownClientException;
import de.clusteval.serverclient.ClustEvalClient;
import de.clusteval.utils.IncompatibleClustEvalVersionException;

@Service
@ControllerAdvice
public class ClustEvalClientService {

	@Value("${clustevalServerIP}")
	private String ip;

	@Value("${clustevalServerPort}")
	private int port;

	@Value("${clientId}")
	private String clientId;

	private ClustEvalClient client;

	private boolean serverWasRestarted;
	private boolean newClientIDassigned;

	public ClustEvalClient getClustEvalClient()
			throws IncompatibleClustEvalVersionException, ParseException, RemoteException {
		try {
			try {
				if (this.client == null) {
					this.client = new ClustEvalClient(ip, port, clientId);
					this.client.connectToServer();
				}
				this.client.getClientPermissions();
				return this.client;
			} catch (NoSuchObjectException e) {
				// the server has been restarted and the current reference is
				// invalid -> setting the reference to null such that the next
				// request creates a new client
				System.out.println("The reference to the clusteval backend server is invalid -> setting client to null");
				serverWasRestarted = true;
				this.client = null;
				throw e;
			}
		} catch (UnknownClientException e) {
			System.out.println(String.format(
					"Unknown Client ID provided (%s) -> Reaquiring new Client ID (non-administrator) from server",
					clientId));
			ClustEvalClient client = new ClustEvalClient(ip, port);
			this.client = client;
			this.client.connectToServer();
			clientId = client.getClientId();
			System.out.println(String.format("Received new Client ID from server: %s", clientId));
			newClientIDassigned = true;
			return client;
		}
	}

	@ModelAttribute(name = "clustevalServerIP")
	public String getIp() {
		return ip;
	}

	@ModelAttribute(name = "clustevalServerPort")
	public int getPort() {
		return port;
	}

	@ModelAttribute(name = "clientId")
	public String getClientId() {
		return clientId;
	}

	@ModelAttribute(name = "permissions")
	public IClientPermissions getPermissions()
			throws ConnectException, NoSuchObjectException, RemoteException, UnknownClientException, Exception {
		try {
			return getClustEvalClient().getClientPermissions();
		} catch (Exception e) {
			return null;
		}
	}

	public boolean isServerWasRestarted() {
		return serverWasRestarted;
	}

	public void setServerWasRestarted(boolean serverWasRestarted) {
		this.serverWasRestarted = serverWasRestarted;
	}

	public boolean isNewClientIDassigned() {
		return newClientIDassigned;
	}

	public void setNewClientIDassigned(boolean newClientIDassigned) {
		this.newClientIDassigned = newClientIDassigned;
	}

	@ModelAttribute(name = "absRepoPath")
	public String getRepositoryAbsolutePath() {
		try {
			return this.getClustEvalClient().getAbsoluteRepositoryPath();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	@ModelAttribute(name = "connected")
	public boolean isConnected() {
		try {
			this.getClustEvalClient().getClientPermissions();
			return true;
		} catch (RemoteException e) {
		} catch (Exception e) {
		}
		return false;
	}

	@ModelAttribute
	public void connectionMessages(Model model) {

		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		String requestURI = request.getRequestURI();
		if (requestURI.equals("/getRunStatus"))
			return;

		ExtendedModelMap m = (ExtendedModelMap) model;
		List<String> failures, infos, successes;
		if (m.containsKey("failure"))
			failures = (List) m.get("failure");
		else
			failures = new ArrayList<String>();
		if (m.containsKey("info"))
			infos = (List) m.get("info");
		else
			infos = new ArrayList<String>();
		if (m.containsKey("success"))
			successes = (List) m.get("success");
		else
			successes = new ArrayList<String>();

		if (isServerWasRestarted()) {
			failures.add("The server has been restarted and your current Client ID expired.");
			setServerWasRestarted(false);

			if (!isNewClientIDassigned()) {
				infos.add("Refresh this page to retrieve a new Client ID.");
			}
		}

		if (isNewClientIDassigned()) {
			infos.add("You have been assigned a new Client ID.");
			setNewClientIDassigned(false);
		}

		m.put("failure", failures);
		m.put("info", infos);
		m.put("success", successes);
	}
}

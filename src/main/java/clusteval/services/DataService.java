package clusteval.services;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.rmi.ConnectException;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import clusteval.models.DataCreation;
import de.clusteval.auth.OperationNotPermittedException;
import de.clusteval.auth.UnknownClientException;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.data.SerializableDataConfig;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.IDataSet.WEBSITE_VISIBILITY;
import de.clusteval.data.dataset.IDataSetConfig;
import de.clusteval.data.dataset.ISerializableDataSet;
import de.clusteval.data.dataset.SerializableAbsoluteDataSet;
import de.clusteval.data.dataset.SerializableDataSetConfig;
import de.clusteval.data.dataset.SerializableRelativeDataSet;
import de.clusteval.data.dataset.format.SerializableConversionInputToStandardConfiguration;
import de.clusteval.data.dataset.format.SerializableDataSetFormat;
import de.clusteval.data.dataset.type.SerializableDataSetType;
import de.clusteval.data.distance.SerializableDistanceMeasure;
import de.clusteval.data.goldstandard.IGoldStandard;
import de.clusteval.data.goldstandard.IGoldStandardConfig;
import de.clusteval.data.goldstandard.SerializableGoldStandard;
import de.clusteval.data.goldstandard.SerializableGoldStandardConfig;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import dk.sdu.imada.compbio.utils.SimilarityMatrix.NUMBER_PRECISION;

@Service
public class DataService {

	@Autowired
	ClustEvalClientService clientService;

	public SerializableDataConfig createData(DataCreation dataCreation, ISerializableDataConfig editedDataConfig)
			throws Exception {
		/*
		 * Data set
		 */
		boolean uploadedAnyFile = false, editedAnyAttributes = false;

		byte[] bytesDataSetFile = null;
		ByteArrayOutputStream baos;
		Writer writer;
		BufferedReader br;
		String currentLine;

		MultipartFile inputFile = dataCreation.getDataSetFile();
		if (!inputFile.isEmpty()) {
			uploadedAnyFile = true;
			baos = new ByteArrayOutputStream();
			writer = new OutputStreamWriter(baos);
			br = new BufferedReader(new InputStreamReader(inputFile.getInputStream()));
			currentLine = "";
			while ((currentLine = br.readLine()) != null) {
				if (!currentLine.startsWith("//")) {
					writer.write(currentLine + "\n");
				}
			}
			writer.close();
			bytesDataSetFile = baos.toByteArray();
		}

		List<byte[]> bytesAdditionalFiles = new ArrayList<>();
		for (MultipartFile addFile : dataCreation.getAdditionalDataSetFiles()) {
			if (!addFile.isEmpty()) {
				uploadedAnyFile = true;
				baos = new ByteArrayOutputStream();
				writer = new OutputStreamWriter(baos);
				br = new BufferedReader(new InputStreamReader(addFile.getInputStream()));
				currentLine = "";
				while ((currentLine = br.readLine()) != null) {
					if (!currentLine.startsWith("//")) {
						writer.write(currentLine + "\n");
					}
				}
				writer.close();
				bytesAdditionalFiles.add(baos.toByteArray());
			}
		}

		String fullDataSetName;
		
		if (editedDataConfig != null) {
			editedAnyAttributes |= !dataCreation.getAlias()
					.equals(editedDataConfig.getDatasetConfig().getDataSet().getAlias());
			editedAnyAttributes |= !dataCreation.getDataSetType()
					.equals(editedDataConfig.getDatasetConfig().getDataSet().getDatasetType().toString());
			editedAnyAttributes |= !dataCreation.getDataSetFormat()
					.equals(editedDataConfig.getDatasetConfig().getDataSet().getDatasetFormat().toString());
			fullDataSetName = dataCreation.getName() + "/" + editedDataConfig.getDatasetConfig().getDataSet().getMinorName();
		} else {
			fullDataSetName = dataCreation.getName() + "/" + dataCreation.getDataSetFile().getOriginalFilename();
		}

		String nextVersion;
		ISerializableDataSet<? extends IDataSet> dataSet;

		if (uploadedAnyFile || editedAnyAttributes) {
			nextVersion = clientService.getClustEvalClient().getNextVersionForRepositoryObject(IDataSet.class,
					fullDataSetName, dataCreation.getVersion());

			String[] split = dataCreation.getDataSetFormat().split(":");
			SerializableDataSetFormat format = new SerializableDataSetFormat(null, split[0], split[1], split[1]);
			split = dataCreation.getDataSetType().split(":");
			SerializableDataSetType type = new SerializableDataSetType(null, split[0], split[1], split[1]);

			if (clientService.getClustEvalClient().isRelativeDataSetFormat(format))
				dataSet = new SerializableRelativeDataSet(null, fullDataSetName, nextVersion, dataCreation.getAlias(),
						format, type, WEBSITE_VISIBILITY.SHOW_ALWAYS);
			else
				dataSet = new SerializableAbsoluteDataSet(null, fullDataSetName, nextVersion, dataCreation.getAlias(),
						format, type, WEBSITE_VISIBILITY.SHOW_ALWAYS);

			if (uploadedAnyFile) {
				clientService.getClustEvalClient().uploadDataSet(dataSet, bytesDataSetFile, bytesAdditionalFiles);
			} else {
				clientService.getClustEvalClient().createNewDatasetWithAttributesFromExisting(
						(ISerializableDataSet) dataSet, editedDataConfig.getDatasetConfig().getDataSet());
			}
		} else if (editedDataConfig != null) {
			// only if we didn't edit any attributes of the data set directly at all we take
			// the old object
			dataSet = editedDataConfig.getDatasetConfig().getDataSet();
		} else {
			throw new IllegalArgumentException("The original data configuration to be edited could not be found");
		}

		/*
		 * Dataset config
		 */
		nextVersion = clientService.getClustEvalClient().getNextVersionForRepositoryObject(IDataSetConfig.class,
				dataCreation.getName(), dataCreation.getVersion());

		SerializableDataSetConfig datasetConfig = new SerializableDataSetConfig(null, dataCreation.getName(),
				nextVersion,
				// TODO: retrieve distance measure from server
				new SerializableConversionInputToStandardConfiguration(
						new SerializableDistanceMeasure(null, "EuclidianDistanceMeasure", "2", "Euclidian Distance"),
						NUMBER_PRECISION.DOUBLE, new ArrayList<>(), new ArrayList<>()),
				dataSet);

		clientService.getClustEvalClient().uploadDataSetConfig(datasetConfig);

		/*
		 * Gold standard
		 */
		SerializableGoldStandardConfig gsConfig = null;
		if (!dataCreation.getGoldstandardFile().isEmpty()) {
			nextVersion = clientService.getClustEvalClient().getNextVersionForRepositoryObject(IGoldStandard.class,
					dataCreation.getName(), dataCreation.getVersion());

			SerializableGoldStandard goldStandard = new SerializableGoldStandard(null,
					dataCreation.getName() + "/" + dataCreation.getGoldstandardFile().getOriginalFilename(),
					nextVersion);

			baos = new ByteArrayOutputStream();
			writer = new OutputStreamWriter(baos);

			br = new BufferedReader(new InputStreamReader(dataCreation.getGoldstandardFile().getInputStream()));
			currentLine = "";
			while ((currentLine = br.readLine()) != null) {
				if (!currentLine.startsWith("//")) {
					writer.write(currentLine + "\n");
				}
			}
			writer.close();
			clientService.getClustEvalClient().uploadGoldStandard(goldStandard, baos.toByteArray());

			/*
			 * Gold standard configuration
			 */
			nextVersion = clientService.getClustEvalClient().getNextVersionForRepositoryObject(
					IGoldStandardConfig.class, dataCreation.getName(), dataCreation.getVersion());
			gsConfig = new SerializableGoldStandardConfig(null, dataCreation.getName(), nextVersion, goldStandard);

			clientService.getClustEvalClient().uploadGoldStandardConfig(gsConfig);
		}
		/*
		 * Data config
		 */
		nextVersion = clientService.getClustEvalClient().getNextVersionForRepositoryObject(IDataConfig.class,
				dataCreation.getName(), dataCreation.getVersion());
		SerializableDataConfig serializableDataConfig = new SerializableDataConfig(null, dataCreation.getName(),
				nextVersion, datasetConfig, null, gsConfig);
		clientService.getClustEvalClient().uploadDataConfig(serializableDataConfig);
		return serializableDataConfig;
	}

	public SerializableDataConfig editData(DataCreation dataCreation) throws Exception {
		ISerializableDataConfig dataConfig = clientService.getClustEvalClient()
				.getDataConfiguration(dataCreation.getName(), dataCreation.getVersion());

		try {
			return createData(dataCreation, dataConfig);
		} catch (Exception e) {
			throw (e);
		}
	}

	public boolean deleteDataset(final String name, final String version) throws ConnectException,
			NoSuchObjectException, RemoteException, ObjectNotRegisteredException, ObjectVersionNotRegisteredException,
			IOException, OperationNotPermittedException, UnknownClientException, Exception {
		return clientService.getClustEvalClient()
				.deleteDataset(clientService.getClustEvalClient().getDataSet(name, version));
		// try {
		// BufferedReader br = new BufferedReader(new FileReader(getPath() +
		// "/data/configs/" + name + ".dataconfig"));
		// String currentLine;
		//
		// while ((currentLine = br.readLine()) != null) {
		// if (currentLine.startsWith("datasetConfig")) {
		// BufferedReader br2 = new BufferedReader(new FileReader(
		// getPath() + "/data/datasets/configs/" +
		// currentLine.split("=")[1].trim() + ".dsconfig"));
		// String currentLine2;
		// while ((currentLine2 = br2.readLine()) != null) {
		// if (currentLine2.startsWith("datasetName")) {
		// // Delete dataset folder
		// String directoryName = currentLine2.split("=")[1].trim();
		// File directory = new File(getPath() + "/data/datasets/" +
		// directoryName);
		// FileUtils.deleteDirectory(directory);
		// }
		// }
		//
		// // Delete dataset configuration file
		// File configurationFile = new File(
		// getPath() + "/data/datasets/configs/" +
		// currentLine.split("=")[1].trim() + ".dsconfig");
		// configurationFile.delete();
		// }
		// if (currentLine.startsWith("goldstandardConfig")) {
		// BufferedReader br2 = new BufferedReader(new FileReader(getPath() +
		// "/data/goldstandards/configs/"
		// + currentLine.split("=")[1].trim() + ".gsconfig"));
		// String currentLine2;
		// while ((currentLine2 = br2.readLine()) != null) {
		// if (currentLine2.startsWith("goldstandardName")) {
		// // Delete goldstandard folder
		// String directoryName = currentLine2.split("=")[1].trim();
		// File directory = new File(getPath() + "/data/goldstandards/" +
		// directoryName);
		// FileUtils.deleteDirectory(directory);
		// }
		// }
		//
		// // Delete goldstandard configuration file
		// File configurationFile = new File(getPath() +
		// "/data/goldstandards/configs/"
		// + currentLine.split("=")[1].trim() + ".gsconfig");
		// configurationFile.delete();
		// }
		// }
		//
		// // Delete data configuration file
		// File configurationFile = new File(getPath() + "/data/configs/" + name
		// + ".dataconfig");
		// configurationFile.delete();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
	}

	public boolean deleteDataConfiguration(final String name, final String version) throws ConnectException,
			NoSuchObjectException, RemoteException, ObjectNotRegisteredException, ObjectVersionNotRegisteredException,
			IOException, OperationNotPermittedException, UnknownClientException, Exception {
		return clientService.getClustEvalClient()
				.deleteDataConfiguration(clientService.getClustEvalClient().getDataConfiguration(name, version));
	}
}

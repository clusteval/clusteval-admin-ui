package clusteval.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@Service
@ControllerAdvice
public class ClustEvalResultWebsiteService {

	@Value("${clustevalWebsiteHost}")
	private String clustevalWebsiteHost;

	@Value("${clustevalWebsitePort}")
	private int clustevalWebsitePort;

	@Value("${clustevalWebsiteBaseURL}")
	private String clustevalWebsiteBaseURL;

	@ModelAttribute(name = "clustevalWebsiteHost")
	public String getIp() {
		return clustevalWebsiteHost;
	}

	@ModelAttribute(name = "clustevalWebsitePort")
	public int getPort() {
		return clustevalWebsitePort;
	}

	@ModelAttribute(name = "clustevalWebsiteBaseURL")
	public String getClientId() {
		return clustevalWebsiteBaseURL;
	}
}

package clusteval.services;

import java.io.IOException;
import java.net.URISyntaxException;
import java.rmi.ConnectException;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import clusteval.models.Program;
import clusteval.models.ProgramCreation;
import clusteval.models.ProgramCreationParameter;
import clusteval.models.ProgramParameter;
import clusteval.models.ProgramParameterOption;
import de.clusteval.auth.OperationNotPermittedException;
import de.clusteval.auth.UnknownClientException;
import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.context.SerializableContext;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.dataset.format.ISerializableDataSetFormat;
import de.clusteval.data.dataset.format.SerializableDataSetFormat;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;
import de.clusteval.framework.repository.NoRepositoryObjectFinderFoundException;
import de.clusteval.framework.repository.ObjectNotFoundException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotFoundException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.UnknownDynamicComponentException;
import de.clusteval.framework.repository.parse.RepositoryObjectParseException;
import de.clusteval.program.DoubleProgramParameter;
import de.clusteval.program.IProgram;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ISerializableProgram;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.program.ISerializableStandaloneProgram;
import de.clusteval.program.IStandaloneProgram;
import de.clusteval.program.IntegerProgramParameter;
import de.clusteval.program.SerializableProgramConfig;
import de.clusteval.program.SerializableStandaloneProgram;
import de.clusteval.program.SerializableStandaloneProgramConfig;
import de.clusteval.program.StringProgramParameter;
import de.clusteval.program.r.IRProgram;
import de.clusteval.program.r.ISerializableRProgram;
import de.clusteval.program.r.SerializableRProgramConfig;
import de.clusteval.run.runresult.format.SerializableRunResultFormat;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.IncompatibleClustEvalVersionException;
import dk.sdu.imada.compbio.format.Formatter;
import dk.sdu.imada.compbio.utils.StringExt;

@Service
public class ProgramService {

	@Autowired
	ClustEvalClientService clientService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Program getConvertedProgramConfig(String name)
			throws ObjectNotFoundException, ObjectVersionNotFoundException, ConnectException, NoSuchObjectException,
			RemoteException, IncompatibleClustEvalVersionException, ParseException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		Program program = new Program();
		program.setName(name);

		// Get array of parameter eligible for parameter optimization
		String split[] = name.split(":");
		ISerializableProgramConfig<IProgramConfig> pc = clientService.getClustEvalClient()
				.getProgramConfiguration(split[0], split[1]);

		program.setAlias(pc.getProgram().getAlias());

		List<IProgramParameter<?>> optimizationParameters = pc.getOptimizableParameters();

		List<IProgramParameter<?>> parameters = pc.getParameters();

		ArrayList<ProgramParameter> programParameters = new ArrayList<ProgramParameter>();
		for (IProgramParameter<?> param : parameters) {
			ProgramParameter programParameter = new ProgramParameter();
			programParameter.setName(param.getName());

			programParameter.setOptimizable(optimizationParameters.contains(param));

			ArrayList<ProgramParameterOption> programParameterOptions = new ArrayList<ProgramParameterOption>();

			ProgramParameterOption programParameterOption = new ProgramParameterOption();
			programParameterOption.setName("defaultValue");
			programParameterOption.setValue(param.getDefault());
			programParameter.setValue(param.getDefault());
			programParameterOptions.add(programParameterOption);

			programParameterOption = new ProgramParameterOption();
			programParameterOption.setName("minValue");
			programParameterOption.setValue(param.getMinValue());
			programParameter.setMinValue(param.getMinValue());
			programParameterOptions.add(programParameterOption);

			programParameterOption = new ProgramParameterOption();
			programParameterOption.setName("maxValue");
			programParameterOption.setValue(param.getMaxValue());
			programParameter.setMaxValue(param.getMaxValue());
			programParameterOptions.add(programParameterOption);

			programParameterOption = new ProgramParameterOption();
			programParameterOption.setName("options");
			programParameterOption.setValue(StringExt.paste(",", param.getOptions()));
			programParameter.setOptions(StringExt.paste(",", param.getOptions()));
			programParameterOptions.add(programParameterOption);

			programParameter.setDefaultOptions(programParameterOptions);
			programParameters.add(programParameter);
		}

		program.setParameters(programParameters);

		return program;
	}

	public ISerializableProgramConfig<? extends IProgramConfig> createProgram(ProgramCreation programCreation,
			BindingResult bindingResult, ISerializableProgramConfig<IProgramConfig> editedProgramConfig)
			throws RepositoryObjectParseException, RegisterException, DeserializationException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException, NoRepositoryObjectFinderFoundException,
			OperationNotPermittedException, UnknownClientException, DynamicComponentInitializationException,
			UnknownDynamicComponentException, Exception {
		boolean uploadedAnyFile = !programCreation.getExecutableFile().isEmpty();
		ISerializableProgram<? extends IProgram> program = null;
		if (uploadedAnyFile) {
			if (!programCreation.getIsRProgram()) {
				// Invocation format is required for this type of program
				if (programCreation.getInvocationFormat().isEmpty()) {
					bindingResult.rejectValue("invocationFormat", "invocationFormat",
							"Invocation format is required for this type of program");
					return null;
				}
			}

			byte[] bytes = programCreation.getExecutableFile().getBytes();
			if (programCreation.getIsRProgram()) {
				ISerializableWrapperDynamicComponent wrapper = clientService.getClustEvalClient()
						.uploadDynamicComponent(IRProgram.class,
								programCreation.getExecutableFile().getOriginalFilename(), bytes);
				program = clientService.getClustEvalClient().getProgram(wrapper.getName(), wrapper.getVersion());
			} else {
				String nextVersion = clientService.getClustEvalClient().getNextVersionForRepositoryObject(
						IStandaloneProgram.class,
						programCreation.getName() + "/" + programCreation.getExecutableFile().getOriginalFilename(),
						programCreation.getVersion());
				program = new SerializableStandaloneProgram(null, nextVersion,
						programCreation.getExecutableFile().getOriginalFilename(), programCreation.getName(),
						new SerializableContext(null, "ClusteringContext", "1", "Clustering Context"),
						programCreation.getAlias());
				clientService.getClustEvalClient().uploadStandaloneProgram((SerializableStandaloneProgram) program,
						bytes);
			}
		} else if (editedProgramConfig != null) {
			program = editedProgramConfig.getProgram();
		} else {
			throw new IllegalArgumentException("The original program configuration to be edited could not be found");
		}

		try {
			/*
			 * Program configuration
			 */
			ISerializableProgramConfig<? extends IProgramConfig> programConfig;

			if (programCreation.getIsRProgram()) {
				String nextVersion = clientService.getClustEvalClient().getNextVersionForRepositoryObject(
						IProgramConfig.class, programCreation.getName(), programCreation.getVersion());

				programConfig = new SerializableRProgramConfig(null, programCreation.getName(), nextVersion,
						(ISerializableRProgram) program,
						new ArrayList<IProgramParameter<?>>(((ISerializableRProgram) program).getParameters()), false,
						-1);
				clientService.getClustEvalClient().uploadProgramConfig(programConfig);
			} else {
				String nextVersion = clientService.getClustEvalClient().getNextVersionForRepositoryObject(
						IProgramConfig.class, programCreation.getName(), programCreation.getVersion());

				List<ISerializableDataSetFormat> compatibleDataSetFormats = new ArrayList<>();
				for (String dsFormat : programCreation.getCompatibleDataSetFormats()) {
					String[] format = dsFormat.split(":");
					compatibleDataSetFormats.add(new SerializableDataSetFormat(null, format[0], format[1], format[1]));
				}
				String[] outputFormatSplit = programCreation.getOutputFormat().split(":");
				SerializableRunResultFormat outputFormat = new SerializableRunResultFormat(null, outputFormatSplit[0],
						outputFormatSplit[1], outputFormatSplit[1]);

				List<IProgramParameter<?>> parameters = new ArrayList<>();
				List<IProgramParameter<?>> optimizationParameters = new ArrayList<>();
				for (ProgramCreationParameter p : programCreation.getParameters()) {
					IProgramParameter<?> param = null;
					switch (p.getType()) {
					case 0:
						param = new StringProgramParameter(p.getName(), p.getDescription(),
								p.getOptions().toArray(new String[0]), p.getDefaultValue());
						break;
					case 1:
						if (p.getOptions() == null || p.getOptions().isEmpty())
							param = new IntegerProgramParameter(p.getName(), p.getDescription(), p.getMinValue(),
									p.getMaxValue(), p.getDefaultValue());
						else
							param = new IntegerProgramParameter(p.getName(), p.getDescription(),
									p.getOptions().toArray(new String[0]), p.getDefaultValue());
						break;
					case 2:
						if (p.getOptions() == null || p.getOptions().isEmpty())
							param = new DoubleProgramParameter(p.getName(), p.getDescription(), p.getMinValue(),
									p.getMaxValue(), p.getDefaultValue());
						else
							param = new DoubleProgramParameter(p.getName(), p.getDescription(),
									p.getOptions().toArray(new String[0]), p.getDefaultValue());
						break;
					}
					if (param != null) {
						parameters.add(param);
						if (p.getOptimizable())
							optimizationParameters.add(param);
					}
				}

				programConfig = new SerializableStandaloneProgramConfig(null, programCreation.getName(), nextVersion,
						compatibleDataSetFormats, outputFormat, (ISerializableStandaloneProgram) program,
						programCreation.getInvocationFormat(), programCreation.getInvocationFormat(),
						programCreation.getInvocationFormat(), programCreation.getInvocationFormat(), parameters,
						optimizationParameters,
						// for now no environmental variables, max running time
						// and normalized data set
						false, -1, programCreation.getAlias(), new HashMap<String, String>());
				clientService.getClustEvalClient().uploadProgramConfig(programConfig);
			}
			return programConfig;
		} catch (Exception e) {
			throw (e);
		}
	}

	public ISerializableProgramConfig<? extends IProgramConfig> editProgram(ProgramCreation programCreation,
			BindingResult bindingResult) throws Exception {
		ISerializableProgramConfig<IProgramConfig> programConfig = clientService.getClustEvalClient()
				.getProgramConfiguration(programCreation.getName(), programCreation.getVersion());

		try {
			return createProgram(programCreation, bindingResult, programConfig);
		} catch (Exception e) {
			throw (e);
		}
	}

	public boolean deleteProgram(String name, String version) throws ConnectException, NoSuchObjectException,
			RemoteException, ObjectNotRegisteredException, ObjectVersionNotRegisteredException, IOException,
			OperationNotPermittedException, UnknownClientException, Exception {
		// try {
		// BufferedReader br = new BufferedReader(new FileReader(getPath() +
		// "/programs/configs/" + name + ".config"));
		// String currentLine;
		//
		// while ((currentLine = br.readLine()) != null) {
		// if (currentLine.startsWith("type")) {
		// // Delete program jar file
		// File jarFile = new File(getPath() + "/programs/" +
		// currentLine.split("=")[1].trim() + ".jar");
		//
		// jarFile.delete();
		// }
		// if (currentLine.startsWith("program")) {
		// // Delete program folder
		// String directoryName =
		// currentLine.split("=")[1].split("/")[0].trim();
		// File directory = new File(getPath() + "/programs/" + directoryName);
		// FileUtils.deleteDirectory(directory);
		// }
		// }
		//
		// // Delete program configuration file
		// File configurationFile = new File(getPath() + "/programs/configs/" +
		// name + ".config");
		// configurationFile.delete();
		// } catch (Exception e) {
		// }
		return clientService.getClustEvalClient()
				.deleteProgram(clientService.getClustEvalClient().getProgram(name, version));
	}

	public boolean deleteProgramConfiguration(String name, String version) throws ConnectException,
			NoSuchObjectException, RemoteException, ObjectNotRegisteredException, ObjectVersionNotRegisteredException,
			IOException, OperationNotPermittedException, UnknownClientException, Exception {
		return clientService.getClustEvalClient()
				.deleteProgramConfiguration(clientService.getClustEvalClient().getProgramConfiguration(name, version));
	}

	public long getExpectedRuntime(ISerializableProgramConfig<?> programConfig, int datasetSize, int iterations)
			throws UnknownContextException, UnknownClusteringQualityMeasureException, UnknownDataSetFormatException,
			UnknownDataSetTypeException, UnknownDistanceMeasureException, RepositoryObjectDumpException,
			DynamicComponentInitializationException, RegisterException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, RemoteException, IncompatibleClustEvalVersionException, IOException,
			URISyntaxException, InterruptedException, ParseException, DeserializationException {
		clientService.getClustEvalClient().ensureRuntimeInformationForAllProgramConfigs();
		return clientService.getClustEvalClient().getExpectedRuntime(programConfig, datasetSize, iterations);
	}
}

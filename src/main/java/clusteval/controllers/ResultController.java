package clusteval.controllers;

import java.io.IOException;
import java.rmi.ConnectException;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import clusteval.models.DataAnalysisResult;
import clusteval.models.DataAnalysisResultData;
import clusteval.models.DataAnalysisResultDataStatistic;
import clusteval.models.ParameterOptimizationResult;
import clusteval.models.ParameterOptimizationResultData;
import clusteval.models.ParameterOptimizationResultProgram;
import clusteval.models.ParameterOptimizationResultQuality;
import clusteval.models.Result;
import clusteval.services.ClustEvalClientService;
import de.clusteval.auth.OperationNotPermittedException;
import de.clusteval.auth.UnknownClientException;
import de.clusteval.cluster.ICluster;
import de.clusteval.cluster.IClusterItem;
import de.clusteval.cluster.ISerializableClustering;
import de.clusteval.cluster.quality.IClusteringQualityMeasureValue;
import de.clusteval.cluster.quality.ISerializableClusteringQualityMeasure;
import de.clusteval.cluster.quality.ISerializableClusteringQualitySet;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.data.dataset.DataMatrix;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IDoubleProgramParameter;
import de.clusteval.program.IIntegerProgramParameter;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.program.ParameterSet;
import de.clusteval.run.IParameterOptimizationRun;
import de.clusteval.run.ISerializableParameterOptimizationRun;
import de.clusteval.run.RUN_TYPE;
import de.clusteval.run.UnknownRunTypeException;
import de.clusteval.run.runresult.IClusteringRunResult;
import de.clusteval.run.runresult.IParameterOptimizationResult;
import de.clusteval.run.runresult.ISerializableClusteringRunResult;
import de.clusteval.run.runresult.ISerializableDataAnalysisRunResult;
import de.clusteval.run.runresult.ISerializableParameterOptimizationRunResult;
import de.clusteval.run.runresult.UnknownRunResultException;
import de.clusteval.utils.ISerializableStatistic;
import de.clusteval.utils.IncompatibleClustEvalVersionException;
import dk.sdu.imada.compbio.utils.Pair;

@Controller
public class ResultController {
	@Autowired
	ClustEvalClientService clientService;

	/* Overview page with all run results */
	@RequestMapping(value = "/results")
	public String index(Model model) throws RemoteException, IncompatibleClustEvalVersionException, ParseException,
			OperationNotPermittedException, UnknownClientException {
		ArrayList<Result> results = new ArrayList<Result>();

		for (String id : clientService.getClustEvalClient().getParameterOptimizationRunResultIdentifiers()) {
			Result result = new Result();
			result.setName(id);
			result.setRunType("Parameter Optimization");
			results.add(result);
		}
		for (String id : clientService.getClustEvalClient().getClusteringRunResultIdentifiers()) {
			Result result = new Result();
			result.setName(id);
			result.setRunType("Clustering");
			results.add(result);
		}
		for (String id : clientService.getClustEvalClient().getDataAnalysisRunResultIdentifiers()) {
			Result result = new Result();
			result.setName(id);
			result.setRunType("Data Analysis");
			results.add(result);
		}
		for (String id : clientService.getClustEvalClient().getRunAnalysisRunResultIdentifiers()) {
			Result result = new Result();
			result.setName(id);
			result.setRunType("Run Analysis");
			results.add(result);
		}
		for (String id : clientService.getClustEvalClient().getRunDataAnalysisRunResultIdentifiers()) {
			Result result = new Result();
			result.setName(id);
			result.setRunType("Run-Data Analysis");
			results.add(result);
		}

		// newest results first
		Collections.sort(results, new Comparator<Result>() {
			@Override
			public int compare(Result o1, Result o2) {
				return o1.getName().compareTo(o2.getName());
			}
		}.reversed());

		model.addAttribute("results", results);

		return "results/index";
	}

	/*
	 * Display page for run results. Calls a more specialized function based on run
	 * type
	 */
	@RequestMapping(value = "/results/show")
	public String show(Model model, @RequestParam(value = "name", required = true) String name)
			throws RemoteException, IncompatibleClustEvalVersionException, ParseException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, OperationNotPermittedException, UnknownClientException,
			RepositoryObjectSerializationException, UnknownRunTypeException, UnknownRunResultException {

		RUN_TYPE runType = this.clientService.getClustEvalClient().getRunTypeOfRunResult(name);

		if (runType.equals(RUN_TYPE.PARAMETER_OPTIMIZATION)) {
			return showResultsParameterOptimization(model, name);
		} else if (runType.equals(RUN_TYPE.CLUSTERING)) {
			return showResultsClustering(model, name);
		} else if (runType.equals(RUN_TYPE.DATA_ANALYSIS)) {
			return showResultsDataAnalysis(model, name);
		}
		model.addAttribute(runType);

		return "results/show";
	}

	/* Display page for parameter optimization run results */
	public String showResultsParameterOptimization(Model model, String name)
			throws RepositoryObjectSerializationException, RemoteException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, OperationNotPermittedException, UnknownClientException,
			IncompatibleClustEvalVersionException, ParseException, UnknownRunTypeException, UnknownRunResultException {
		Collection<ISerializableParameterOptimizationRunResult<IParameterOptimizationResult>> runResults = (Collection) this.clientService
				.getClustEvalClient().getRunResult(name);

		ParameterOptimizationResult result = new ParameterOptimizationResult();
		result.setName(name);

		Map<ISerializableProgramConfig<? extends IProgramConfig>, ParameterOptimizationResultProgram> programs = new HashMap<>();

		ParameterOptimizationResultProgram resultProgram = new ParameterOptimizationResultProgram();

		int dataCounter = 0;
		int programCounter = 0;

		Map<String, Map<String, Map<String, String>>> bestQuals = new HashMap<>();
		Set<String> qualityMeasures = new TreeSet<>(
		// new Comparator<ISerializableClusteringQualityMeasure>() {
		// @Override
		// public int compare(ISerializableClusteringQualityMeasure o1,
		// ISerializableClusteringQualityMeasure o2) {
		// return o1.getAlias().compareTo(o2.getAlias());
		// }
		// }
		);

		for (ISerializableParameterOptimizationRunResult<IParameterOptimizationResult> runResult : runResults) {
			ISerializableProgramConfig<? extends IProgramConfig> programConfig = runResult.getProgramConfig();
			ISerializableDataConfig dataConfig = runResult.getDataConfig();

			if (programs.containsKey(programConfig))
				resultProgram = programs.get(programConfig);
			else {
				resultProgram = new ParameterOptimizationResultProgram();
				resultProgram.setName(programConfig.toString());
				resultProgram.setAlias(programConfig.getProgram().hasAlias() ? programConfig.getProgram().getAlias()
						: programConfig.getProgram().toString());
				resultProgram.setId(programCounter++);
				result.addToPrograms(resultProgram);
			}
			ParameterOptimizationResultData resultData = new ParameterOptimizationResultData();
			resultData.setName(dataConfig.toString());
			resultData.setAlias(dataConfig.getDatasetConfig().getDataSet().getAlias());
			resultData.setId(dataCounter++);

			Map<ISerializableClusteringQualityMeasure, Long> optimalIterations = runResult.getOptimalIterations();
			HashMap<ISerializableClusteringQualityMeasure, IClusteringQualityMeasureValue> qualities = runResult
					.getOptimalCriterionValue().getQualities();

			for (ISerializableClusteringQualityMeasure m : qualities.keySet()) {
				long iteration = optimalIterations.get(m);
				ParameterSet parameterSet = runResult.getIterationToParameterSets().get(iteration);
				ParameterOptimizationResultQuality resultQuality = new ParameterOptimizationResultQuality();
				resultQuality.setAlias(m.getAlias());
				resultQuality.setName(m.getName());
				resultQuality.setVersion(m.getVersion());
				resultQuality.setValue(qualities.get(m).toString());
				resultQuality.setParameterSet(parameterSet.entrySet().stream()
						.map(e -> String.format("%s=%s", e.getKey(), e.getValue())).collect(Collectors.joining(",")));
				resultData.addToQualities(resultQuality);
			}
			Collections.sort(resultData.getQualities(), new Comparator<ParameterOptimizationResultQuality>() {
				@Override
				public int compare(ParameterOptimizationResultQuality o1, ParameterOptimizationResultQuality o2) {
					return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
				}
			});
			resultProgram.addToData(resultData);

			String dataAlias = String.format("%s:%s", dataConfig.getDatasetConfig().getDataSet().getAlias(),
					dataConfig.getVersion());
			String programAlias = String.format("%s:%s", programConfig.getProgram().getAlias(),
					programConfig.getVersion());

			if (!bestQuals.containsKey(dataAlias))
				bestQuals.put(dataAlias, new TreeMap<>());
			if (!bestQuals.get(dataAlias).containsKey(programAlias))
				bestQuals.get(dataAlias).put(programAlias, new TreeMap<>());
			for (Entry<ISerializableClusteringQualityMeasure, IClusteringQualityMeasureValue> entry : qualities
					.entrySet()) {
				bestQuals.get(dataAlias).get(programAlias).put(entry.getKey().getAlias(), entry.getValue().toString());
			}
			if (qualityMeasures.isEmpty()) {
				for (ISerializableClusteringQualityMeasure m : qualities.keySet())
					qualityMeasures.add(m.toString(true));
			}
		}

		for (ParameterOptimizationResultProgram p : result.getPrograms()) {

			Collections.sort(p.getData(), new Comparator<ParameterOptimizationResultData>() {
				@Override
				public int compare(ParameterOptimizationResultData o1, ParameterOptimizationResultData o2) {
					return o1.getAlias().toLowerCase().compareTo(o2.getAlias().toLowerCase());
				}
			});
		}

		Collections.sort(result.getPrograms(), new Comparator<ParameterOptimizationResultProgram>() {
			@Override
			public int compare(ParameterOptimizationResultProgram o1, ParameterOptimizationResultProgram o2) {
				return o1.getAlias().toLowerCase().compareTo(o2.getAlias().toLowerCase());
			}
		});

		// warnings of run execution
		Collection<String> runWarnings = clientService.getClustEvalClient().getRunExceptions(name, true);

		model.addAttribute("runType",
				this.clientService.getClustEvalClient().getRunTypeOfRunResult(name).toString(true));
		model.addAttribute("result", result);
		model.addAttribute("runWarnings", runWarnings);

		model.addAttribute("bestQuals", bestQuals);
		model.addAttribute("qualityMeasures", qualityMeasures);

		return "results/showParameterOptimization";
	}

	/* Display page for clustering run results */
	public String showResultsClustering(Model model, String name)
			throws RemoteException, ObjectNotRegisteredException, ObjectVersionNotRegisteredException,
			OperationNotPermittedException, UnknownClientException, IncompatibleClustEvalVersionException,
			ParseException, RepositoryObjectSerializationException, UnknownRunTypeException, UnknownRunResultException {
		Collection<ISerializableClusteringRunResult> runResults = (Collection) this.clientService.getClustEvalClient()
				.getRunResult(name);

		ParameterOptimizationResult result = new ParameterOptimizationResult();
		result.setName(name);

		Map<ISerializableProgramConfig<? extends IProgramConfig>, ParameterOptimizationResultProgram> programs = new HashMap<>();

		for (ISerializableClusteringRunResult<IClusteringRunResult> runResult : runResults) {
			ISerializableProgramConfig<? extends IProgramConfig> programConfig = runResult.getProgramConfig();
			ISerializableDataConfig dataConfig = runResult.getDataConfig();
			HashMap<ISerializableClusteringQualityMeasure, IClusteringQualityMeasureValue> qualities = runResult
					.getClustering().getQualities().getQualities();

			ParameterOptimizationResultProgram resultProgram;
			if (programs.containsKey(programConfig))
				resultProgram = programs.get(programConfig);
			else {
				resultProgram = new ParameterOptimizationResultProgram();
				resultProgram.setName(programConfig.toString());
				resultProgram.setAlias(programConfig.getProgram().getAlias());
				result.addToPrograms(resultProgram);
			}
			ParameterOptimizationResultData resultData = new ParameterOptimizationResultData();
			resultData.setName(dataConfig.toString());
			resultData.setAlias(dataConfig.getDatasetConfig().getDataSet().getAlias());

			for (ISerializableClusteringQualityMeasure m : qualities.keySet()) {
				ParameterOptimizationResultQuality resultQuality = new ParameterOptimizationResultQuality();
				resultQuality.setAlias(m.getAlias());
				resultQuality.setName(m.getName());
				resultQuality.setVersion(m.getVersion());
				resultQuality.setValue(qualities.get(m).toString());
				resultData.addToQualities(resultQuality);
			}

			Collections.sort(resultData.getQualities(), new Comparator<ParameterOptimizationResultQuality>() {
				@Override
				public int compare(ParameterOptimizationResultQuality o1, ParameterOptimizationResultQuality o2) {
					return o1.getName().compareTo(o2.getName());
				}
			});
			resultProgram.addToData(resultData);
		}

		for (ParameterOptimizationResultProgram p : result.getPrograms()) {

			Collections.sort(p.getData(), new Comparator<ParameterOptimizationResultData>() {
				@Override
				public int compare(ParameterOptimizationResultData o1, ParameterOptimizationResultData o2) {
					return o1.getName().compareTo(o2.getName());
				}
			});
		}

		Collections.sort(result.getPrograms(), new Comparator<ParameterOptimizationResultProgram>() {
			@Override
			public int compare(ParameterOptimizationResultProgram o1, ParameterOptimizationResultProgram o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		model.addAttribute("runType",
				this.clientService.getClustEvalClient().getRunTypeOfRunResult(name).toString(true));
		model.addAttribute("result", result);

		return "results/clustering/show";
	}

	/* Display page for data analysis run results */
	public String showResultsDataAnalysis(Model model, String name)
			throws RemoteException, IncompatibleClustEvalVersionException, ParseException,
			OperationNotPermittedException, UnknownClientException, RepositoryObjectSerializationException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException {
		ISerializableDataAnalysisRunResult runResult = (ISerializableDataAnalysisRunResult) this.clientService
				.getClustEvalClient().getRunResult(name).iterator().next();

		DataAnalysisResult result = new DataAnalysisResult();
		result.setName(name);

		for (IDataConfig dataConfig : runResult.getStatistics().keySet()) {
			DataAnalysisResultData resultDataConfig = new DataAnalysisResultData();
			resultDataConfig.setName(dataConfig.getName());

			for (ISerializableStatistic<IDataStatistic> statistic : runResult.getStatistics().get(dataConfig)
					.keySet()) {
				DataAnalysisResultDataStatistic resultDataStatistic = new DataAnalysisResultDataStatistic();

				Map<String, Object> statsOfDataConfiguration = clientService.getClustEvalClient()
						.getStatsOfDataConfiguration(dataConfig.getName(), dataConfig.getVersion().toString());
				if (statsOfDataConfiguration.containsKey("numberOfSamples"))
					resultDataConfig.setNumberOfSamples((Integer) statsOfDataConfiguration.get("numberOfSamples"));
				if (statsOfDataConfiguration.containsKey("dimensionality"))
					resultDataConfig.setDimensionality((Integer) statsOfDataConfiguration.get("dimensionality"));

				resultDataConfig.setHasGoldstandard(dataConfig.hasGoldStandardConfig());

				// if (currentDataStatistic.equals("ClassSizeDistributionDataStatistic")) {
				// resultDataConfig.setClassSizeDistribution(resultDataStatistic);
				// } else if
				// (currentDataStatistic.equals("NodeDegreeDistributionDataStatistic")) {
				// resultDataConfig.setNodeDegreeDistribution(resultDataStatistic);
				// } else if
				// (currentDataStatistic.equals("IntraInterDistributionDataStatistic")) {
				// resultDataConfig.setIntraInterSimilarityDistribution(resultDataStatistic);
				// } else if
				// (currentDataStatistic.equals("SimilarityDistributionDataStatistic")) {
				// resultDataConfig.setSimilarityDistribution(resultDataStatistic);
				// } else {
				// resultDataConfig.addToDataStatistics(resultDataStatistic);
				// }
				// } else {
				// if (!dataStatistic.equals(currentDataStatistic)) {
				// currentDataStatistic = dataStatistic;
				// resultDataStatistic = new DataAnalysisResultDataStatistic();
				// resultDataStatistic.setName(currentDataStatistic);
				// resultDataStatistic.setAlias(dataStatisticAlias);
				// if (currentDataStatistic.equals("ClassSizeDistributionDataStatistic")) {
				// resultDataConfig.setClassSizeDistribution(resultDataStatistic);
				// } else if
				// (currentDataStatistic.equals("NodeDegreeDistributionDataStatistic")) {
				// resultDataConfig.setNodeDegreeDistribution(resultDataStatistic);
				// } else if
				// (currentDataStatistic.equals("IntraInterDistributionDataStatistic")) {
				// resultDataConfig.setIntraInterSimilarityDistribution(resultDataStatistic);
				// } else if
				// (currentDataStatistic.equals("SimilarityDistributionDataStatistic")) {
				// resultDataConfig.setSimilarityDistribution(resultDataStatistic);
				// } else {
				// resultDataConfig.addToDataStatistics(resultDataStatistic);
				// }
				// }
				// }

				// Read value from file
				// String statisticFile =
				// clientService.getClustEvalClient().getAbsoluteRepositoryPath() + "/results/"
				// + name + "/analyses/" + currentDataConfig + "_" + dataStatistic + ".txt";
				// resultDataStatistic.setFilePath(statisticFile);
				// try {
				// BufferedReader br = new BufferedReader(new FileReader(statisticFile));
				// String currentLine;
				// while ((currentLine = br.readLine()) != null) {
				// resultDataStatistic.setValue(currentLine);
				// }
				// } catch (FileNotFoundException e) {
				// resultDataStatistic.setValue("Error. File not found.");
				// } catch (Exception e) {
				// }
				resultDataStatistic.setName(statistic.getName());
				resultDataStatistic.setAlias(statistic.getAlias());
				resultDataStatistic.setValue(runResult.getStatistics().get(dataConfig).get(statistic));
				resultDataConfig.addToDataStatistics(resultDataStatistic);
			}
			result.addToDataConfigs(resultDataConfig);
		}

		for (DataAnalysisResultData dataAnalysisResultData : result.getDataConfigs()) {
			Collections.sort(dataAnalysisResultData.getDataStatistics());
		}

		model.addAttribute("result", result);

		return "results/data_analysis/show";
	}

	/* Display page for clustering graphs */
	@RequestMapping(value = "/results/show-clustering")
	public String showClustering(Model model, @RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "program", required = true) String program,
			@RequestParam(value = "data", required = true) String data,
			@RequestParam(value = "param-set", required = false) String paramSet) {
		model.addAttribute("name", name);
		model.addAttribute("program", program);
		model.addAttribute("data", data);

		if (paramSet == null || paramSet.equals("")) {
			return "results/clustering/clusteringGraph";
		} else {
			model.addAttribute("paramSet", paramSet);
			return "results/showClustering";
		}
	}

	/*
	 * Find clustering for parameter optimization runs Figures out which files to
	 * fetch for the clustering graph
	 */
	@RequestMapping(value = "/results/load-clustering")
	public String loadClustering(Model model, @RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "program", required = true) String program,
			@RequestParam(value = "data", required = true) String data,
			@RequestParam(value = "param-set", required = true) String paramSet)
			throws RemoteException, IncompatibleClustEvalVersionException, ParseException,
			OperationNotPermittedException, UnknownClientException, RepositoryObjectSerializationException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException {
		Collection<ISerializableParameterOptimizationRunResult<IParameterOptimizationResult>> runResults = (Collection) this.clientService
				.getClustEvalClient().getRunResult(name);

		ISerializableParameterOptimizationRunResult<IParameterOptimizationResult> result = null;
		for (ISerializableParameterOptimizationRunResult<IParameterOptimizationResult> r : runResults)
			if (r.getDataConfig().toString().equals(data) && r.getProgramConfig().toString().equals(program)) {
				result = r;
				break;
			}

		Map<String, String> parameterValues = new HashMap<>();
		for (String s : StringUtils.split(paramSet, ',')) {
			String[] split = s.split("=");
			parameterValues.put(split[0], split[1]);
		}

		Map<Long, ParameterSet> iterationToParameterSets = result.getIterationToParameterSets();

		long correctIteration = -1;
		for (long iteration : iterationToParameterSets.keySet()) {
			ParameterSet parameterSet = iterationToParameterSets.get(iteration);

			// check for locked parameters
			boolean satisfiedLocked = true;
			for (String p : parameterSet.keySet()) {
				if (!parameterValues.get(p).equals(parameterSet.get(p))) {
					satisfiedLocked = false;
					break;
				}
			}

			if (satisfiedLocked) {
				correctIteration = iteration;
				break;
			}
		}

		model.addAttribute("iteration", correctIteration);
		model.addAttribute("name", name);
		model.addAttribute("program", program);
		model.addAttribute("data", data);

		return "results/loadClustering";
	}

	/*
	 * Find clustering for clustering runs Figures out which files to fetch for the
	 * clustering graph
	 */
	@RequestMapping(value = "/results/load-clustering-cluster")
	public String loadClustering(Model model, @RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "program", required = true) String program,
			@RequestParam(value = "data", required = true) String data)
			throws RemoteException, IncompatibleClustEvalVersionException, ParseException,
			OperationNotPermittedException, UnknownClientException, RepositoryObjectSerializationException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException {
		model.addAttribute("name", name);
		model.addAttribute("program", program);
		model.addAttribute("data", data);
		model.addAttribute("iteration", "-1");

		return "results/loadClustering";
	}

	/*
	 * Given a cluster file and a pca file Create a CSV file for Highcharts To
	 * display
	 */
	@RequestMapping(value = "/results/get-clustering")
	public void getClustering(HttpServletResponse response, @RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "program", required = true) String program,
			@RequestParam(value = "data", required = true) String data,
			@RequestParam(value = "iteration", required = true) long iteration)
			throws RepositoryObjectSerializationException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, OperationNotPermittedException, UnknownClientException,
			IncompatibleClustEvalVersionException, ParseException, UnknownRunResultException, IOException {
		// We want to generate a CSV file, so we set the header accordingly
		response.setContentType("application/csv");
		response.setHeader("content-disposition", "attachment;filename =filename.csv");

		// this indicates that the clustering is of a clustering run and not parameter
		// optimization run
		ISerializableClustering clustering;
		if (iteration == -1) {
			clustering = this.clientService.getClustEvalClient().getClusteringOfClusteringRun(name, program, data);
		} else {
			clustering = this.clientService.getClustEvalClient().getClusteringOfParameterOptimizationIteration(name,
					program, data, iteration);
		}

		DataMatrix pcaOfDataConfig = this.clientService.getClustEvalClient().getPCAOfDataConfig(name, program, data, 2);
		String[] ids = pcaOfDataConfig.getIds();
		double[][] data2 = pcaOfDataConfig.getData();

		Map<String, double[]> idToCoords = new HashMap<>();
		for (int i = 0; i < ids.length; i++) {
			idToCoords.put(ids[i], data2[i]);
		}

		Set<ICluster> sortedClusters = new TreeSet<>(new Comparator<ICluster>() {
			@Override
			public int compare(ICluster o1, ICluster o2) {
				return o1.getId().compareTo(o2.getId());
			}
		});
		sortedClusters.addAll(clustering.getClusters());

		try {
			ServletOutputStream writer = response.getOutputStream();

			for (ICluster cluster : sortedClusters) {
				writer.print("Cluster " + cluster.getId() + "\n");

				for (IClusterItem clusterObject : cluster.getFuzzyItems().keySet()) {
					double[] ds = idToCoords.get(clusterObject.getId());
					writer.print(clusterObject.getId() + "," + ds[0] + "," + ds[1] + "\n");
				}
			}

			writer.flush();
			writer.close();
		} catch (Exception e) {
		}
	}

	public String showResults(Model model, @RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "program", required = true) String program,
			@RequestParam(value = "data", required = true) String data, boolean cluster)
			throws RepositoryObjectSerializationException, RemoteException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, OperationNotPermittedException, UnknownClientException,
			IncompatibleClustEvalVersionException, ParseException {
		Collection<ISerializableParameterOptimizationRunResult<IParameterOptimizationResult>> runResults = (Collection) this.clientService
				.getClustEvalClient().getRunResult(name);

		ISerializableParameterOptimizationRunResult<IParameterOptimizationResult> result = null;
		for (ISerializableParameterOptimizationRunResult<IParameterOptimizationResult> r : runResults)
			if (r.getDataConfig().toString().equals(data) && r.getProgramConfig().toString().equals(program)) {
				result = r;
				break;
			}

		Map<Long, ParameterSet> iterationToParameterSets = result.getIterationToParameterSets();

		Map<String, Set<String>> paramValues = new HashMap<>();

		for (ParameterSet paramSet : iterationToParameterSets.values()) {
			for (String param : paramSet.keySet()) {
				boolean isNumeric = (result.getProgramConfig()
						.getParameterForName(param) instanceof IDoubleProgramParameter
						|| result.getProgramConfig().getParameterForName(param) instanceof IIntegerProgramParameter);
				if (!paramValues.containsKey(param))
					paramValues.put(param, new TreeSet<String>(new Comparator<String>() {

						private Comparator<String> natural = Comparator.naturalOrder();

						public int compare(String o1, String o2) {
							if (isNumeric) {
								return Double.valueOf(o1).compareTo(Double.valueOf(o2));
							}
							int lengthComp = o1.length() - o2.length();
							if (lengthComp != 0)
								return lengthComp;
							return natural.compare(o1, o2);
						};
					}));
				paramValues.get(param).add(paramSet.get(param));
			}
		}

		model.addAttribute("parameters", paramValues);
		model.addAttribute("name", name);
		model.addAttribute("program", program);
		model.addAttribute("data", data);

		if (!cluster) {
			return "results/showParameterSlider";
		} else {
			return "results/showClusteringSlider";
		}
	}

	@RequestMapping(value = "/results/get-parameter-sliders")
	public String showParameterSliders(Model model, @RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "program", required = true) String program,
			@RequestParam(value = "data", required = true) String data)
			throws RepositoryObjectSerializationException, RemoteException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, OperationNotPermittedException, UnknownClientException,
			IncompatibleClustEvalVersionException, ParseException {
		return showResults(model, name, program, data, false);
	}

	@RequestMapping(value = "/results/get-clustering-sliders")
	public String showClusteringSliders(Model model, @RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "program", required = true) String program,
			@RequestParam(value = "data", required = true) String data)
			throws RepositoryObjectSerializationException, RemoteException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, OperationNotPermittedException, UnknownClientException,
			IncompatibleClustEvalVersionException, ParseException {
		return showResults(model, name, program, data, true);
	}

	@RequestMapping(value = "/results/get-parameter-graph")
	public void getParameterGraph(Model model,
			@RequestParam(value = "active-parameter", required = true) String activeParameter,
			@RequestParam(value = "parameters", required = true) String lockedParametersJoined,
			@RequestParam(value = "name") String name, @RequestParam(value = "program") String program,
			@RequestParam(value = "data") String data, HttpServletResponse response)
			throws RepositoryObjectSerializationException, RemoteException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, OperationNotPermittedException, UnknownClientException,
			IncompatibleClustEvalVersionException, ParseException {
		// We want to generate a CSV file, so we set the header accordingly
		response.setContentType("application/csv");
		response.setHeader("content-disposition", "attachment;filename =filename.csv");

		Collection<ISerializableParameterOptimizationRunResult<IParameterOptimizationResult>> runResults = (Collection) this.clientService
				.getClustEvalClient().getRunResult(name);

		ISerializableParameterOptimizationRunResult<IParameterOptimizationResult> result = null;
		for (ISerializableParameterOptimizationRunResult<IParameterOptimizationResult> r : runResults)
			if (r.getDataConfig().toString().equals(data) && r.getProgramConfig().toString().equals(program)) {
				result = r;
				break;
			}

		Map<Long, ParameterSet> iterationToParameterSets = result.getIterationToParameterSets();
		Map<Long, ISerializableClusteringQualitySet> iterationToQualities = result.getIterationToQualities();

		List<ISerializableClusteringQualityMeasure> measuresOrdered = ((ISerializableParameterOptimizationRun<IParameterOptimizationRun>) result
				.getRun()).getQualityMeasures();

		Map<String, String> lockedParameters = new HashMap<>();
		for (String s : StringUtils.split(lockedParametersJoined, ',')) {
			String[] split = s.split("=");
			lockedParameters.put(split[0], split[1]);
		}

		Set<String> writtenValuesForParameter = new HashSet<>();

		try {
			ServletOutputStream writer = response.getOutputStream();

			writer.print(activeParameter + "," + measuresOrdered.stream()
					.map(m -> String.format("%s:%s", m.getAlias(), m.getVersion())).collect(Collectors.joining(","))
					+ "\n");

			for (long iteration : iterationToParameterSets.keySet()) {
				if (!iterationToParameterSets.containsKey(iteration))
					continue;
				ParameterSet parameterSet = iterationToParameterSets.get(iteration);
				if (!iterationToQualities.containsKey(iteration))
					continue;
				HashMap<ISerializableClusteringQualityMeasure, IClusteringQualityMeasureValue> qualities = iterationToQualities
						.get(iteration).getQualities();
				String activeParameterValue = parameterSet.get(activeParameter);

				// check for locked parameters
				boolean satisfiedLocked = true;
				for (String p : lockedParameters.keySet()) {
					if (!parameterSet.get(p).equals(lockedParameters.get(p))) {
						satisfiedLocked = false;
						break;
					}
				}

				if (!satisfiedLocked)
					continue;

				// TODO: we should take the average or something like that
				if (writtenValuesForParameter.contains(activeParameterValue))
					continue;

				StringBuilder sb = new StringBuilder();
				for (ISerializableClusteringQualityMeasure m : measuresOrdered) {
					IClusteringQualityMeasureValue q = qualities.get(m);
					if (q != null && q.hasValue())
						if (Double.isNaN(q.getValue())) {
							// sb.append("null,");
							sb.append(",");
						} else
							sb.append(q + ",");
				}
				if (sb.length() > 0)
					sb.deleteCharAt(sb.length() - 1);

				writer.print(activeParameterValue + "," + sb.toString() + "\n");

				writtenValuesForParameter.add(activeParameterValue);
			}

			// String currentValue = "";
			// String oldValue = "";
			// int qualityIndex = 0;
			// for (Map row : rows) {
			// if (currentValue.equals("")) {
			// currentValue = new String((byte[]) row.get("value"));
			// oldValue = currentValue;
			// } else {
			// currentValue = new String((byte[]) row.get("value"));
			// }
			//
			// // Check if we have reached a new value
			// if (!currentValue.equals(oldValue)) {
			// // A new row should be written out at this point
			// writer.print(oldValue + "," + StringUtils.join(rowToPrint, ',') + "\n");
			// qualityIndex = 0;
			// oldValue = currentValue;
			// rowToPrint = new ArrayList<String>();
			// }
			//
			// // If there are missing quality values, write out 0's
			// currentQualityId = (int) row.get("quality_id");
			// while (currentQualityId > qualities.get(qualityIndex)) {
			// rowToPrint.add("0");
			// qualityIndex++;
			// }
			// rowToPrint.add(Double.toString((double) row.get("quality")));
			// qualityIndex++;
			// }
			//
			// writer.print(currentValue + "," + StringUtils.join(rowToPrint, ',') + "\n");

			writer.flush();
			writer.close();
		} catch (IOException e) {
		}
	}

	@RequestMapping(value = "/run-result/delete")
	public String delete(@RequestParam(value = "name", required = true) String name,
			RedirectAttributes redirectAttributes) throws ConnectException, NoSuchObjectException, RemoteException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException, IOException,
			OperationNotPermittedException, UnknownClientException, Exception {
		if (clientService.getClustEvalClient().deleteRunResult(name)) {
			redirectAttributes.addFlashAttribute("success",
					Arrays.asList("The run result '" + name + "' was successfully scheduled for deletion."));
		} else {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("The run result '" + name + "' could not be deleted."));
		}

		redirectAttributes.addFlashAttribute("redirectUrl", "/results");

		return "redirect:/redirect";
	}

}

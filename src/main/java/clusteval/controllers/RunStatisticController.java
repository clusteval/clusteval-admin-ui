package clusteval.controllers;

import java.rmi.ConnectException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import clusteval.services.ClustEvalClientService;
import de.clusteval.framework.repository.ISerializableWrapper;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.run.statistics.IRunStatistic;
import de.clusteval.serverclient.ClustEvalClient;

@Controller
public class RunStatisticController {
	// @Autowired
	// ProgramService programService;

	@Autowired
	ClustEvalClientService clientService;

	@RequestMapping(value = "/run_statistics")
	public String showPrograms(Model model) {
		ArrayList<ISerializableWrapper<IRunStatistic>> qualityMeasures;

		try {
			ClustEvalClient ClustEvalClient = clientService.getClustEvalClient();
			qualityMeasures = new ArrayList<ISerializableWrapper<IRunStatistic>>(ClustEvalClient.getRunStatistics());
			Collections.sort(qualityMeasures);

			model.addAttribute("statistics", qualityMeasures);
		} catch (ConnectException e) {
			return "runs/notRunning";
		} catch (Exception e) {

		}

		return "run_statistics/index";
	}

	@RequestMapping(value = "/run_statistics/delete")
	public String delete(Model model, @RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "version", required = true) String version, RedirectAttributes redirectAttributes) {
		try {
			ClustEvalClient ClustEvalClient = clientService.getClustEvalClient();
			ClustEvalClient.deleteDynamicComponent(IRunStatistic.class, name, version);

			redirectAttributes.addFlashAttribute("success", Arrays
					.asList("The component '" + name + ":" + version + "' was successfully scheduled for deletion."));
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("failure", Arrays
					.asList("The component '" + name + ":" + version + "' could not be deleted: " + e.getMessage()));
			return "redirect:/";
		}

		redirectAttributes.addFlashAttribute("redirectUrl", "/run_statistics");

		return "redirect:/redirect";
	}
}

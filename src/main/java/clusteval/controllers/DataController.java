package clusteval.controllers;

import java.io.IOException;
import java.rmi.ConnectException;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import clusteval.models.DataCreation;
import clusteval.services.ClustEvalClientService;
import clusteval.services.DataService;
import de.clusteval.auth.OperationNotPermittedException;
import de.clusteval.auth.UnknownClientException;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.data.SerializableDataConfig;
import de.clusteval.data.dataset.ISerializableDataSet;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.type.IDataSetType;
import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.serverclient.ClustEvalClient;
import de.clusteval.utils.VersionStringComparator;
import dk.sdu.imada.compbio.utils.Pair;

@Controller
public class DataController {
	@Autowired
	DataService dataService;

	@Autowired
	ClustEvalClientService clientService;

	@RequestMapping(value = "/data")
	public String index(Model model, RedirectAttributes redirectAttributes) {
		Map<Pair<String, String>, Map<String, ISerializableDataConfig>> data;
		Map<String, Map<String, ISerializableDataSet>> datasets;

		try {
			ClustEvalClient ClustEvalClient = clientService.getClustEvalClient();
			data = new TreeMap<Pair<String, String>, Map<String, ISerializableDataConfig>>(
					new Comparator<Pair<String, String>>() {
						@Override
						public int compare(Pair<String, String> o1, Pair<String, String> o2) {
							return (o1.getSecond().toLowerCase() + " " + o1.getFirst().toLowerCase())
									.compareTo((o2.getSecond().toLowerCase() + " " + o2.getFirst().toLowerCase()));
						}
					});
			for (ISerializableDataConfig obj : ClustEvalClient.getDataConfigurations()) {
				Pair<String, String> p = Pair.getPair(obj.getName(), obj.getDatasetConfig().getDataSet().getAlias());
				if (!(data.containsKey(p)))
					data.put(p, new TreeMap<>(new VersionStringComparator().reversed()));
				data.get(p).put(obj.getVersion(), obj);
			}

			model.addAttribute("datas", data);

			datasets = new TreeMap<String, Map<String, ISerializableDataSet>>(String.CASE_INSENSITIVE_ORDER);
			for (ISerializableDataSet obj : ClustEvalClient.getDataSets()) {
				if (!(datasets.containsKey(obj.getName())))
					datasets.put(obj.getName(), new TreeMap<>(new VersionStringComparator().reversed()));
				datasets.get(obj.getName()).put(obj.getVersion(), obj);
			}

			model.addAttribute("datasets", datasets);

			model.addAttribute("parsingErrors",
					new TreeMap<>(ClustEvalClient.getParsingExceptionMessages(IDataConfig.class)));

		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("connected", false);
			redirectAttributes.addFlashAttribute("failure", Arrays.asList(e.getMessage()));
			return "redirect:/";
		}

		return "data/index";
	}

	@RequestMapping(value = "/data/show")
	public String show(Model model, @RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "version", required = true) String version) {
		ISerializableDataConfig dataConfig;
		try {
			dataConfig = clientService.getClustEvalClient().getDataConfiguration(name, version);
			boolean isRelative = dataConfig.getDatasetConfig().getDataSet().isRelative();
			model.addAttribute("dataConfig", dataConfig);
			model.addAttribute("isRelative", isRelative);

			Map<String, Object> stats = clientService.getClustEvalClient().getStatsOfDataConfiguration(name, version);
			model.addAttribute("numberOfSamples", stats.get("numberOfSamples"));
			if (!isRelative)
				model.addAttribute("dimensionality", stats.get("dimensionality"));
		} catch (ConnectException e) {
			return "runs/notRunning";
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "data/show";
	}

	@RequestMapping(value = "/data/upload")
	public String uploadData(DataCreation dataCreation, Model model) {
		try {
			populateModel(model);
		} catch (Exception e) {
			return "runs/notRunning";
		}
		return "data/upload";
	}

	@RequestMapping(value = "/data/upload", method = RequestMethod.POST)
	public String uploadDataPOST(@Valid DataCreation dataCreation, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes) {
		// Return to form if there were validation errors
		if (bindingResult.hasErrors()) {
			try {
				populateModel(model);
			} catch (ConnectException e) {
				return "runs/notRunning";
			}
			return "data/upload";
		}

		try {
			dataCreation.setFileName(dataCreation.getDataSetFile().getOriginalFilename());
			// dataCreation.setAdditionalFileNames(dataCreation.getAdditionalDataSetFiles().stream()
			// .map(f -> f.getOriginalFilename()).collect(Collectors.toList()));

			SerializableDataConfig dc = dataService.createData(dataCreation, null);
			redirectAttributes.addFlashAttribute("success",
					Arrays.asList(String.format("Data set '%s' uploaded successfully and saved as version '%s'.",
							dc.getName(), dc.getVersion())));
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("dataCreation", dataCreation);
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("There was an error when processing your data: " + e.getMessage()));
		}

		return "redirect:/data";
	}

	@RequestMapping(value = "/data/edit")
	public String editData(DataCreation dataCreation, Model model,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "version", required = true) String version)
			throws ConnectException, NoSuchObjectException, Exception {

		try {
			populateModel(model);
			model.addAttribute("name", name);
			model.addAttribute("version", version);
		} catch (ConnectException e) {
			return "runs/notRunning";
		}

		dataCreation.parse(clientService.getClustEvalClient().getDataConfiguration(name, version));

		return "data/edit";
	}

	@RequestMapping(value = "/data/edit", method = RequestMethod.POST)
	public String editDataPOST(@Valid DataCreation dataCreation, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes) {
		boolean ignoreErrors = true;

		// Ignore validation errors if only data set file is missing
		for (FieldError error : bindingResult.getFieldErrors()) {
			if (!error.getField().equals("dataSetFile")) {
				ignoreErrors = false;
			}
		}

		// Return to form if there were validation errors
		if (bindingResult.hasErrors() && !ignoreErrors) {
			try {
				populateModel(model);
			} catch (ConnectException e) {
				return "runs/notRunning";
			}
			return "data/edit";
		}

		try {
			SerializableDataConfig serDataConfig = dataService.editData(dataCreation);
			redirectAttributes.addFlashAttribute("success",
					Arrays.asList(
							String.format("You succcesfully edited version '%s' of data '%s'.",
									dataCreation.getVersion(), dataCreation.getName()),
							String.format("Your edited data was saved as version '%s' of data '%s'.",
									serDataConfig.getVersion(), serDataConfig.getName())));
			return "redirect:/data";
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("An error occurred when editing the data: " + e.getMessage()));
		}

		return "redirect:/data/upload";
	}

	@RequestMapping(value = "/datasets/delete")
	public String deleteData(@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "version", required = true) String version, RedirectAttributes redirectAttributes)
			throws ConnectException, NoSuchObjectException, RemoteException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, IOException, OperationNotPermittedException, UnknownClientException,
			Exception {
		if (dataService.deleteDataset(name, version)) {
			redirectAttributes.addFlashAttribute("success", Arrays
					.asList("The data set '" + name + ":" + version + "' was successfully scheduled for deletion."));
		} else {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("The data set '" + name + ":" + version + "' could not be deleted."));
		}

		redirectAttributes.addFlashAttribute("redirectUrl", "/data#datasets");

		return "redirect:/redirect";
	}

	@RequestMapping(value = "/data-config/delete")
	public String deleteDataConfig(@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "version", required = true) String version, RedirectAttributes redirectAttributes)
			throws ConnectException, NoSuchObjectException, RemoteException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, IOException, OperationNotPermittedException, UnknownClientException,
			Exception {
		if (dataService.deleteDataConfiguration(name, version)) {
			redirectAttributes.addFlashAttribute("success", Arrays.asList(
					"The data configuration '" + name + ":" + version + "' was successfully scheduled for deletion."));
		} else {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("The data configuration '" + name + ":" + version + "' could not be deleted."));
		}

		redirectAttributes.addFlashAttribute("redirectUrl", "/data#dataconfigs");

		return "redirect:/redirect";
	}

	private void populateModel(Model model) throws ConnectException {
		try {
			ClustEvalClient ClustEvalClient = clientService.getClustEvalClient();

			ArrayList<ISerializableWrapperDynamicComponent<IDataSetType>> dataSetTypes = new ArrayList<ISerializableWrapperDynamicComponent<IDataSetType>>(
					ClustEvalClient.getDataSetTypes());
			ArrayList<ISerializableWrapperDynamicComponent<IDataSetFormat>> dataSetFormats = new ArrayList<ISerializableWrapperDynamicComponent<IDataSetFormat>>(
					ClustEvalClient.getDataSetFormats());

			Comparator<ISerializableWrapperDynamicComponent<?>> compDynamicComponent = new Comparator<ISerializableWrapperDynamicComponent<?>>() {
				@Override
				public int compare(ISerializableWrapperDynamicComponent<?> o1,
						ISerializableWrapperDynamicComponent<?> o2) {
					String s1 = o1.hasAlias() ? o1.getAlias().toLowerCase() : o1.toString().toLowerCase();
					String s2 = o2.hasAlias() ? o2.getAlias().toLowerCase() : o2.toString().toLowerCase();
					int res = s1.compareTo(s2);
					if (res == 0)
						// newer versions first
						return o2.getVersion().compareTo(o1.getVersion());
					return res;
				}
			};

			Collections.sort(dataSetTypes, compDynamicComponent);
			Collections.sort(dataSetFormats, compDynamicComponent);

			model.addAttribute("dataSetTypes", dataSetTypes);
			model.addAttribute("dataSetFormats", dataSetFormats);
		} catch (ConnectException e) {
			throw (e);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package clusteval.controllers;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.rmi.RemoteException;
import java.util.Arrays;

import javax.validation.Valid;

import org.apache.commons.cli.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import clusteval.models.Utility;
import clusteval.services.ClustEvalClientService;
import de.clusteval.auth.OperationNotPermittedException;
import de.clusteval.auth.UnknownClientException;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.format.IDataSetFormatParser;
import de.clusteval.data.dataset.type.IDataSetType;
import de.clusteval.data.distance.IDistanceMeasure;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.data.statistics.IDataStatisticCalculator;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.parse.RepositoryObjectParseException;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.run.runresult.format.IRunResultFormatParser;
import de.clusteval.run.statistics.IRunDataStatistic;
import de.clusteval.run.statistics.IRunDataStatisticCalculator;
import de.clusteval.run.statistics.IRunStatistic;
import de.clusteval.run.statistics.IRunStatisticCalculator;
import de.clusteval.utils.IncompatibleClustEvalVersionException;

@Controller
public class UtilityController {

	@Autowired
	ClustEvalClientService clientService;

	@RequestMapping(value = "/utilities")
	public String index(Model model) throws RemoteException, OperationNotPermittedException, UnknownClientException,
			IncompatibleClustEvalVersionException, ParseException {
		model.addAttribute("qualityMeasures", clientService.getClustEvalClient().getClusteringQualityMeasures().size());
		model.addAttribute("generators", clientService.getClustEvalClient().getDataSetGenerators().size());
		model.addAttribute("preprocessors", clientService.getClustEvalClient().getDataPreprocessors().size());
		model.addAttribute("randomizers", clientService.getClustEvalClient().getDataRandomizers().size());
		model.addAttribute("dataFormats", clientService.getClustEvalClient().getDataSetFormats().size());
		model.addAttribute("types", clientService.getClustEvalClient().getDataSetTypes().size());
		model.addAttribute("dataStatistics", clientService.getClustEvalClient().getDataStatistics().size());
		model.addAttribute("distanceMeasures", clientService.getClustEvalClient().getDistanceMeasures().size());
		model.addAttribute("methods", clientService.getClustEvalClient().getParameterOptimizationMethods().size());
		model.addAttribute("resultFormats", clientService.getClustEvalClient().getRunResultFormats().size());
		model.addAttribute("runStatistics", clientService.getClustEvalClient().getRunStatistics().size());
		model.addAttribute("runDataStatistics", clientService.getClustEvalClient().getRunDataStatistics().size());
		return "utilities/index";
	}

	@RequestMapping(value = "/utilities/upload-distance-measure")
	public String uploadDistanceMeasure(Utility utility, Model model) {
		utility.setTypeName("Distance measure");
		return "utilities/uploadDistanceMeasure";
	}

	@RequestMapping(value = "/utilities/upload-distance-measure", method = RequestMethod.POST)
	public String uploadDistanceMeasure(@Valid Utility utility, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			return "utilities/uploadDistanceMeasure";
		}

		try {
			// Copy distance measure file to repository
			if (utility.getFile().isEmpty()) {
				redirectAttributes.addFlashAttribute("failure", Arrays.asList("Failed to store empty file"));
			}

			if (uploadUtility(utility.getFile(), new Class[] { IDistanceMeasure.class })) {
				redirectAttributes.addFlashAttribute("success",
						Arrays.asList("Distance measure uploaded successfully"));
			} else {
				redirectAttributes.addFlashAttribute("failure",
						Arrays.asList("The jar file contained invalid classes"));
			}
		} catch (FileAlreadyExistsException e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Failed to upload distance measure. File already exists"));
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Failed to upload distance measure: " + e.getMessage()));
		}

		return "redirect:/utilities/upload-distance-measure";
	}

	@RequestMapping(value = "/utilities/upload-quality-measure")
	public String uploadQualityMeasure(Utility utility, Model model) {
		utility.setTypeName("Quality measure");
		return "utilities/uploadQualityMeasure";
	}

	@RequestMapping(value = "/utilities/upload-quality-measure", method = RequestMethod.POST)
	public String uploadQualityMeasure(@Valid Utility utility, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			return "utilities/uploadQualityMeasure";
		}

		try {
			// Copy distance measure file to repository
			if (utility.getFile().isEmpty()) {
				redirectAttributes.addFlashAttribute("failure", Arrays.asList("Failed to store empty file"));
			}

			if (uploadUtility(utility.getFile(), new Class[] { IDistanceMeasure.class })) {
				redirectAttributes.addFlashAttribute("success",
						Arrays.asList("Distance measure uploaded successfully"));
			} else {
				redirectAttributes.addFlashAttribute("failure",
						Arrays.asList("The jar file contained invalid classes"));
			}
		} catch (FileAlreadyExistsException e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Failed to upload distance measure. File already exists"));
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Failed to upload distance measure: " + e.getMessage()));
		}

		return "redirect:/utilities/upload-distance-measure";
	}

	@RequestMapping(value = "/utilities/upload-data-set-format")
	public String uploadDataSetFormat(Utility utility, Model model) {
		utility.setTypeName("Data set format");
		return "utilities/uploadDataSetFormat";
	}

	@RequestMapping(value = "/utilities/upload-data-set-format", method = RequestMethod.POST)
	public String uploadDataSetFormat(@Valid Utility utility, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			return "utilities/uploadDataSetFormat";
		}

		try {
			// Copy data set format file to repository
			if (utility.getFile().isEmpty()) {
				redirectAttributes.addFlashAttribute("failure", Arrays.asList("Failed to store empty file"));
			}

			if (uploadUtility(utility.getFile(), new Class[] { IDataSetFormat.class, IDataSetFormatParser.class })) {
				redirectAttributes.addFlashAttribute("success", Arrays.asList("Data set format uploaded successfully"));
			} else {
				redirectAttributes.addFlashAttribute("failure",
						Arrays.asList("The jar file contained invalid classes"));
			}
		} catch (FileAlreadyExistsException e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Failed to upload data set format. File already exists"));
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Failed to upload data set format: " + e.getMessage()));
		}

		return "redirect:/utilities/upload-data-set-format";
	}

	@RequestMapping(value = "/utilities/upload-run-result-format")
	public String uploadRunResultFormat(Utility utility, Model model) {
		utility.setTypeName("Run result format");
		return "utilities/uploadRunResultFormat";
	}

	@RequestMapping(value = "/utilities/upload-run-result-format", method = RequestMethod.POST)
	public String uploadRunResultFormat(@Valid Utility utility, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			return "utilities/uploadRunResultFormat";
		}

		try {
			// Copy run result format file to repository
			if (utility.getFile().isEmpty()) {
				redirectAttributes.addFlashAttribute("failure", Arrays.asList("Failed to store empty file"));
			}

			if (uploadUtility(utility.getFile(),
					new Class[] { IRunResultFormat.class, IRunResultFormatParser.class })) {
				redirectAttributes.addFlashAttribute("success",
						Arrays.asList("Run result format uploaded successfully"));
			} else {
				redirectAttributes.addFlashAttribute("failure",
						Arrays.asList("The jar file contained invalid classes"));
			}
		} catch (FileAlreadyExistsException e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Failed to upload run result format. File already exists"));
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Failed to upload run result format: " + e.getMessage()));
		}

		return "redirect:/utilities/upload-run-result-format";
	}

	@RequestMapping(value = "/utilities/upload-data-set-type")
	public String uploadDataSetType(Utility utility, Model model) {
		utility.setTypeName("Data set type");
		return "utilities/uploadDataSetType";
	}

	@RequestMapping(value = "/utilities/upload-data-set-type", method = RequestMethod.POST)
	public String uploadDataSetType(@Valid Utility utility, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			return "utilities/uploadDataSetType";
		}

		try {
			// Copy data set type file to repository
			if (utility.getFile().isEmpty()) {
				redirectAttributes.addFlashAttribute("failure", Arrays.asList("Failed to store empty file"));
			}

			if (uploadUtility(utility.getFile(), new Class[] { IDataSetType.class })) {
				redirectAttributes.addFlashAttribute("success", Arrays.asList("Data set type uploaded successfully"));
			} else {
				redirectAttributes.addFlashAttribute("failure",
						Arrays.asList("The jar file contained invalid classes"));
			}
		} catch (FileAlreadyExistsException e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Failed to upload data set type. File already exists"));
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Failed to upload data set type: " + e.getMessage()));
		}

		return "redirect:/utilities/upload-data-set-type";
	}

	@RequestMapping(value = "/utilities/upload-data-statistic")
	public String uploadDataStatistic(Utility utility, Model model) {
		utility.setTypeName("Data statistic");
		return "utilities/uploadDataStatistic";
	}

	@RequestMapping(value = "/utilities/upload-data-statistic", method = RequestMethod.POST)
	public String uploadDataStatistic(@Valid Utility utility, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			return "utilities/uploadDataStatistic";
		}

		try {
			// Copy data statistic file to repository
			if (utility.getFile().isEmpty()) {
				redirectAttributes.addFlashAttribute("failure", Arrays.asList("Failed to store empty file"));
			}

			if (uploadUtility(utility.getFile(),
					new Class[] { IDataStatistic.class, IDataStatisticCalculator.class })) {
				redirectAttributes.addFlashAttribute("success", Arrays.asList("Data statistic uploaded successfully"));
			} else {
				redirectAttributes.addFlashAttribute("failure",
						Arrays.asList("The jar file contained invalid classes"));
			}
		} catch (FileAlreadyExistsException e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Failed to upload data statistic. File already exists"));
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("failure", "Failed to upload data statistic: " + e.getMessage());
		}

		return "redirect:/utilities/upload-data-statistic";
	}

	@RequestMapping(value = "/utilities/upload-rundata-statistic")
	public String uploadRunDataStatistic(Utility utility, Model model) {
		utility.setTypeName("Run-data statistic");
		return "utilities/uploadRunDataStatistic";
	}

	@RequestMapping(value = "/utilities/upload-rundata-statistic", method = RequestMethod.POST)
	public String uploadRunDataStatistic(@Valid Utility utility, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			return "utilities/uploadRunDataStatistic";
		}

		try {
			// Copy statistic file to repository
			if (utility.getFile().isEmpty()) {
				redirectAttributes.addFlashAttribute("failure", Arrays.asList("Failed to store empty file"));
			}

			if (uploadUtility(utility.getFile(),
					new Class[] { IRunDataStatistic.class, IRunDataStatisticCalculator.class })) {
				redirectAttributes.addFlashAttribute("success",
						Arrays.asList("Run data statistic uploaded successfully"));
			} else {
				redirectAttributes.addFlashAttribute("failure",
						Arrays.asList("The jar file contained invalid classes"));
			}
		} catch (FileAlreadyExistsException e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Failed to upload run data statistic. File already exists"));
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Failed to upload run data statistic: " + e.getMessage()));
		}

		return "redirect:/utilities/upload-rundata-statistic";
	}

	@RequestMapping(value = "/utilities/upload-run-statistic")
	public String uploadRunStatistic(Utility utility, Model model) {
		utility.setTypeName("Run statistic");
		return "utilities/uploadRunStatistic";
	}

	@RequestMapping(value = "/utilities/upload-run-statistic", method = RequestMethod.POST)
	public String uploadRunStatistic(@Valid Utility utility, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			return "utilities/uploadRunStatistic";
		}

		try {
			// Copy statistic file to repository
			if (utility.getFile().isEmpty()) {
				redirectAttributes.addFlashAttribute("failure", Arrays.asList("Failed to store empty file"));
			}
			if (uploadUtility(utility.getFile(), new Class[] { IRunStatistic.class, IRunStatisticCalculator.class })) {
				redirectAttributes.addFlashAttribute("success", Arrays.asList("Run statistic uploaded successfully"));
			} else {
				redirectAttributes.addFlashAttribute("failure",
						Arrays.asList("The jar file contained invalid classes"));
			}
		} catch (FileAlreadyExistsException e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Failed to upload run statistic. File already exists"));
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Failed to upload run statistic: " + e.getMessage()));
		}

		return "redirect:/utilities/upload-run-statistic";
	}

	private boolean uploadUtility(MultipartFile file, Class[] utilityClasses)
			throws IOException, RepositoryObjectParseException, OperationNotPermittedException, UnknownClientException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException, RegisterException,
			IncompatibleClustEvalVersionException, ParseException {
		clientService.getClustEvalClient().uploadDynamicComponent(utilityClasses[0], file.getOriginalFilename(),
				file.getBytes());

		return true;
	}
}

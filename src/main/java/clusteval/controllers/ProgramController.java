package clusteval.controllers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.ConnectException;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import clusteval.models.Program;
import clusteval.models.ProgramCreation;
import clusteval.services.ClustEvalClientService;
import clusteval.services.ProgramService;
import de.clusteval.auth.OperationNotPermittedException;
import de.clusteval.auth.UnknownClientException;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;
import de.clusteval.framework.repository.ObjectNotFoundException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotFoundException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.program.IProgram;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.ISerializableProgram;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.program.IStandaloneProgram;
import de.clusteval.program.r.IRProgram;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.serverclient.ClustEvalClient;
import de.clusteval.utils.VersionStringComparator;
import dk.sdu.imada.compbio.format.Formatter;
import dk.sdu.imada.compbio.utils.Pair;

@Controller
public class ProgramController {
	@Autowired
	ProgramService programService;

	@Autowired
	ClustEvalClientService clientService;

	@RequestMapping(value = "/programs")
	public String index(Model model, RedirectAttributes redirectAttributes) {
		Map<Pair<String, String>, Map<String, ISerializableProgramConfig>> programConfigs = new TreeMap<>(
				new Comparator<Pair<String, String>>() {
					@Override
					public int compare(Pair<String, String> o1, Pair<String, String> o2) {
						return (o1.getSecond().toLowerCase() + " " + o1.getFirst().toLowerCase())
								.compareTo((o2.getSecond().toLowerCase() + " " + o2.getFirst().toLowerCase()));
					}

				});
		Map<String, Map<String, ISerializableProgram>> programs = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

		try {
			ClustEvalClient ClustEvalClient = clientService.getClustEvalClient();
			for (ISerializableProgramConfig pc : new ArrayList<ISerializableProgramConfig>(
					ClustEvalClient.getProgramConfigurations())) {
				Pair<String, String> p;
				if (pc.getProgram().hasAlias())
					p = Pair.getPair(pc.getName(), pc.getProgram().getAlias());
				else
					p = Pair.getPair(pc.getName(), pc.getProgram().getName());
				if (!(programConfigs.containsKey(p)))
					programConfigs.put(p, new TreeMap<>(new VersionStringComparator().reversed()));
				programConfigs.get(p).put(pc.getVersion(), pc);
			}

			model.addAttribute("programs", programConfigs);

			for (ISerializableProgram p : new ArrayList<ISerializableProgram>(ClustEvalClient.getPrograms())) {
				if (!(programs.containsKey(p.getName())))
					programs.put(p.getName(), new TreeMap<>(new VersionStringComparator().reversed()));
				programs.get(p.getName()).put(p.getVersion(), p);
			}

			model.addAttribute("programs", programConfigs);

			model.addAttribute("executables", programs);

			model.addAttribute("parsingErrors",
					new TreeMap<>(ClustEvalClient.getParsingExceptionMessages(IProgramConfig.class,
							IStandaloneProgram.class, IProgram.class, IRProgram.class)));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("connected", false);
			redirectAttributes.addFlashAttribute("failure", Arrays.asList(e.getMessage()));
			return "redirect:/";
		}

		return "programs/index";
	}

	@RequestMapping(value = "/programs/show")
	public String show(Model model, @RequestParam(value = "name", required = true) String name) {
		ArrayList<String> programConfigContent = new ArrayList<String>();
		try {
			BufferedReader br;
			String currentLine;
			br = new BufferedReader(new FileReader(clientService.getClustEvalClient().getAbsoluteRepositoryPath()
					+ "/programs/configs/" + name + ".config"));

			while ((currentLine = br.readLine()) != null) {
				programConfigContent.add(currentLine);
			}
		} catch (Exception e) {
		}

		model.addAttribute("programConfigContent", programConfigContent);

		return "programs/show";
	}

	@RequestMapping(value = "/getProgram", method = RequestMethod.GET)
	public @ResponseBody Program getProgram(@RequestParam(value = "name", required = true) String name)
			throws ObjectNotFoundException, ObjectVersionNotFoundException, ConnectException, NoSuchObjectException,
			RemoteException, Exception {
		return programService.getConvertedProgramConfig(name);
	}

	@RequestMapping(value = "/program-config-runtime", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Map<String, String> getProgramConfigRuntime(
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "version", required = true) String version,
			@RequestParam(value = "iterations", required = true) int iterations,
			@RequestParam(value = "simplify", required = false) boolean simplify,
			@RequestParam(value = "dataConfigs", required = true) String... dataConfigs) throws ObjectNotFoundException,
			ObjectVersionNotFoundException, ConnectException, NoSuchObjectException, RemoteException, Exception {
		long result = 0l;

		ISerializableProgramConfig<?> programConfiguration = clientService.getClustEvalClient()
				.getProgramConfiguration(name, version);
		if (dataConfigs.length == 1 && dataConfigs[0].equals("null"))
			return Collections.EMPTY_MAP;
		for (String dc : dataConfigs) {
			String[] dcSplit = dc.split(":");
			int datasetSize = (Integer) clientService.getClustEvalClient()
					.getStatsOfDataConfiguration(dcSplit[0], dcSplit[1]).get("numberOfSamples");
			long tmp = programService.getExpectedRuntime(programConfiguration, datasetSize, iterations);
			if (tmp < 0) {
				result = -1l;
				break;
			}
			result += tmp;
		}
		String formattedTime;
		if (result == -1)
			formattedTime = "Not sufficient data to estimate ...";
		else if (simplify) {
			formattedTime = Formatter.formatMsToDuration(result, false, true);
		} else
			formattedTime = Formatter.formatMsToDuration(result);

		Map<String, String> resultMap = new HashMap<>();
		resultMap.put("program", programConfiguration.toString());
		resultMap.put("formattedTime", formattedTime);
		return resultMap;
	}

	@RequestMapping(value = "/programs/upload")
	public String uploadProgram(ProgramCreation programCreation, Model model) {
		try {
			populateModel(model);
		} catch (ConnectException e) {
			return "runs/notRunning";
		}
		return "programs/upload";
	}

	@RequestMapping(value = "/programs/upload", method = RequestMethod.POST)
	public String uploadProgram(@Valid ProgramCreation programCreation, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes) {
		// Return to form if there were validation errors
		if (bindingResult.hasErrors()) {
			try {
				populateModel(model);
			} catch (ConnectException e) {
				return "runs/notRunning";
			}
			return "programs/upload";
		}

		try {
			ISerializableProgramConfig<? extends IProgramConfig> pc = programService.createProgram(programCreation,
					bindingResult, null);

			// Check for errors again (one may have been added)
			if (bindingResult.hasErrors()) {
				try {
					populateModel(model);
				} catch (ConnectException e) {
					return "runs/notRunning";
				}
				return "programs/upload";
			}

			redirectAttributes.addFlashAttribute("success",
					Arrays.asList(String.format("Program '%s' uploaded successfully and saved as version '%s'.",
							pc.getName(), pc.getVersion())));
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Failed to upload program: " + e.getMessage()));
		}

		return "redirect:/programs";
	}

	@RequestMapping(value = "/programs/edit")
	public String editProgram(ProgramCreation programCreation, Model model,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "version", required = true) String version) {
		try {
			populateModel(model);
			model.addAttribute("name", name);
			model.addAttribute("version", version);
		} catch (ConnectException e) {
			return "runs/notRunning";
		}

		try {
			programCreation.parse(clientService.getClustEvalClient().getProgramConfiguration(name, version));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "programs/edit";
	}

	@RequestMapping(value = "/programs/edit", method = RequestMethod.POST)
	public String editProgramPOST(@Valid ProgramCreation programCreation, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes) {
		boolean ignoreErrors = true;

		// Ignore validation errors if only executable file is missing
		for (FieldError error : bindingResult.getFieldErrors()) {
			if (!error.getField().equals("executableFile")) {
				ignoreErrors = false;
			}
		}

		// Return to form if there were validation errors
		if (bindingResult.hasErrors() && !ignoreErrors) {
			try {
				populateModel(model);
			} catch (ConnectException e) {
				return "runs/notRunning";
			}
			return "programs/edit";
		}

		try {
			ISerializableProgramConfig<? extends IProgramConfig> pc = programService.editProgram(programCreation,
					bindingResult);

			redirectAttributes.addFlashAttribute("success",
					Arrays.asList(
							String.format("You succcesfully edited version '%s' of program '%s'.",
									programCreation.getVersion(), programCreation.getName()),
							String.format("Your edited program was saved as version '%s' of program '%s'.",
									pc.getVersion(), pc.getName())));
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("An error occurred when editing the program: " + e.getMessage()));
		}

		return "redirect:/programs";
	}

	@RequestMapping(value = "/executables/delete")
	public String deleteProgram(@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "version", required = true) String version, RedirectAttributes redirectAttributes)
			throws ConnectException, NoSuchObjectException, RemoteException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, IOException, OperationNotPermittedException, UnknownClientException,
			Exception {
		if (programService.deleteProgram(name, version)) {
			redirectAttributes.addFlashAttribute("success", Arrays
					.asList("The program '" + name + ":" + version + "' was successfully scheduled for deletion."));
		} else {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("The program '" + name + ":" + version + "' could not be deleted."));
		}

		redirectAttributes.addFlashAttribute("redirectUrl", "/programs");

		return "redirect:/redirect";
	}

	@RequestMapping(value = "/programs-config/delete")
	public String deleteProgramConfig(@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "version", required = true) String version, RedirectAttributes redirectAttributes)
			throws ConnectException, NoSuchObjectException, RemoteException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, IOException, OperationNotPermittedException, UnknownClientException,
			Exception {
		if (programService.deleteProgramConfiguration(name, version)) {
			redirectAttributes.addFlashAttribute("success", Arrays.asList("The program configuration '" + name + ":"
					+ version + "' was successfully scheduled for deletion."));
		} else {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("The program configuration '" + name + ":" + version + "' could not be deleted."));
		}

		redirectAttributes.addFlashAttribute("redirectUrl", "/programs");

		return "redirect:/redirect";
	}

	private void populateModel(Model model) throws ConnectException {
		try {
			ClustEvalClient ClustEvalClient = clientService.getClustEvalClient();

			ArrayList<ISerializableWrapperDynamicComponent<IDataSetFormat>> compatibleDataSetFormats = new ArrayList<ISerializableWrapperDynamicComponent<IDataSetFormat>>(
					ClustEvalClient.getDataSetFormats());
			ArrayList<ISerializableWrapperDynamicComponent<IRunResultFormat>> outputFormats = new ArrayList<ISerializableWrapperDynamicComponent<IRunResultFormat>>(
					ClustEvalClient.getRunResultFormats());

			Comparator<ISerializableWrapperDynamicComponent<?>> compDynamicComponent = new Comparator<ISerializableWrapperDynamicComponent<?>>() {
				@Override
				public int compare(ISerializableWrapperDynamicComponent<?> o1,
						ISerializableWrapperDynamicComponent<?> o2) {
					String s1 = o1.hasAlias() ? o1.getAlias().toLowerCase() : o1.toString().toLowerCase();
					String s2 = o2.hasAlias() ? o2.getAlias().toLowerCase() : o2.toString().toLowerCase();
					int res = s1.compareTo(s2);
					if (res == 0)
						// newer versions first
						return o2.getVersion().compareTo(o1.getVersion());
					return res;
				}
			};

			Collections.sort(compatibleDataSetFormats, compDynamicComponent);
			Collections.sort(outputFormats, compDynamicComponent);

			model.addAttribute("compatibleDataSetFormats", compatibleDataSetFormats);
			model.addAttribute("outputFormats", outputFormats);
		} catch (ConnectException e) {
			throw (e);
		} catch (Exception e) {
		}
	}
}

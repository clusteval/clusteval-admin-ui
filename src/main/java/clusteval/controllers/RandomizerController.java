package clusteval.controllers;

import java.rmi.ConnectException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import clusteval.models.Parameter;
import clusteval.models.Randomizer;
import clusteval.services.ClustEvalClientService;
import de.clusteval.serverclient.ClustEvalClient;

@Controller
public class RandomizerController {

	@Autowired
	ClustEvalClientService clientService;

	@RequestMapping(value = "/getRandomizer", method = RequestMethod.GET)
	public @ResponseBody Randomizer getRandomizerTest(@RequestParam(value = "name", required = true) String name) {
		Randomizer randomizer = new Randomizer();
		randomizer.setName(name);
		String result = "";
		try {
			ArrayList<Parameter> parameters = new ArrayList<Parameter>();
			ClustEvalClient ClustEvalClient = clientService.getClustEvalClient();

			Options options = ClustEvalClient.getOptionsForDataRandomizer(name);
			Collection<Option> optionValues = options.getOptions();

			for (Option option : optionValues) {
				Parameter param = new Parameter();
				param.setName(option.getArgName());
				param.setDescription(option.getDescription());
				parameters.add(param);
			}
			randomizer.setAvailableParameters(parameters);
		} catch (ConnectException e) {
			result = " There was a connect exception!";
		} catch (Exception e) {
			result = " There was some exception!";
			result += e.toString();
			e.printStackTrace();
		}

		return randomizer;
	}
}

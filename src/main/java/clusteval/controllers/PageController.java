package clusteval.controllers;

import java.io.IOException;
import java.net.URISyntaxException;
import java.rmi.ConnectException;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.cli.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import clusteval.services.ClustEvalClientService;
import clusteval.services.ClustEvalConnectionService;
import de.clusteval.auth.OperationNotPermittedException;
import de.clusteval.auth.UnknownClientException;
import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.serverclient.ClustEvalClient;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.IncompatibleClustEvalVersionException;
import dk.sdu.imada.compbio.format.Formatter;

@Controller
public class PageController {
	@Autowired
	ClustEvalConnectionService connectionService;

	@Autowired
	ClustEvalClientService clientService;

	@Autowired
	Environment springEnv;

	@RequestMapping("/")
	public String homePage(Model model)
			throws UnknownContextException, UnknownClusteringQualityMeasureException, UnknownDataSetFormatException,
			UnknownDataSetTypeException, UnknownDistanceMeasureException, RepositoryObjectDumpException,
			DynamicComponentInitializationException, RegisterException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, RemoteException, IncompatibleClustEvalVersionException, IOException,
			URISyntaxException, InterruptedException, ParseException, RepositoryObjectSerializationException,
			DeserializationException, OperationNotPermittedException, UnknownClientException {

		// Properties props = new Properties();
		// MutablePropertySources propSrcs = ((AbstractEnvironment)
		// springEnv).getPropertySources();
		// StreamSupport.stream(propSrcs.spliterator(), false).filter(ps -> ps
		// instanceof EnumerablePropertySource)
		// .map(ps -> ((EnumerablePropertySource)
		// ps).getPropertyNames()).flatMap(Arrays::<String>stream)
		// .forEach(propName -> props.setProperty(propName,
		// springEnv.getProperty(propName)));
		// System.out.println(props);

		int numberOfRuns = 0;
		int numberOfDataConfigs = 0;
		int numberOfProgramConfigs = 0;

		List<String> errors = new ArrayList<String>();

		try {
			ClustEvalClient ClustEvalClient = clientService.getClustEvalClient();
			numberOfRuns = ClustEvalClient.getRuns().size();
			numberOfDataConfigs = ClustEvalClient.getDataConfigurations().size();
			numberOfProgramConfigs = ClustEvalClient.getProgramConfigurations().size();
		} catch (UnknownHostException | IncompatibleClustEvalVersionException | ConnectException e) {
			model.addAttribute("connectError", e.getMessage());
		} catch (NoSuchObjectException e) {
			model.addAttribute("connectError", "The server has been restarted and your current Client ID expired.");
		} catch (Exception e) {
			e.printStackTrace();
			errors.add(e.getMessage());
		}

		if (!errors.isEmpty())
			model.addAttribute("failure", errors);
		model.addAttribute("numberOfRuns", numberOfRuns);
		model.addAttribute("numberOfDataConfigs", numberOfDataConfigs);
		model.addAttribute("numberOfProgramConfigs", numberOfProgramConfigs);

		return "pages/home";
	}

	@RequestMapping("/redirect")
	public String redirectPage(Model model) {
		return "pages/redirect";
	}

	@RequestMapping("/connect")
	public String connectToServer(Model model, RedirectAttributes redirectAttributes) {
		connectionService.connectToServer();
		redirectAttributes.addFlashAttribute("success", Arrays.asList("The ClustEval server is starting up"));

		return "redirect:/";
	}

	@RequestMapping("/disconnect")
	public String disconnectFromServer(Model model, RedirectAttributes redirectAttributes) {
		try {
			ClustEvalClient ClustEvalClient = clientService.getClustEvalClient();
			ClustEvalClient.shutdownFramework();
			redirectAttributes.addFlashAttribute("success", Arrays.asList("The ClustEval server has been shut down"));
		} catch (ConnectException e) {
			e.printStackTrace();
		} catch (OperationNotPermittedException e) {
			redirectAttributes.addFlashAttribute("failure", Arrays.asList("Operation not permitted"));
		} catch (Exception e) {
			e.printStackTrace();

		}

		return "redirect:/";
	}
}

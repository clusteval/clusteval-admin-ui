package clusteval.controllers;

import java.io.IOException;
import java.rmi.ConnectException;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.validation.Valid;

import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import clusteval.models.Parameter;
import clusteval.models.Program;
import clusteval.models.ProgramParameter;
import clusteval.models.Run;
import clusteval.models.RunCreation;
import clusteval.services.ClustEvalClientService;
import clusteval.services.ProgramService;
import de.clusteval.auth.OperationNotPermittedException;
import de.clusteval.auth.UnknownClientException;
import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod;
import de.clusteval.cluster.paramOptimization.ISerializableParameterOptimizationMethod;
import de.clusteval.cluster.paramOptimization.SerializableParameterOptimizationMethod;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.ISerializableClusteringQualityMeasure;
import de.clusteval.cluster.quality.SerializableClusteringQualityMeasure;
import de.clusteval.context.ISerializableContext;
import de.clusteval.context.SerializableContext;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.data.randomizer.IDataRandomizer;
import de.clusteval.data.randomizer.ISerializableDataRandomizer;
import de.clusteval.data.randomizer.SerializableDataRandomizer;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.data.statistics.SerializableDataStatistic;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.ISerializableWrapper;
import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;
import de.clusteval.framework.repository.ObjectNotFoundException;
import de.clusteval.framework.repository.ObjectVersionNotFoundException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IProgramParameter;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.program.ParameterSet;
import de.clusteval.run.IClusteringRun;
import de.clusteval.run.IDataAnalysisRun;
import de.clusteval.run.IExecutionRun;
import de.clusteval.run.IInternalParameterOptimizationRun;
import de.clusteval.run.IParameterOptimizationRun;
import de.clusteval.run.IRobustnessAnalysisRun;
import de.clusteval.run.IRun;
import de.clusteval.run.IRunAnalysisRun;
import de.clusteval.run.IRunDataAnalysisRun;
import de.clusteval.run.ISerializableRun;
import de.clusteval.run.ISerializableToolRuntimeEvaluationRun;
import de.clusteval.run.IToolRuntimeEvaluationRun;
import de.clusteval.run.RUN_STATUS;
import de.clusteval.run.RUN_TYPE;
import de.clusteval.run.SerializableClusteringRun;
import de.clusteval.run.SerializableDataAnalysisRun;
import de.clusteval.run.SerializableExecutionRun;
import de.clusteval.run.SerializableParameterOptimizationRun;
import de.clusteval.run.SerializableRobustnessAnalysisRun;
import de.clusteval.run.SerializableRunAnalysisRun;
import de.clusteval.run.SerializableRunDataAnalysisRun;
import de.clusteval.run.runresult.postprocessing.ISerializableRunResultPostprocessor;
import de.clusteval.run.statistics.IRunDataStatistic;
import de.clusteval.run.statistics.IRunStatistic;
import de.clusteval.run.statistics.SerializableRunDataStatistic;
import de.clusteval.run.statistics.SerializableRunStatistic;
import de.clusteval.serverclient.ClustEvalClient;
import de.clusteval.utils.ISerializableStatistic;
import de.clusteval.utils.IncompatibleClustEvalVersionException;
import de.clusteval.utils.VersionStringComparator;
import dk.sdu.imada.compbio.utils.Pair;

@Controller
public class RunController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	ClustEvalClientService clientService;

	@Autowired
	ProgramService programService;

	@RequestMapping("/runs")
	public String index(Model model) {
		// ArrayList<IRun> runs = new ArrayList<IRun>();
		// ArrayList<String> runNames = new ArrayList<String>();
		ArrayList<String> runResumes = new ArrayList<String>();
		ArrayList<String> runningRuns;
		ArrayList<String> finishedRuns;
		ArrayList<String> terminatedRuns;
		Map<String, Collection<String>> runErrors = new HashMap<>();
		Map<String, Collection<String>> runWarnings = new HashMap<>();
		Map<ISerializableWrapper<? extends IRepositoryObject>, List<String>> parsingErrors = new TreeMap<>();

		try {
			ClustEvalClient ClustEvalClient = clientService.getClustEvalClient();

			final Map<String, Map<String, ISerializableRun>> runMap = new TreeMap<String, Map<String, ISerializableRun>>(
					String.CASE_INSENSITIVE_ORDER);
			for (ISerializableRun run : ClustEvalClient.getRuns()) {
				if (!(runMap.containsKey(run.getName())))
					runMap.put(run.getName(), new TreeMap<>(new VersionStringComparator().reversed()));
				runMap.get(run.getName()).put(run.getVersion(), run);
			}
			// for (String runName : runMap.keySet()) {
			// Run run = new Run(runName);
			//
			// String displayName = runName;

			// TODO: do we need this? is this actually ever happening?
			// Remove appended date if it is there
			// int index = runName.lastIndexOf('_');
			// if (index > -1) {
			// SimpleDateFormat dateFormat = new
			// SimpleDateFormat("yyyy-MM-dd-kk-mm-ss");
			// try {
			// if (dateFormat.parse(runName, new ParsePosition(index + 1)) !=
			// null) {
			// Date date = dateFormat.parse(runName, new ParsePosition(index +
			// 1));
			// SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy
			// kk:mm:ss");
			// displayName = runName.substring(0, index);
			//
			// run.setDate(formatter.format(date));
			// }
			// } catch (Exception e) {
			// }
			// }

			// run.setDisplayName(displayName);

			// runs.add(run);
			// }

			// TODO: we should keep this edit of info in the sorting
			// Collections.sort(runs, new Comparator<Run>() {
			// public int compare(Run left, Run right) {
			// // Use the 'edit of' line to determine the proper order of
			// // runs
			// String leftName = left.getName();
			// String rightName = right.getName();
			//
			// try {
			// BufferedReader leftReader = new BufferedReader(
			// new FileReader(getPath() + "/runs/" + left.getName() + ".run"));
			// BufferedReader rightReader = new BufferedReader(
			// new FileReader(getPath() + "/runs/" + right.getName() + ".run"));
			// String currentLine;
			//
			// while ((currentLine = leftReader.readLine()) != null) {
			// if (currentLine.startsWith("#editOf")) {
			// leftName = currentLine.split("=")[1].trim();
			//
			// if (runNames.contains(leftName)) {
			// left.setEdited(true);
			// }
			// break;
			// }
			// }
			//
			// while ((currentLine = rightReader.readLine()) != null) {
			// if (currentLine.startsWith("#editOf")) {
			// rightName = currentLine.split("=")[1].trim();
			//
			// if (runNames.contains(rightName)) {
			// right.setEdited(true);
			// }
			// break;
			// }
			// }
			// } catch (Exception e) {
			// }
			//
			// if (leftName.equals(rightName) && !left.getEdited()) {
			// return -1;
			// } else if (leftName.equals(rightName) && !right.getEdited()) {
			// return 1;
			// }
			//
			// return leftName.compareToIgnoreCase(rightName);
			// }
			// });
			model.addAttribute("connected", true);

			// model.addAttribute("runs", runs);
			model.addAttribute("runs", runMap);

			runResumes = new ArrayList<String>(ClustEvalClient.getRunResumes());
			model.addAttribute("resumes", runResumes);

			runningRuns = new ArrayList<String>(ClustEvalClient.getRunningRuns());
			model.addAttribute("runningRuns", runningRuns);

			finishedRuns = new ArrayList<String>(ClustEvalClient.getFinishedRuns());
			model.addAttribute("finishedRuns", finishedRuns);

			terminatedRuns = new ArrayList<String>(ClustEvalClient.getTerminatedRuns());
			model.addAttribute("terminatedRuns", terminatedRuns);

			List<String> toRemove = new ArrayList<>();

			for (String finishedRun : finishedRuns) {
				Collection<String> err = ClustEvalClient.getRunExceptions(finishedRun, false);
				if (!err.isEmpty()) {
					runErrors.put(finishedRun, err);
					toRemove.add(finishedRun);
				}
			}
			finishedRuns.removeAll(toRemove);

			// warnings of run execution
			for (String finishedRun : finishedRuns) {
				Collection<String> warning = ClustEvalClient.getRunExceptions(finishedRun, true);
				if (!warning.isEmpty()) {
					runWarnings.put(finishedRun, warning);
				}
			}

			parsingErrors.putAll(ClustEvalClient.getParsingExceptionMessages(IClusteringRun.class,
					IParameterOptimizationRun.class, IInternalParameterOptimizationRun.class, IDataAnalysisRun.class,
					IRunAnalysisRun.class, IRunDataAnalysisRun.class, IRobustnessAnalysisRun.class, IRun.class));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("connected", false);
			return "redirect:/";
		} finally {
			model.addAttribute("parsingErrors", parsingErrors);
			model.addAttribute("runErrors", runErrors);
			model.addAttribute("runWarnings", runWarnings);
		}

		return "runs/index";
	}

	@RequestMapping(value = "/runs", method = RequestMethod.POST)
	public String performRun(@ModelAttribute Run run, Model model, RedirectAttributes redirectAttributes) {
		try {
			ClustEvalClient ClustEvalClient = clientService.getClustEvalClient();

			ClustEvalClient.performRun(run.getName());

			logger.info("An instance of the run '" + run.getName() + "' was started.");
			redirectAttributes.addFlashAttribute("success",
					Arrays.asList("The run '" + run.getName() + "' was successfully started."));
		} catch (ConnectException e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Could not connect to the Clusteval server. Is it running?"));
		} catch (Exception e) {
			logger.error("An unknown error occurred when attempting to start an instance of the run '" + run.getName()
					+ "'. Exception: " + e);
			redirectAttributes.addFlashAttribute("failure", Arrays.asList("An unknown error occurred."));
		}

		redirectAttributes.addFlashAttribute("redirectUrl", "/runs");

		return "redirect:/redirect";
	}

	@RequestMapping(value = "/resume-run", method = RequestMethod.POST)
	public String resumeRun(@ModelAttribute Run run, Model model, RedirectAttributes redirectAttributes) {
		try {
			ClustEvalClient ClustEvalClient = clientService.getClustEvalClient();

			ClustEvalClient.resumeRun(run.getName());

			redirectAttributes.addFlashAttribute("success",
					Arrays.asList("The run '" + run.getName() + "' was successfully resumed."));
		} catch (ConnectException e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Could not connect to the Clusteval server. Is it running?"));
		} catch (Exception e) {
			logger.error("An unknown error occurred when attempting to resume the run '" + run.getName()
					+ "'. Exception: " + e);
			redirectAttributes.addFlashAttribute("failure", Arrays.asList("An unknown error occurred."));
		}

		redirectAttributes.addFlashAttribute("redirectUrl", "/runs");

		return "redirect:/redirect";
	}

	@RequestMapping(value = "/terminate-run", method = RequestMethod.POST)
	public String terminateRun(@ModelAttribute Run run, Model model, RedirectAttributes redirectAttributes) {
		try {
			ClustEvalClient ClustEvalClient = clientService.getClustEvalClient();

			ClustEvalClient.terminateRun(run.getName());

			redirectAttributes.addFlashAttribute("success",
					Arrays.asList("The run '" + run.getName() + "' was successfully paused."));
		} catch (ConnectException e) {
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("Could not connect to the Clusteval server. Is it running?"));
		} catch (Exception e) {
			logger.error("An unknown error occurred when attempting to pause the run '" + run.getName()
					+ "'. Exception: " + e);
			redirectAttributes.addFlashAttribute("failure", Arrays.asList("An unknown error occurred."));
		}

		redirectAttributes.addFlashAttribute("redirectUrl", "/runs");

		return "redirect:/redirect";
	}

	@RequestMapping(value = "/delete-run")
	public String deleteRunResult(@RequestParam(value = "name", required = true) String runName, Model model,
			RedirectAttributes redirectAttributes) {

		try {
			clientService.getClustEvalClient().deleteRunResult(runName);
			redirectAttributes.addFlashAttribute("success",
					Arrays.asList("The run '" + runName + "' has been succcesfully deleted."));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(
					"An unknown error occurred when attempting to delete the run '" + runName + "'. Exception: " + e);
			redirectAttributes.addFlashAttribute("failure", Arrays.asList("There was an error when deleting the run."));
		}

		redirectAttributes.addFlashAttribute("redirectUrl", "/runs");

		return "redirect:/redirect";
	}

	@RequestMapping(value = "/getRunStatus", method = RequestMethod.GET)
	public @ResponseBody Map<String, Pair<RUN_STATUS, Float>> getRunStatus() {
		Map<String, Pair<RUN_STATUS, Float>> status = null;
		try {
			ClustEvalClient ClustEvalClient = clientService.getClustEvalClient();

			status = ClustEvalClient.getMyRunStatus();
		} catch (ConnectException e) {
		} catch (Exception e) {
		}

		return status;
	}

	@RequestMapping(value = "/hideExecutionStatus", method = RequestMethod.GET)
	public String hideExecutionStatus(@RequestParam(value = "name", required = true) String name,
			RedirectAttributes redirectAttributes) {
		try {
			clientService.getClustEvalClient().clearRunExceptions(name);
			clientService.getClustEvalClient().forgetRunStatus(name);
			redirectAttributes.addFlashAttribute("success",
					Arrays.asList("The execution '" + name + "' will now be hidden."));
		} catch (Exception e) {
		}

		redirectAttributes.addFlashAttribute("redirectUrl", "/runs");

		return "redirect:/redirect";
	}

	// @RequestMapping(value = "/getRunErrors", method = RequestMethod.GET)
	// public @ResponseBody Collection<String> getRunErrors(
	// @RequestParam(value = "uniqueRunId", required = true) String uniqueRunId)
	// {
	// Collection<String> exceptions = null;
	// try {
	// ClustEvalClient ClustEvalClient = clientService.getClustEvalClient();
	//
	// exceptions = ClustEvalClient.getRunExceptions(uniqueRunId);
	// } catch (ConnectException e) {
	// } catch (Exception e) {
	// }
	//
	// return exceptions;
	// }

	private void populateModel(Model model) throws ConnectException {
		try {
			ClustEvalClient ClustEvalClient = clientService.getClustEvalClient();

			List<ISerializableDataConfig> dataSets = new ArrayList<ISerializableDataConfig>();
			for (ISerializableDataConfig obj : ClustEvalClient.getDataConfigurations()) {
				dataSets.add(obj);
			}
			List<ISerializableProgramConfig<? extends IProgramConfig>> programs = new ArrayList<ISerializableProgramConfig<? extends IProgramConfig>>();
			for (ISerializableProgramConfig<? extends IProgramConfig> obj : ClustEvalClient
					.getProgramConfigurations()) {
				programs.add(obj);
			}
			List<ISerializableWrapperDynamicComponent<IClusteringQualityMeasure>> qualityMeasures = new ArrayList<ISerializableWrapperDynamicComponent<IClusteringQualityMeasure>>(
					ClustEvalClient.getClusteringQualityMeasures());
			List<ISerializableWrapperDynamicComponent<IParameterOptimizationMethod>> optimizationMethods = new ArrayList<ISerializableWrapperDynamicComponent<IParameterOptimizationMethod>>(
					ClustEvalClient.getParameterOptimizationMethods());
			List<ISerializableWrapperDynamicComponent<IDataStatistic>> dataStatistics = new ArrayList<ISerializableWrapperDynamicComponent<IDataStatistic>>(
					ClustEvalClient.getDataStatistics());
			List<ISerializableWrapperDynamicComponent<IRunStatistic>> runStatistics = new ArrayList<ISerializableWrapperDynamicComponent<IRunStatistic>>(
					ClustEvalClient.getRunStatistics());
			List<ISerializableWrapperDynamicComponent<IRunDataStatistic>> runDataStatistics = new ArrayList<ISerializableWrapperDynamicComponent<IRunDataStatistic>>(
					ClustEvalClient.getRunDataStatistics());
			List<String> uniqueRunIdentifiers = new ArrayList<String>(
					ClustEvalClient.getClusteringRunResultIdentifiers());
			uniqueRunIdentifiers.addAll(ClustEvalClient.getParameterOptimizationRunResultIdentifiers());
			List<String> uniqueDataIdentifiers = new ArrayList<String>(
					ClustEvalClient.getDataAnalysisRunResultIdentifiers());
			List<ISerializableWrapperDynamicComponent<IDataRandomizer>> randomizers = new ArrayList<ISerializableWrapperDynamicComponent<IDataRandomizer>>(
					ClustEvalClient.getDataRandomizers());

			Collections.sort(programs, new Comparator<ISerializableProgramConfig<? extends IProgramConfig>>() {
				@Override
				public int compare(ISerializableProgramConfig<? extends IProgramConfig> o1,
						ISerializableProgramConfig<? extends IProgramConfig> o2) {
					String s1 = o1.getProgram().hasAlias() ? o1.getProgram().getAlias().toLowerCase()
							: o1.toString().toLowerCase();
					String s2 = o2.getProgram().hasAlias() ? o2.getProgram().getAlias().toLowerCase()
							: o2.toString().toLowerCase();
					int res = s1.compareTo(s2);
					if (res == 0)
						// newer versions first
						return o2.getVersion().compareTo(o1.getVersion());
					return res;
				}
			});
			Collections.sort(dataSets, new Comparator<ISerializableDataConfig>() {
				@Override
				public int compare(ISerializableDataConfig o1, ISerializableDataConfig o2) {
					int res = o1.getDatasetConfig().getDataSet().getAlias().toLowerCase()
							.compareTo(o2.getDatasetConfig().getDataSet().getAlias().toLowerCase());
					if (res == 0)
						// newer versions first
						return o2.getVersion().compareTo(o1.getVersion());
					return res;
				}
			});

			Comparator<ISerializableWrapperDynamicComponent<?>> compDynamicComponent = new Comparator<ISerializableWrapperDynamicComponent<?>>() {
				@Override
				public int compare(ISerializableWrapperDynamicComponent<?> o1,
						ISerializableWrapperDynamicComponent<?> o2) {
					String s1 = o1.hasAlias() ? o1.getAlias().toLowerCase() : o1.toString().toLowerCase();
					String s2 = o2.hasAlias() ? o2.getAlias().toLowerCase() : o2.toString().toLowerCase();
					int res = s1.compareTo(s2);
					if (res == 0)
						// newer versions first
						return o2.getVersion().compareTo(o1.getVersion());
					return res;
				}
			};

			Collections.sort(qualityMeasures, compDynamicComponent);
			Collections.sort(optimizationMethods, compDynamicComponent);
			Collections.sort(dataStatistics, compDynamicComponent);
			Collections.sort(runStatistics, compDynamicComponent);
			Collections.sort(runDataStatistics, compDynamicComponent);
			Collections.sort(uniqueRunIdentifiers, String.CASE_INSENSITIVE_ORDER);
			Collections.sort(uniqueDataIdentifiers, String.CASE_INSENSITIVE_ORDER);
			Collections.sort(randomizers, compDynamicComponent);

			model.addAttribute("dataSets", dataSets);
			model.addAttribute("programs", programs);
			model.addAttribute("qualityMeasures", qualityMeasures);
			model.addAttribute("optimizationMethods", optimizationMethods);
			model.addAttribute("dataStatistics", dataStatistics);
			model.addAttribute("runStatistics", runStatistics);
			model.addAttribute("runDataStatistics", runDataStatistics);
			model.addAttribute("uniqueRunIdentifiers", uniqueRunIdentifiers);
			model.addAttribute("uniqueDataIdentifiers", uniqueDataIdentifiers);
			model.addAttribute("randomizers", randomizers);
		} catch (ConnectException e) {
			throw (e);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/runs/create")
	public String createRun(RunCreation runCreation, Model model) {
		try {
			populateModel(model);
		} catch (ConnectException e) {
			return "runs/notRunning";
		} catch (Exception e) {
		}

		return "runs/create";
	}

	@RequestMapping(value = "/runs/create", method = RequestMethod.POST)
	public String createRunPOST(@Valid RunCreation runCreation, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes)
			throws ObjectNotFoundException, ObjectVersionNotFoundException, Exception {
		/*
		 * Create a new array of program settings This fixes an error that occurs if a
		 * program was selected and then deselected in the form
		 */
		ArrayList<Program> programSettings = new ArrayList<Program>();
		for (Program program : runCreation.getProgramSettings()) {
			if (program != null && program.getParameters() != null && program.getParameters().size() > 0) {
				programSettings.add(program);
			}
		}
		runCreation.setProgramParameters(programSettings);

		// Return to form if there were validation errors
		if (bindingResult.hasErrors()) {
			try {
				populateModel(model);
			} catch (ConnectException e) {
				return "runs/notRunning";
			} catch (Exception e) {
			}
			return "runs/create";
		}

		try {
			ISerializableRun<? extends IRun> run = createRunFromRunCreation(runCreation, programSettings);

			if (run != null)
				clientService.getClustEvalClient().uploadRun(run);

			redirectAttributes.addFlashAttribute("success", Arrays.asList("The run has been succcesfully created."));
		} catch (Exception e) {
			logger.error("An unknown error occurred when attempting to create a new run '" + runCreation.getName()
					+ "'. Exception: " + e);
			redirectAttributes.addFlashAttribute("failure", Arrays.asList("An error occurred during file writing."));
		}

		redirectAttributes.addFlashAttribute("redirectUrl", "/runs");

		return "redirect:/redirect";
	}

	protected ISerializableRun<? extends IRun> createRunFromRunCreation(final RunCreation runCreation,
			final ArrayList<Program> programSettings) throws ObjectNotFoundException, ObjectVersionNotFoundException,
			RemoteException, IncompatibleClustEvalVersionException, ParseException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		ISerializableRun<? extends IRun> run = null;
		List<ISerializableDataConfig> dataConfigs = new ArrayList<>();
		if (runCreation.getDataSets() != null) {
			for (String name : runCreation.getDataSets()) {
				if (name.equals("DUMMY"))
					continue;
				String[] split = name.split(":");
				// TODO: we could probably create those ourselves and dont
				// need to fetch them from the server
				dataConfigs.add(clientService.getClustEvalClient().getDataConfiguration(split[0], split[1]));
			}
		}
		List<ISerializableProgramConfig<? extends IProgramConfig>> programConfigs = new ArrayList<>();
		if (runCreation.getPrograms() != null) {
			for (String name : runCreation.getPrograms()) {
				if (name.equals("DUMMY"))
					continue;
				String[] split = name.split(":");
				// we retrieve this one from the server instead of creating
				// it to have all parameter available
				programConfigs.add(clientService.getClustEvalClient().getProgramConfiguration(split[0], split[1]));
			}
		}
		// TODO: left out for now
		List<ISerializableRunResultPostprocessor> postProcessors = new ArrayList<>();

		List<ISerializableClusteringQualityMeasure> qualityMeasures = new ArrayList<>();
		if (runCreation.getQualityMeasures() != null) {
			for (String name : runCreation.getQualityMeasures()) {
				if (name.equals("DUMMY"))
					continue;
				String[] split = name.split(":");
				// TODO: alias
				qualityMeasures.add(new SerializableClusteringQualityMeasure(null, split[0], split[1], split[0]));
			}
		}
		List<ISerializableStatistic> statistics = new ArrayList<>();
		// data statistics
		if (runCreation.getDataStatistics() != null) {
			for (String name : runCreation.getDataStatistics()) {
				if (name.equals("DUMMY"))
					continue;
				try {
					String[] split = name.split(":");
					// TODO: alias
					statistics.add(new SerializableDataStatistic(null, split[0], split[1], split[0]));
				} catch (Exception e) {
				}
			}
		}
		// run statistics
		if (runCreation.getDataStatistics() != null) {
			for (String name : runCreation.getRunStatistics()) {
				if (name.equals("DUMMY"))
					continue;
				try {
					String[] split = name.split(":");
					// TODO: alias
					statistics.add(new SerializableRunStatistic(null, split[0], split[1], split[0]));
				} catch (Exception e) {
				}
			}
		}
		// run-data statistics
		if (runCreation.getRunDataStatistics() != null) {
			for (String name : runCreation.getRunDataStatistics()) {
				if (name.equals("DUMMY"))
					continue;
				try {
					String[] split = name.split(":");
					statistics.add(new SerializableRunDataStatistic(null, split[0], split[1], split[0]));
				} catch (Exception e) {
				}
			}
		}
		List<String> uniqueDataIdentifiers = runCreation.getUniqueDataIdentifiers(),
				uniqueRunIdentifiers = runCreation.getUniqueRunIdentifiers();

		// TODO: context version from server
		// TODO: alias
		ISerializableContext context = new SerializableContext(null, "ClusteringContext", "1", "Clustering Context");

		String nextVersion = clientService.getClustEvalClient().getNextVersionForRepositoryObject(IRun.class,
				runCreation.getName(), runCreation.getVersion());

		if (runCreation.getMode().equals(RUN_TYPE.CLUSTERING.toString())) {
			run = new SerializableClusteringRun<IClusteringRun<?>>(null, runCreation.getName(), nextVersion, context,
					dataConfigs, programConfigs, postProcessors, qualityMeasures,
					// TODO: do we need these?
					new HashMap<>(), new HashMap<>());
		} else if (runCreation.getMode().equals(RUN_TYPE.PARAMETER_OPTIMIZATION.toString())) {
			Map<String, ISerializableProgramConfig<? extends IProgramConfig>> nameToProgramConfig = new HashMap<>();
			for (ISerializableProgramConfig<? extends IProgramConfig> pc : programConfigs)
				nameToProgramConfig.put(pc.toString(), pc);

			Map<ISerializableProgramConfig<? extends IProgramConfig>, List<IProgramParameter<?>>> optimizationParameters = new HashMap<>();
			Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> parameterMinValueOverrides = new HashMap<>();
			Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String>> parameterMaxValueOverrides = new HashMap<>();
			Map<ISerializableProgramConfig<? extends IProgramConfig>, Map<IProgramParameter<?>, String[]>> parameterOptionsOverrides = new HashMap<>();

			for (Program program : programSettings) {
				if (!(nameToProgramConfig.containsKey(program.getName())))
					continue;
				ISerializableProgramConfig<? extends IProgramConfig> pc = nameToProgramConfig.get(program.getName());

				List<IProgramParameter<?>> optParams;
				Map<IProgramParameter<?>, String> minVals = new HashMap<>();
				Map<IProgramParameter<?>, String> maxVals = new HashMap<>();
				Map<IProgramParameter<?>, String[]> options = new HashMap<>();
				// do we have parameter settings for this program config?
				if (program.getParameters() != null) {
					optParams = new ArrayList<>();
					for (ProgramParameter parameter : program.getParameters()) {
						IProgramParameter<?> pp = pc.getParameterForName(parameter.getName());
						// has this parameter been selected for optimization
						if (parameter.getOptimize() && parameter.getOptimizable()) {
							optParams.add(pp);
						}
						// overwrite values per parameter
						if (parameter.getMinValue() != null)
							minVals.put(pp, parameter.getMinValue());
						if (parameter.getMaxValue() != null)
							maxVals.put(pp, parameter.getMaxValue());
						if (parameter.getOptions() != null)
							// TODO: WTF?!?
							options.put(pp, parameter.getOptions().split(","));

					}
				} else {
					// if nothing is specified, use all the optimizable
					// parameters of the program config
					optParams = pc.getOptimizableParameters();
				}

				optimizationParameters.put(pc, optParams);
			}
			for (ISerializableProgramConfig<? extends IProgramConfig> pc : programConfigs)
				if (!optimizationParameters.containsKey(pc))
					optimizationParameters.put(pc, new ArrayList<>());

			List<ISerializableParameterOptimizationMethod> optimizationMethods = new ArrayList<>();

			String[] split = runCreation.getOptimizationCriterion().split(":");
			// TODO: alias
			ISerializableClusteringQualityMeasure optimizationCriterion = new SerializableClusteringQualityMeasure(null,
					split[0], split[1], split[0]);

			run = new SerializableParameterOptimizationRun<IParameterOptimizationRun>(null, runCreation.getName(),
					nextVersion, context, dataConfigs, programConfigs, postProcessors, qualityMeasures,
					// TODO: do we need this?
					new HashMap<>(), optimizationParameters, runCreation.getOptimizationMethod(), optimizationMethods,
					optimizationCriterion, runCreation.getOptimizationIterations(), parameterMaxValueOverrides,
					parameterMinValueOverrides, parameterOptionsOverrides, new HashMap<>());

			split = runCreation.getOptimizationMethod().split(":");
			for (int i = 0; i < programConfigs.size(); i++) {
				for (int j = 0; j < dataConfigs.size(); j++) {
					// TODO: alias
					optimizationMethods.add(new SerializableParameterOptimizationMethod(null, split[0], split[1],
							split[0], programConfigs.get(i), dataConfigs.get(j),
							runCreation.getOptimizationIterations(), optimizationParameters.get(programConfigs.get(i)),
							(SerializableParameterOptimizationRun) run));
				}
			}
		} else if (runCreation.getMode().equals(RUN_TYPE.DATA_ANALYSIS.toString())) {
			run = new SerializableDataAnalysisRun<>(null, runCreation.getName(), nextVersion, context, statistics,
					dataConfigs);
		} else if (runCreation.getMode().equals(RUN_TYPE.RUN_ANALYSIS.toString())) {
			run = new SerializableRunAnalysisRun<>(null, runCreation.getName(), nextVersion, context, statistics,
					uniqueRunIdentifiers);
		} else if (runCreation.getMode().equals(RUN_TYPE.RUN_DATA_ANALYSIS.toString())) {
			run = new SerializableRunDataAnalysisRun<>(null, runCreation.getName(), nextVersion, context, statistics,
					uniqueRunIdentifiers, uniqueDataIdentifiers);
		} else if (runCreation.getMode().equals(RUN_TYPE.ROBUSTNESS_ANALYSIS.toString())) {
			ISerializableDataRandomizer randomizer = null;
			List<ParameterSet> randomizerParams = new ArrayList<>();

			String r = runCreation.getRandomizer();
			String split[] = r.split(":");
			randomizer = new SerializableDataRandomizer(null, split[0], split[1], split[0]);

			for (List<Parameter> params : runCreation.getRandomizerParameters()) {

				ParameterSet paramSet = new ParameterSet();
				for (Parameter parameter : params) {
					if (!parameter.getValue().isEmpty()) {
						paramSet.put(parameter.getName(), parameter.getValue());
					}
				}
				randomizerParams.add(paramSet);
			}

			run = new SerializableRobustnessAnalysisRun<>(null, runCreation.getName(), nextVersion, context,
					dataConfigs, programConfigs, postProcessors, qualityMeasures, uniqueRunIdentifiers,
					randomizerParams, runCreation.getNumberOfRandomizedDataSets(), randomizer, new HashMap<>());
		}
		return run;
	}

	@RequestMapping(value = "/runs/edit")
	public String editRunGET(@RequestParam(value = "name", required = true) String fileName,
			@RequestParam(value = "version", required = true) String version, RunCreation runCreation, Model model) {
		try {
			ISerializableRun run = clientService.getClustEvalClient().getRun(fileName, version);

			Map<String, Program> programs = convertRunPrograms(run);
			runCreation.setProgramParameters(new ArrayList<>(programs.values()));
			runCreation.parse(run, programs);

			// // Remove appended date if it is there
			// int index = runCreation.getName().lastIndexOf('_');
			// if (index > -1) {
			// SimpleDateFormat dateFormat = new
			// SimpleDateFormat("yyyy-MM-dd-kk-mm-ss");
			// try {
			// if (dateFormat.parse(runCreation.getName(), new
			// ParsePosition(index + 1)) != null) {
			// runCreation.setName(runCreation.getName().substring(0, index));
			// }
			// } catch (Exception e) {
			// }
			// }

			populateModel(model);
		} catch (ConnectException e) {
			return "runs/notRunning";
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "runs/edit";
	}

	private Map<String, Program> convertRunPrograms(ISerializableRun run)
			throws ObjectNotFoundException, ObjectVersionNotFoundException, ConnectException, NoSuchObjectException,
			RemoteException, IncompatibleClustEvalVersionException, ParseException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		Map<String, Program> programs = new HashMap<>();

		if (run instanceof SerializableExecutionRun) {
			for (ISerializableProgramConfig<?> pc : ((SerializableExecutionRun<?>) run).getProgramConfigs()) {
				programs.put(pc.toString(), programService.getConvertedProgramConfig(pc.toString()));
			}
		}
		return programs;
	}

	@RequestMapping(value = "/runs/edit", method = RequestMethod.POST)
	public String editRunPOST(@Valid RunCreation runCreation, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes)
			throws ObjectNotFoundException, ObjectVersionNotFoundException, Exception {
		/*
		 * Create a new array of program settings This fixes an error that occurs if a
		 * program was selected and then deselected in the form
		 */
		ArrayList<Program> programSettings = new ArrayList<Program>();
		for (Program program : runCreation.getProgramSettings()) {
			if (program != null && program.getParameters() != null && program.getParameters().size() > 0) {
				programSettings.add(program);
			}
		}
		runCreation.setProgramParameters(programSettings);

		// Return to form if there were validation errors
		if (bindingResult.hasErrors()) {
			try {
				populateModel(model);
			} catch (ConnectException e) {
				return "runs/notRunning";
			} catch (Exception e) {
			}
			return "runs/edit";
		}
		try {
			ISerializableRun<? extends IRun> run = this.createRunFromRunCreation(runCreation, programSettings);
			//
			// String originalName = runCreation.getOriginalName();
			//
			// // Append date to run file to show when it was edited
			// // String dateAppend = new
			// // SimpleDateFormat("_yyyy-MM-dd-kk-mm-ss").format(new Date());
			// // runCreation.setName(runCreation.getName() + dateAppend);
			//
			if (run != null)
				clientService.getClustEvalClient().uploadRun(run);

			redirectAttributes.addFlashAttribute("success",
					Arrays.asList(
							String.format("You succcesfully edited version '%s' of run '%s'.", runCreation.getVersion(),
									runCreation.getName()),
							String.format("Your edited run was saved as version '%s' of run '%s'.", run.getVersion(),
									run.getName())));
		} catch (IOException e) {
			logger.error("An unknown error occurred when attempting to edit the run '" + runCreation.getName()
					+ "'. Exception: " + e);
			redirectAttributes.addFlashAttribute("failure", Arrays.asList("An error occurred during file writing."));
		}

		redirectAttributes.addFlashAttribute("redirectUrl", "/runs");

		return "redirect:/redirect";
	}

	@RequestMapping(value = "/runs/delete")
	public String deleteRun(@RequestParam(value = "name", required = true) String fileName,
			@RequestParam(value = "version", required = true) String version, Model model,
			RedirectAttributes redirectAttributes) {

		try {
			clientService.getClustEvalClient().deleteRun(clientService.getClustEvalClient().getRun(fileName, version));
		} catch (ConnectException e) {
			return "runs/notRunning";
		} catch (Exception e) {
			e.printStackTrace();
			redirectAttributes.addFlashAttribute("failure",
					Arrays.asList("The program configuration '" + fileName + "' could not be deleted."));
			redirectAttributes.addFlashAttribute("redirectUrl", "/runs");
			return "redirect:/redirect";
		}

		redirectAttributes.addFlashAttribute("success",
				Arrays.asList("The run '" + fileName + "' was successfully scheduled for deletion."));

		redirectAttributes.addFlashAttribute("redirectUrl", "/runs");

		return "redirect:/redirect";
	}

	@RequestMapping(value = "/getRun", method = RequestMethod.GET)
	public @ResponseBody RunCreation getRunCreationFromFileName(
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "version", required = true) String version) {
		RunCreation runCreation = new RunCreation();
		try {
			ISerializableRun run = clientService.getClustEvalClient().getRun(name, version);

			Map<String, Program> programs = convertRunPrograms(run);
			runCreation.parse(run, programs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return runCreation;
	}
}

package clusteval.controllers;

import java.rmi.ConnectException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import clusteval.services.ClustEvalClientService;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.ISerializableWrapper;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.serverclient.ClustEvalClient;

@Controller
public class LogsController {
	@Autowired
	ClustEvalClientService clientService;

	@RequestMapping(value = "/logs")
	public String showPrograms(Model model) {
		try {
			ClustEvalClient ClustEvalClient = clientService.getClustEvalClient();
			Map<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>, List<String>> parsingExceptionMessages = ClustEvalClient
					.getParsingExceptionMessages();
			// Collections.sort(parsingExceptionMessages.keySet(),
			// String.CASE_INSENSITIVE_ORDER);

			System.out.println(parsingExceptionMessages);
			model.addAttribute("measures", parsingExceptionMessages);
		} catch (ConnectException e) {
			return "runs/notRunning";
		} catch (Exception e) {

		}

		return "logs/index";
	}
}

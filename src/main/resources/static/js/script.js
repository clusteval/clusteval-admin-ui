// formatting of program select box options
function formatAliasAndNameWithVersion(alias, nameWithVersion) {
	var split = nameWithVersion.split(":");
	return '<span>' + alias + ' <small style="color: #777;">' + split[0] + ' <div class="label label-primary">v' + split[1] + '</div></small></span>';
}
function formatNameWithVersion(nameWithVersion) {
	var split = nameWithVersion.split(":");
	return '<span>' + split[0] + ' <small style="color: #777;"><div class="label label-primary">v' + split[1] + '</div></small></span>';
}


$(document).ready(function() {
    /************************
    *** Run creation form ***
    ************************/

    //Add select2 to multiple select boxes
    $('.select2-multiple').select2({
        closeOnSelect: true,
        width: '100%',
        dropdownCssClass: 'dropdown-option',
        containerCssClass: 'dropdown-container'
    });
    
	function getFormattedAliasNameVersionForSelect (data) {
		  if (!data.id) { 
			  return data.text; 
		  }
		  var $data = $(formatAliasAndNameWithVersion(data.text, data.id));
		  return $data;
	};
	// without name
	function getFormattedAliasVersionForSelect (data) {
		  if (!data.id) { 
			  return data.text;
		  }
		  var split = data.text.split(":");

		  var $data = $(formatNameWithVersion(data.text));
		  return $data;
	};
	
    $('.select2-multiple.programs').select2({
        closeOnSelect: true,
        width: '100%',
        dropdownCssClass: 'dropdown-option',
        containerCssClass: 'dropdown-container',
    	templateResult: getFormattedAliasNameVersionForSelect,
    	templateSelection: getFormattedAliasNameVersionForSelect
    });

    $('.select2-multiple.datasets').select2({
        closeOnSelect: true,
        width: '100%',
        dropdownCssClass: 'dropdown-option',
        containerCssClass: 'dropdown-container',
    	templateResult: getFormattedAliasNameVersionForSelect,
    	templateSelection: getFormattedAliasNameVersionForSelect
    });

    $('.select2-multiple.quality-measures').select2({
        closeOnSelect: true,
        width: '100%',
        dropdownCssClass: 'dropdown-option',
        containerCssClass: 'dropdown-container',
    	templateResult: getFormattedAliasVersionForSelect,
    	templateSelection: getFormattedAliasVersionForSelect
    });
    $('.select2-multiple.optimization-method').select2({
        closeOnSelect: true,
        width: '100%',
        dropdownCssClass: 'dropdown-option',
        containerCssClass: 'dropdown-container',
    	templateResult: getFormattedAliasVersionForSelect,
    	templateSelection: getFormattedAliasVersionForSelect
    });
    $('.select2-multiple.data-statistics').select2({
        closeOnSelect: true,
        width: '100%',
        dropdownCssClass: 'dropdown-option',
        containerCssClass: 'dropdown-container',
    	templateResult: getFormattedAliasVersionForSelect,
    	templateSelection: getFormattedAliasVersionForSelect
    });
    $('.select2-multiple.run-statistics').select2({
        closeOnSelect: true,
        width: '100%',
        dropdownCssClass: 'dropdown-option',
        containerCssClass: 'dropdown-container',
    	templateResult: getFormattedAliasVersionForSelect,
    	templateSelection: getFormattedAliasVersionForSelect
    });
    $('.select2-multiple.run-data-statistics').select2({
        closeOnSelect: true,
        width: '100%',
        dropdownCssClass: 'dropdown-option',
        containerCssClass: 'dropdown-container',
    	templateResult: getFormattedAliasVersionForSelect,
    	templateSelection: getFormattedAliasVersionForSelect
    });
    $('.select2-multiple.data-randomizer').select2({
        closeOnSelect: true,
        width: '100%',
        dropdownCssClass: 'dropdown-option',
        containerCssClass: 'dropdown-container',
    	templateResult: getFormattedAliasVersionForSelect,
    	templateSelection: getFormattedAliasVersionForSelect
    });
    $('.select2-multiple.dataset-types').select2({
        closeOnSelect: true,
        width: '100%',
        dropdownCssClass: 'dropdown-option',
        containerCssClass: 'dropdown-container',
    	templateResult: getFormattedAliasVersionForSelect,
    	templateSelection: getFormattedAliasVersionForSelect
    });
    $('.select2-multiple.dataset-formats').select2({
        closeOnSelect: true,
        width: '100%',
        dropdownCssClass: 'dropdown-option',
        containerCssClass: 'dropdown-container',
    	templateResult: getFormattedAliasVersionForSelect,
    	templateSelection: getFormattedAliasVersionForSelect
    });
    $('.select2-multiple.compatible-input-formats').select2({
        closeOnSelect: true,
        width: '100%',
        dropdownCssClass: 'dropdown-option',
        containerCssClass: 'dropdown-container',
    	templateResult: getFormattedAliasVersionForSelect,
    	templateSelection: getFormattedAliasVersionForSelect
    });
    $('.select2-multiple.output-format').select2({
        closeOnSelect: true,
        width: '100%',
        dropdownCssClass: 'dropdown-option',
        containerCssClass: 'dropdown-container',
    	templateResult: getFormattedAliasVersionForSelect,
    	templateSelection: getFormattedAliasVersionForSelect
    });

    //Make all mode-specific sections initially invisible
    $('form.run-creation [data-mode]').hide();
    $('form.run-creation [data-mode]').find('input,select').prop('disabled', true);
    $('form.run-creation [data-mode]').find('input[type=hidden].placeholder').prop('disabled', false);

    //If a mode is already selected on page load, show appropriate sections
    if ($('form.run-creation input[type=radio][name=mode]:checked').length > 0) {
        updateMode($('form.run-creation input[type=radio][name=mode]:checked'), $('form.run-creation'));
    }

    //Mode select/change
    $('form.run-creation input[type=radio][name=mode]').change(function() {
        updateMode($(this), $('form.run-creation'));
        updateAccordion();
    });

    function updateMode(radio, parent) {
        var mode = $(radio).val();

        //Only show mode-specific sections for the selected mode
        var elementsToHide = parent.find('.filter').filter(function(){
            return $.inArray(mode.toString(), $(this).data('mode')) < 0
        });

        var elementsToShow = parent.find('.filter').filter(function(){
            return $.inArray(mode.toString(), $(this).data('mode')) >= 0
        });

        elementsToShow.show();
        elementsToShow.find('input,select').prop('disabled', false);
        elementsToShow.find('input[type=hidden].placeholder').prop('disabled', true);

        elementsToHide.hide();
        elementsToHide.find('input,select').prop('disabled', true);
        elementsToHide.find('input[type=hidden].placeholder').prop('disabled', false);

        if (mode == "parameter_optimization") {
            $('.optimize-parameter').find('input').prop('disabled', false);
            $('.optimize-parameter.hidden').find('input').prop('disabled', true);
        }
    }
    

    //If program type is r-program on page load, show appropriate sections
    if ($('form.program-creation input[type=checkbox][name=isRProgram]:checked').length > 0) {
        updateProgramType($('form.program-creation input[type=checkbox][name=isRProgram]:checked').length > 0, $('form.program-creation'));
    }

    //program type select/change
    $('form.program-creation input[type=checkbox][name=isRProgram]').change(function() {
    	updateProgramType($('form.program-creation input[type=checkbox][name=isRProgram]:checked').length > 0, $('form.program-creation'));
    });

    function updateProgramType(isChecked, parent) {
        var mode = isChecked ? 'r' : 'standalone';

        //Only show mode-specific sections for the selected mode
        var elementsToHide = parent.find('.filter').filter(function(){
            return $.inArray(mode.toString(), $(this).data('programtype')) < 0
        });

        var elementsToShow = parent.find('.filter').filter(function(){
            return $.inArray(mode.toString(), $(this).data('programtype')) >= 0
        });

        elementsToShow.show();
        elementsToShow.find('input,select').prop('disabled', false);
        elementsToShow.find('input[type=hidden].placeholder').prop('disabled', true);

        elementsToHide.hide();
        elementsToHide.find('input,select').prop('disabled', true);
        elementsToHide.find('input[type=hidden].placeholder').prop('disabled', false);

        if (mode == "r")
        	$('.add-program-parameter').hide();
        else
        	$('.add-program-parameter').show();
    }

    //When selecting a quality measure, enable the corresponding optimization criterion
    $('form.run-creation select[name=qualityMeasures]').change(function(){
        updateOptimizationCriterion($(this));
    });
    function updateOptimizationCriterion(element) {
        $('form.run-creation select[name=optimizationCriterion] option').attr('disabled', 'disabled');
        $(element).find('option:selected').each(function(){
            var value = $(this).val();
            $('form.run-creation select[name=optimizationCriterion]').find('option[value="' + value + '"]').removeAttr('disabled');
        });

        // unselect if the selected option is now disabled
    	if ($("form.run-creation select[name=optimizationCriterion] option:selected").filter(':disabled').length > 0) {
    		$('form.run-creation select[name=optimizationCriterion]').val('').change();
    	}

        //Reinitialize select2 to reflect changes
        $('form.run-creation select[name=optimizationCriterion]').select2({ 
        	closeOnSelect: true, 
        	width: '100%',
        	templateResult: getFormattedAliasVersionForSelect,
        	templateSelection: getFormattedAliasVersionForSelect });
    }

    //Add randomizer
    $('form.run-creation .add-randomizer').click(function() {
        var index = $('form.run-creation .randomizers .randomizer').length;

        var randomizer = $('#select-randomizer').val();
        
        //var randomizer = $(this).data('randomizer');

        $.ajax({
            url: "/getRandomizer?name=" + randomizer,
            type: 'get',
            dataType: 'html',
            success: function(data) {
                result = $.parseJSON(data);

                var returnValue = '<div class="randomizer"><div class="form-group">';
                var paramIndex = 0;
                $.each(result.availableParameters, function(key,value) {
                    returnValue += '<div class="col-sm-3"><label>' + value.name + ' - ' + value.description + '</label></div>';
                    returnValue += '<div class="col-sm-9"><input class="form-control" type="text" id="randomizerParameters' + index + '.' + paramIndex + '.value" name="randomizerParameters[' + index + '][' + paramIndex + '].value" />'
                    returnValue += '<input type="hidden" id="randomizerParameters' + index + '.' + paramIndex + '.description" name="randomizerParameters[' + index + '][' + paramIndex + '].description" value="' + value.description + '" />'
                    returnValue += '<input type="hidden" id="randomizerParameters' + index + '.' + paramIndex + '.name" name="randomizerParameters[' + index + '][' + paramIndex + '].name" value="' + value.name + '" /></div>'
                    paramIndex++;
                });
                returnValue += '</div></div>';
                $(".randomizers").append(returnValue);
            }
        });
    });

    //Add program
    var val = $('form.run-creation select.programs').val();
    if (val == null) {
        val = [];
    }
    $('form.run-creation').on('change', 'select.programs', function() {
        var newVal = $(this).val();
        var selectedProgram;
        var index = parseInt($('form.run-creation select.programs').data('index'));

        if (newVal == null) {
            //All have been removed - reset everything
            newVal = [];
        }
        //Check if element was selected
        for (var i = 0; i < newVal.length; i++) {
            if ($.inArray(newVal[i], val) == -1) {
                selectedProgram = $(this).find('option[value="' + newVal[i] + '"]').val();
                addProgramParameters(selectedProgram, index);
                $('form.run-creation select.programs').data('index', index + 1);
            }
        }
        
        updateProgramExpectedRuntime();
        console.log(val);

        //Check if element was unselected
        for (var i = 0; i < val.length; i++) {
            if ($.inArray(val[i], newVal) == -1) {
            	console.log("removed");
                selectedProgram = $(this).find('option[value="' + val[i] + '"]').val();
                $('form.run-creation #program-parameters .program[data-name="' + selectedProgram + '"]').remove();
            }
        }

        val = newVal;
    });
    
    $("#optimizationIterations").on('change', function(x) {
    	console.log("change");
    	updateProgramExpectedRuntime();
    });
    
    $(".datasets").on('change', function(x) {
    	console.log("change");
    	updateProgramExpectedRuntime();
    });

    $('form.run-creation select.programs').data('index', $('form.run-creation #program-parameters .program').length);

    function updateProgramExpectedRuntime() {
        $('form.run-creation #estimatedRuntimesList li').remove();
        
        var programConfigs = $('form.run-creation select.programs').val();
        for (var idx in programConfigs) {
        	var program = programConfigs[idx];
	    	var split = program.split(":");
	    	var iterations = $("#optimizationIterations").val();
	    	var dataConfigs = $(".select2-multiple.datasets").val();
	        $.ajax({
	            url: "/program-config-runtime?name=" + split[0] + "&version=" + split[1] + "&dataConfigs=" + dataConfigs + "&iterations=" + iterations + "&simplify=true",
	            type: 'get',
	            dataType: 'html',
	            success: function(data) {
	                result = $.parseJSON(data);
	                $("#estimatedRuntimesList").append("<li data-name=\"" + result.program + "\">" + formatNameWithVersion(result.program) + ": " + result.formattedTime + "</li>");
	            }});
        }
        
        console.log(programConfigs.length);
        $('form.run-creation #estimatedRuntimesDiv').css('display', programConfigs.length > 0 ? 'inline' : 'none');
    }

    function addProgramExpectedRuntime(program, index) {
    	var split = program.split(":");
    	var iterations = $("#optimizationIterations").val();
    	var dataConfigs = $(".select2-multiple.datasets").val();
        $.ajax({
            url: "/program-config-runtime?name=" + split[0] + "&version=" + split[1] + "&dataConfigs=" + dataConfigs + "&iterations=" + iterations + "&simplify=true",
            type: 'get',
            dataType: 'html',
            success: function(data) {
                result = $.parseJSON(data);
                console.log(result);
                $("#estimatedRuntimesList").append("<li data-name=\"" + program + "\">" + program + ": " + result + "</li>");
            }});
    }

    function addProgramParameters(program, index) {
        $.ajax({
            url: "/getProgram?name=" + program,
            type: 'get',
            dataType: 'html',
            success: function(data) {
                result = $.parseJSON(data);

                var returnValue = '<div class="program" data-name="' + result.name + '">';
                    returnValue += '<span class="name"><strong>Program: </strong>' + formatAliasAndNameWithVersion(result.alias, result.name) + '</span>';
                    returnValue += '<input type="hidden" id="programSettings' + index + '.name"' + 'name="programSettings[' + index + '].name" value="' + result.name + '" />';
                    returnValue += '<span class="toggle"><i class="fa fa-expand"></i></span>'
                    returnValue += '<div class="program-content">';
                        var paramIndex = 0;
                        $.each(result.parameters, function(key,value) {
                            var optionIndex = 0;
                            var defaultValue = '';
                            var minValue = '';
                            var maxValue = '';
                            var options = '';

                            returnValue += '<div class="program-content-parameter">';
                                returnValue += '<hr><strong>Parameter:</strong> ' + value.name + '<br>';
                                returnValue += '<input type="hidden" id="programSettings' + index + '.parameters' + paramIndex + '.name"' + 'name="programSettings[' + index + '].parameters[' + paramIndex + '].name" value="' + value.name + '" />';
                                returnValue += '<strong>Defaults:</strong><br>';
                                $.each(value.defaultOptions, function(key,value){
                                    if (value.name == "defaultValue") {
                                        defaultValue = value.value;
                                    }
                                    if (value.name == "minValue") {
                                        returnValue += 'Minimum value: ' + value.value + '<br>';
                                        minValue = value.value;
                                    }
                                    if (value.name == "maxValue") {
                                        returnValue += 'Maximum value: ' + value.value + '<br>';
                                        maxValue = value.value;
                                    }
                                    if (value.name == "options") {
                                        options = value.value;
                                    }
                                    returnValue += '<input type="hidden" id="programSettings' + index + '.parameters' + paramIndex + '.defaultOptions' + optionIndex + '.name"' + 'name="programSettings[' + index + '].parameters[' + paramIndex + '].defaultOptions[' + optionIndex + '].name" value="' + value.name + '" />';
                                    returnValue += '<input type="hidden" id="programSettings' + index + '.parameters' + paramIndex + '.defaultOptions' + optionIndex + '.value"' + 'name="programSettings[' + index + '].parameters[' + paramIndex + '].defaultOptions[' + optionIndex + '].value" value="' + value.value + '" />';
                                    optionIndex++;
                                });

                                returnValue += '<div class="filter" data-mode="[&quot;parameter_optimization&quot;]">';
                                    if (value.optimizable) {
                                        returnValue += '<div class="checkbox">';
                                            returnValue += '<label>';
                                                returnValue += '<input class="checkbox-optimize" type="checkbox" checked="checked" value="true" id="programSettings' + index + '.parameters' + paramIndex + '.optimize1" name="programSettings[' + index + '].parameters[' + paramIndex + '].optimize">Optimize';
                                            returnValue += '</label>';
                                            returnValue += '<input type="hidden" id="programSettings' + index + '.parameters' + paramIndex + '.optimizable" name="programSettings[' + index + '].parameters[' + paramIndex + '].optimizable" value="true">';
                                            returnValue += '<input name="_programSettings[' + index + '].parameters[' + paramIndex + '].optimize" value="on" type="hidden">';
                                        returnValue += '</div>';
                                    }
                                    if (options == '') {
                                        if (value.optimizable) {
                                            returnValue += '<div class="optimize-parameter optimize">';
                                                returnValue += '<label>Overwrite minimum value';
                                                    returnValue += '<input placeholder="Minimum value" class="form-control" id="programSettings' + index + '.parameters' + paramIndex + '.minValue" name="programSettings[' + index + '].parameters[' + paramIndex + '].minValue" type="text" value="' + minValue + '" />';
                                                returnValue += '</label>';
                                                returnValue += '<label>Overwrite maximum value';
                                                    returnValue += '<input placeholder="Maximum value" class="form-control" id="programSettings' + index + '.parameters' + paramIndex + '.maxValue" name="programSettings[' + index + '].parameters[' + paramIndex + '].maxValue" type="text" value="' + maxValue + '" />';
                                                returnValue += '</label>';
                                            returnValue += '</div>';
                                            returnValue += '<div class="optimize-parameter no-optimize hidden" style="display: none;">';
                                                returnValue += '<label>Overwrite default value';
                                                    returnValue += '<input placeholder="Default value" class="form-control" id="programSettings' + index + '.parameters' + paramIndex + '.value" name="programSettings[' + index + '].parameters[' + paramIndex + '].value" type="text" value="' + defaultValue + '" disabled />';
                                                returnValue += '</label>';
                                            returnValue += '</div>';
                                        } else {
                                            returnValue += '<div>';
                                                returnValue += '<label>Overwrite default value';
                                                    returnValue += '<input placeholder="Default value" class="form-control" id="programSettings' + index + '.parameters' + paramIndex + '.value" name="programSettings[' + index + '].parameters[' + paramIndex + '].value" type="text" value="' + defaultValue + '" />';
                                                returnValue += '</label>';
                                            returnValue += '</div>';
                                        }
                                    } else {
                                        returnValue += '<label>Values';
                                            returnValue += '<input placeholder="Options" class="form-control" id="programSettings' + index + '.parameters' + paramIndex + '.options" name="programSettings[' + index + '].parameters[' + paramIndex + '].options" type="text" value="' + options + '" />';
                                        returnValue += '</label>';
                                    }
                                returnValue += '</div>';

                                returnValue += '<div class="filter" data-mode="[&quot;clustering&quot;,&quot;robustnessAnalysis&quot;]">';
                                    returnValue += '<label>Value';
                                        returnValue += '<input placeholder="Default value" class="form-control" id="programSettings' + index + '.parameters' + paramIndex + '.value" name="programSettings[' + index + '].parameters[' + paramIndex + '].value" type="text" value="' + defaultValue + '" />';
                                    returnValue += '</label>';
                                returnValue += '</div>';
                            returnValue += '</div>';

                            paramIndex++;
                        });
                    returnValue += '</div>';
                returnValue += '</div>';
                $("#program-parameters").append(returnValue);
                updateMode($('form.run-creation input[type=radio][name=mode]:checked'), $('form.run-creation #program-parameters'));
            }
        });
    }

    //Toogle program parameter collapse
    $('form.run-creation #program-parameters').on('click', '.toggle', function() {
        $(this).parent().find('.program-content').slideToggle();
        $(this).find('.fa').toggleClass('fa-expand');
        $(this).find('.fa').toggleClass('fa-compress');
    });

    //Toggle parameter optimization
    $('form.run-creation #program-parameters').on('change', 'input.checkbox-optimize', function() {
        var content = $(this).closest('.program-content-parameter');
        if ($(this).is(':checked')) {
            content.find('.optimize-parameter.optimize').show();
            content.find('.optimize-parameter.optimize').find('input').prop('disabled', false);
            content.find('.optimize-parameter.optimize').removeClass('hidden');

            content.find('.optimize-parameter.no-optimize').hide();
            content.find('.optimize-parameter.no-optimize').find('input').prop('disabled', true);
            content.find('.optimize-parameter.no-optimize').addClass('hidden');
        } else {
            content.find('.optimize-parameter.optimize').hide();
            content.find('.optimize-parameter.optimize').find('input').prop('disabled', true);
            content.find('.optimize-parameter.optimize').addClass('hidden');

            content.find('.optimize-parameter.no-optimize').show();
            content.find('.optimize-parameter.no-optimize').find('input').prop('disabled', false);
            content.find('.optimize-parameter.no-optimize').removeClass('hidden');
        }
    });

    //Trigger parameter optimize checkbox on page load
    $('form.run-creation #program-parameters input.checkbox-optimize').trigger('change');

    //Initially update criterion
    updateOptimizationCriterion('form.run-creation select[name=qualityMeasures]');

    /****************
    ****************/

    /****************
    *** Accordion ***
    ****************/
    function hideElementWithNoVisibleChildren(elements) {
        //Hide all empty slides
        $(elements).filter(function() {
            return $(this).find('.section-content').children('div:visible').length == 0;
        }).hide();
    }

    function updateAccordion() {
        $('.myAccordion > li').show();
        hideElementWithNoVisibleChildren('.myAccordion > li');

        $('.myAccordion > li').removeClass('animated');

        var subtract = ($('.myAccordion > li:visible').length - 1) * 75;
        $('.myAccordion > li:first-child').css('width', 'calc(100% - ' + subtract + 'px)');

        var width = $('.myAccordion > li:first-child').width() - 75;
        $('.myAccordion .section-content').css('min-width', width);

        $('.myAccordion > li').addClass('animated');
    }

    //Initially hide appropriate slides
    hideElementWithNoVisibleChildren('.myAccordion > li');

    //Add error class to any tabs that contains error
    $('.myAccordion > li').filter(function() {
        return $(this).find('span.error').length > 0;
    }).find('.section-header').addClass('error');

    var subtract = ($('.myAccordion > li:visible').length - 1) * 75;
    $('.myAccordion > li:first-child').css('width', 'calc(100% - ' + subtract + 'px)');

    var width = $('.myAccordion > li:first-child').width() - 75;
    $('.myAccordion .section-content').css('min-width', width);

    $('.myAccordion > li').addClass('animated');

    $('.myAccordion > li').click(function (){
            $('.myAccordion > li').css('width', '75px');

            var subtract = ($('.myAccordion > li:visible').length - 1) * 75;
            $(this).css('width', 'calc(100% - ' + subtract + 'px)');
            $(this).find('.section-content').css('width', 'calc(100% - ' + subtract + 'px)');
    });

    /****************
     * RUNS
    ****************/
    // run container toggle
    $('.run-container-outer .toggle').click(function() {
        var name = $(this).parent().find('.fetch-run').html();
        var container = $(this).parent().find('.run-information-outer');
        var subContainer = $(this).parent().find('.run-information').first();
        var subToggle = $(this).parent().find('.run-container .toggle').first();

        $(this).toggleClass('open');
        $(this).find('.fa').toggleClass('fa-expand');
        $(this).find('.fa').toggleClass('fa-compress');

        if (container.is(':visible')) {
            container.slideUp(100);
        } else {
            container.slideDown(0, function() {
                if (!subContainer.is(':visible'))
                	subToggle.trigger('click');
            });
        }
    });
    $('.run-container-outer .toggle.open').trigger('click');


    //Wrap edited runs in container
    var editedRuns = [];
    var editedRunsCount = 0;
    $('.run.edited').each(function() {
        var nextRunEdited = $(this).next().hasClass('edited');

        editedRuns.push($(this));
        editedRunsCount++;

        if (!nextRunEdited) {
            var container = $('<div class="edited-runs-container"></div>');
            container.insertBefore(editedRuns[0]);

            for (i = 0; i < editedRuns.length; i++) {
                editedRuns[i].appendTo(container);
            }
            if (editedRunsCount == 1) {
                //Append button to original run
                container.prev('.run').find('form').append('<a class="gray-button no-margin pull-right toggle-edits"><i class="fa fa-angle-down"></i> ' + editedRunsCount + ' additional version available</a>');
            } else {
                //Append button to original run
                container.prev('.run').find('form').append('<a class="gray-button no-margin pull-right toggle-edits"><i class="fa fa-angle-down"></i> ' + editedRunsCount + ' additional versions available</a>');
            }

            editedRuns = [];
            editedRunsCount = 0;
        }
    });

    //Toggle edited runs display
    $('.run-list .run').on('click', '.toggle-edits', function() {
        $(this).closest('.run').next('.edited-runs-container').slideToggle();

        $(this).find('.fa').toggleClass('fa-angle-down');
        $(this).find('.fa').toggleClass('fa-angle-up');
    });

    //Delete run button click
    $('.run-list .delete-run').click(function() {
        if (confirm("Delete this run?")) {
            $.get('/runs/delete?name=' + $(this).data('name') + '&version=' + $(this).data('version'));
            $(this).parent().parent().parent().fadeOut();
        }
    });

    //Function that updates the progress bars of runs that are in progress
    function updateRunProgress() {
        $.ajax({
            url: "/getRunStatus",
            type: 'get',
            dataType: 'html',
            success: function(data) {
                result = $.parseJSON(data);

                $.each(result, function (runName, status) {
                    var progressBar = $('.run-list .run[run-name="' + runName + '"] .progress-bar');
                    if (progressBar.is(':visible')) {
                    	if (status.second == 100)
                    		location.reload();
                    }
                    progressBar.attr('aria-valuenow', status.second);
                    progressBar.css('width', status.second + '%');
                    progressBar.find('.sr-only').text(status.second + '% Complete');
                });
            }
        });

        //Update every 5 seconds
        setTimeout(updateRunProgress, 5000);
    }
    updateRunProgress();

    //Fetch information about data config file on click
    $('.data-container-outer .toggle').click(function() {
        var name = $(this).parent().find('.fetch-data').html();
        var container = $(this).parent().find('.data-information-outer');
        var subContainer = $(this).parent().find('.data-information').first();
        var subToggle = $(this).parent().find('.data-container .toggle').first();

        $(this).toggleClass('open');
        $(this).find('.fa').toggleClass('fa-expand');
        $(this).find('.fa').toggleClass('fa-compress');

        if (container.is(':visible')) {
            container.slideUp(100);
        } else {
            container.slideDown(0, function() {
                if (!subContainer.is(':visible'))
                	subToggle.trigger('click');
            });
        }
    });
    $('.data-container-outer .toggle.open').trigger('click');

    //Fetch information about data config file on click
    $('.data-container .toggle').click(function() {
        var name = $(this).parent().parent().parent().find('.fetch-data').html();
        var version = $(this).parent().find('.fetch-data-version').html();
        var container = $(this).parent().find('.data-information');

        $(this).toggleClass('open');
        $(this).find('.fa').toggleClass('fa-expand');
        $(this).find('.fa').toggleClass('fa-compress');

        if (container.is(':visible')) {
            container.slideUp(100);
        } else {
            $.ajax({
                url: "/data/show?name=" + name + "&version=" + version,
                type: 'get',
                dataType: 'html',
                success: function(data) {
                    container.html(data);
                    container.slideDown(100);
                }
            });
        }
    });

    $('#data .expand-all').click(function() {
        $('.data-container .toggle:not(.open)').trigger('click');
        $('.data-container-outer .toggle:not(.open)').trigger('click');
    });

    $('#data .collapse-all').click(function() {
        $('.data-container .toggle.open').trigger('click');
        $('.data-container-outer .toggle.open').trigger('click');
    });

    //Delete data configuration/dataset
    $('#data .delete-data').click(function() {
        if (confirm("Delete this data?")) {
            $.get('/data-config/delete?name=' + $(this).data('name') + '&version=' + $(this).data('version'));
            $(this).parent().fadeOut();
        }
    });
    
    

    /********
     * Programs
     *******/
    $('.program-container-outer .toggle').click(function() {
        var name = $(this).parent().find('.fetch-program').html();
        var container = $(this).parent().find('.program-information-outer');
        var subContainer = $(this).parent().find('.data-information').first();
        var subToggle = $(this).parent().find('.data-container .toggle').first();

        $(this).toggleClass('open');
        $(this).find('.fa').toggleClass('fa-expand');
        $(this).find('.fa').toggleClass('fa-compress');

        if (container.is(':visible')) {
            container.slideUp(100);
        } else {
            container.slideDown(0, function() {
                if (!subContainer.is(':visible'))
                	subToggle.trigger('click');
            });
        }
    });

    $('.program-container .toggle').click(function() {
        var name = $(this).parent().find('.fetch-program').html();
        var version = $(this).parent().find('.fetch-program-version').html();
        var container = $(this).parent().find('.program-information');

        $(this).toggleClass('open');
        $(this).find('.fa').toggleClass('fa-expand');
        $(this).find('.fa').toggleClass('fa-compress');

        if (container.is(':visible')) {
            container.slideUp(100);
        } else {
            $.ajax({
                url: "/programs/show?name=" + name + "&version=" + version,
                type: 'get',
                dataType: 'html',
                success: function(data) {
                    container.html(data);
                    container.slideDown(100);
                }
            });
        }
    });

    $('#programs .expand-all').click(function() {
        $('.program-container .toggle:not(.open)').trigger('click');
    });

    $('#programs .collapse-all').click(function() {
        $('.program-container .toggle.open').trigger('click');
    });

    //Toggle result graphs
    $('.toggle-result-graphs').click(function() {
        $(this).parent().find('.result-graphs').slideToggle();
        $(this).find('.fa').toggleClass('fa-chevron-up');
        $(this).find('.fa').toggleClass('fa-chevron-down');
    });

    //Show value of range input
    $('.parameter-sliders, .cluster-parameter-sliders').on('input change', '.range-container input[type=range]', function() {
        if ($(this).hasClass('options')) {
            try {
                var options = $(this).data('options').split(',');
                $(this).next('.range-value').html(options[$(this).val()]);
            }
            catch (err) {
                //Fix in case there is only one option
                var options = $(this).data('options');
                $(this).next('.range-value').html(options);
            }
        } else {
            $(this).next('.range-value').html($(this).val());
        }
    });

    //Change active radio on parameter slider
    $('.parameter-sliders').on('change', '.parameter-graph-form input[type=radio]', function() {
        $('.parameter-graph-form').find('.icon .locked').show();
        $('.parameter-graph-form').find('.icon .unlocked').hide();
        $('.parameter-graph-form').find('.range-input-container').show();
        $('.parameter-graph-form').find('.range-input-locked').hide();
        if ($(this).is(':checked')) {
            $(this).closest('.range-container').find('.icon .locked').hide();
            $(this).closest('.range-container').find('.icon .unlocked').show();
            $(this).parent().find('.range-input-container').hide();
            $(this).parent().find('.range-input-locked').show();
        }
    });

    //Parameter graph form submit
    $('.parameter-sliders').on('submit', '.parameter-graph-form', function(e) {
        e.preventDefault();

        var activeParameter = $(this).find('input[type=radio]:checked').val();

        var parameters = [];

        $(this).find('input[type=radio]:not(:checked)').each(function(n) {
            var value = $(this).parent().find('.range-value').html();
            parameters[n] = $(this).val() + "=" + value;
        });

        var name = $(this).find('input[name=name]').val();
        var program = $(this).find('input[name=program]').val();
        var data = $(this).find('input[name=data]').val();

        var graphContainer = $(this).closest('.parameter-sliders').find('.parameter-graph');

        $.get("/results/get-parameter-graph?active-parameter=" + activeParameter + "&parameters=" + parameters.join(',') + "&name=" + name + "&program=" + program + "&data=" + data, function(csv) {
            var allTextLines = csv.split('\n');
            var header = allTextLines[1].split(',');
            var graphType = 'column';
            if ($.isNumeric(header[0])) {
                graphType = 'line';
            }

            graphContainer.highcharts({
                chart: {
                    type: graphType,
                    zoomType: 'x'
                },
                exporting: {
                    enabled: true
                },
                plotOptions: {
                    series: {
                        step: 'left'
                    }
                },
                data: {
                    csv: csv,
                    itemDelimiter: ",",
                    lineDelimiter: "\n"
                },
                title: {
                    text: 'Qualities'
                },
                yAxis: {
                    title: {
                        text: 'Value'
                    }
                }
            });
        });
    });

    //See clustering - modal
    $('.cluster-modal').click(function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $.get(url, function(data) {
            $('#modal-container').html(data);
            $('#modal-container').modal();
            $('#modal-container > div').show();
        });
    });

    //Automatically submit graph form on change
    $('.parameter-sliders').on('change', 'input', function (e) {
        var form = $(this).closest('form');
        if (form.find('input[type=radio]:checked').length >= 1) {
            $(this).closest('form').submit();
        }
    });

    //Load in all sliders on result page
    $('.parameter-optimization .parameter-sliders').each(function() {
        var container = $(this);
        var name = $(this).data('name');
        var program = $(this).data('program');
        var data = $(this).data('data');
        $.ajax({
            url: "/results/get-parameter-sliders?name=" + name + "&program=" + program + "&data=" + data,
            type: 'get',
            dataType: 'html',
            success: function(data) {
                container.html(data);

                //Trigger a change, so the div containing the value is initialized
                container.find('input[type=range]').trigger('change');
            }
        });
    });

    //Load in clustering
    $('#load-clustering, .load-clustering').each(function() {
        var container = $(this);
        var name = $(this).data('name');
        var program = $(this).data('program');
        var data = $(this).data('data');
        var paramset = $(this).data('paramset');

        var clusterUrl = "";
        if (paramset == null) {
            clusterUrl = "/results/load-clustering-cluster?name=" + name + "&program=" + program + "&data=" + data;
        } else {
            clusterUrl = "/results/load-clustering?name=" + name + "&program=" + program + "&data=" + data + "&param-set=" + paramset;
        }

        $.ajax({
            url: clusterUrl,
            type: 'get',
            dataType: 'html',
            success: function(data) {
                container.html(data);
            }
        });
    });

    //Program upload - Add parameter
    $('.add-program-parameter').click(function() {
        var index = $('#program-parameters > div').length;

        var returnValue = '<div class="box">';
            returnValue += '<div class="form-group">';
                returnValue += '<div class="col-sm-3"><label>Parameter name</label></div>';
                returnValue += '<div class="col-sm-9"><input placeholder="Parameter name" class="form-control" type="text" id="parameters' + index + '.name" name="parameters[' + index + '].name"></div>';
            returnValue += '</div>';
            returnValue += '<div class="form-group">';
                returnValue += '<div class="col-sm-3"><label>Parameter description</label></div>';
                returnValue += '<div class="col-sm-9"><input placeholder="Parameter description" class="form-control" type="text" id="parameters' + index + '.description" name="parameters[' + index + '].description"></div>';
            returnValue += '</div>';
            returnValue += '<div class="form-group">';
                returnValue += '<div class="col-sm-3"><label>Parameter type</label></div>';
                returnValue += '<div class="col-sm-9"><select class="parameter-type" id="parameters' + index + '.type" name="parameters[' + index + '].type">';
                    returnValue += '<option value="1">Integer</option>';
                    returnValue += '<option value="2">Float</option>';
                    returnValue += '<option value="0">String</option>';
                returnValue += '</select></div>';
            returnValue += '</div>';
            returnValue += '<div class="form-group">';
                returnValue += '<div class="col-sm-3"><label>Default value</label></div>';
                returnValue += '<div class="col-sm-9"><input placeholder="Default value" class="form-control" type="text" id="parameters' + index + '.defaultValue" name="parameters[' + index + '].defaultValue"></div>';
            returnValue += '</div>';
            returnValue += '<div class="parameter-non-string">'
                returnValue += '<div class="form-group">';
                    returnValue += '<div class="col-sm-3"><label>Minimum value</label></div>';
                    returnValue += '<div class="col-sm-9"><input placeholder="Minimum value" class="form-control" type="text" id="parameters' + index + '.minValue" name="parameters[' + index + '].minValue"></div>';
                returnValue += '</div>';
                returnValue += '<div class="form-group">';
                    returnValue += '<div class="col-sm-3"><label>Maximum value</label></div>';
                    returnValue += '<div class="col-sm-9"><input placeholder="Maximum value" class="form-control" type="text" id="parameters' + index + '.maxValue" name="parameters[' + index + '].maxValue"></div>';
                returnValue += '</div>';
            returnValue += '</div>';
            returnValue += '<div class="parameter-string" style="display: none;">'
                returnValue += '<div class="form-group">';
                    returnValue += '<div class="col-sm-3"><label>Options</label></div>';
                    returnValue += '<div class="col-sm-9"><select disabled multiple class="tags form-control" id="parameters' + index + '.options" name="parameters[' + index + '].options" style="display:block; width:100%;"></select></div>';
                returnValue += '</div>';
            returnValue += '</div>';
            returnValue += '<div class="form-group">';
                returnValue += '<div class="checkbox"><div class="col-sm-3"></div>';
                    returnValue += '<div class="col-sm-9"><label>';
                        returnValue += '<input class="checkbox-optimize" type="checkbox" checked value="true" id="parameters' + index + '.optimizable" name="parameters[' + index + '].optimizable">Optimizable';
                    returnValue += '</label></div>';
                returnValue += '</div>';
            returnValue += '</div>';
        returnValue += '</div>';

        $("#program-parameters").append(returnValue);

        $('.tags').select2({
            tags: true,
            tokenSeparators: [',', ' ']
        });
    });

    //Show/hide form elements based on choice of parameter type
    $('#program-parameters').on('change', '.parameter-type', function() {
        var value = $(this).val();
        if (value == 0) {
            $(this).closest('.box').find('.parameter-non-string').hide();
            $(this).closest('.box').find('.parameter-non-string input').prop('disabled', true);

            $(this).closest('.box').find('.parameter-string').show();
            $(this).closest('.box').find('.parameter-string input').prop('disabled', false);
            $(this).closest('.box').find('.parameter-string select').prop('disabled', false);
        } else {
            $(this).closest('.box').find('.parameter-non-string').show();
            $(this).closest('.box').find('.parameter-non-string input').prop('disabled', false);

            $(this).closest('.box').find('.parameter-string').hide();
            $(this).closest('.box').find('.parameter-string input').prop('disabled', true);
            $(this).closest('.box').find('.parameter-string select').prop('disabled', true);
        }
    });

    //On page load, trigger change on parameter type selects
    $('#program-parameters .parameter-type').trigger('change');

    $('.tags').select2({
        tags: true,
        tokenSeparators: [',', ' ']
    });

    //Delete program button click
    $('#programs .delete-program').click(function() {
        if (confirm("Delete this program?")) {
            $.get('/programs-config/delete?name=' + $(this).data('name') + '&version=' + $(this).data('version'));
            $(this).parent().parent().parent().fadeOut();
        }
    });
});
